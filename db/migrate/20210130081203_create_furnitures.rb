class CreateFurnitures < ActiveRecord::Migration
  def change
    create_table :furnitures do |t|
      t.json    :model
      t.string  :category
      t.string  :variant_id
      t.timestamps
    end
  end
end
