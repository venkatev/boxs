class AddModelAndColorToFurniture < ActiveRecord::Migration
  def change
    change_table :furnitures do |t|
      t.remove :colors
      t.string :color_id
      t.string :model_id
      t.boolean :published, default: false
    end

    Furniture.all.select {|f| f.pending_captures_count == 0 }.each {|f| f.update published: true }
  end
end
