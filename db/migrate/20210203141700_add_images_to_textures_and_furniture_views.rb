class AddImagesToTexturesAndFurnitureViews < ActiveRecord::Migration
  def change
    add_column :textures, :image, :string
    add_column :furniture_views, :image, :string
  end
end
