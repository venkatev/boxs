class CreateCategoryCollections < ActiveRecord::Migration
  def change
    create_table :category_collections do |t|
      t.string :category
      t.timestamps
    end
  end
end
