class CreateMoodboardCategoryCollections < ActiveRecord::Migration
  def change
    create_table :moodboard_category_collections do |t|
      t.references :moodboard, index: true, foreign_key: true
      t.references :category_collection, index: true, foreign_key: true
      t.timestamps
    end
  end
end
