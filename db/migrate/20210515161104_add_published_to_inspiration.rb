class AddPublishedToInspiration < ActiveRecord::Migration
  def change
    add_column :inspirations, :published, :boolean, index: true, default: false

    # Mark all existing inspirations as published.
    Inspiration.update_all published: true
  end
end
