class CreateInspirationFurnitures < ActiveRecord::Migration
  def change
    create_table :inspiration_furnitures do |t|
      t.references :inspiration, foreign_key: true, index: true
      t.references :furniture, foreign_key: true, index: true
      t.timestamps null: false
    end
  end
end
