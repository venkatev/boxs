class CreateInspirations < ActiveRecord::Migration
  def change
    create_table :inspirations do |t|
      t.string :room_category
      t.string :image
      t.string :reference
      t.json :items, default: {}
      t.timestamps null: false
    end
  end
end
