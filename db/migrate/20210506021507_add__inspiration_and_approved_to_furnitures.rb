class AddInspirationAndApprovedToFurnitures < ActiveRecord::Migration
  def change
    add_column :furnitures, :inspiration, :string
    add_column :furnitures, :approved, :boolean, index: true
    add_column :furnitures, :archived, :boolean, default: false, index: true
    add_column :furnitures, :owner, :string
  end
end
