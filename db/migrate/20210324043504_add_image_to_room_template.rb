class AddImageToRoomTemplate < ActiveRecord::Migration
  def change
    add_column :room_templates, :image, :string
  end
end
