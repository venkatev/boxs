class AddFeaturedToDesign < ActiveRecord::Migration
  def change
    change_table :designs do |t|
      t.boolean :featured, default: false
    end

    Design.update_all featured: false
  end
end
