class CreateFurnitureLayouts < ActiveRecord::Migration
  def change
    create_table :furniture_layouts do |t|
      t.references  :room, index: true, foreign_key: true
      t.json        :items_json, default: []
      t.timestamps
    end
  end
end
