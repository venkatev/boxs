class CreateCameraViews < ActiveRecord::Migration
  def change
    create_table :camera_views do |t|
      t.belongs_to :room_template, index: true, foreign_key: true
      t.json :position, default: {}
      t.json :target, default: {}
      t.boolean :primary
      t.string :image
      t.timestamps
    end
  end
end
