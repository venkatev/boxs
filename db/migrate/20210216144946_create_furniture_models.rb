class CreateFurnitureModels < ActiveRecord::Migration
  def change
    create_table :furniture_models do |t|
      t.belongs_to :furniture, foreign_key: true, index: true
      t.json :model
      t.timestamps null: false
    end
  end
end
