class CreateDesignViews < ActiveRecord::Migration
  def change
    create_table :design_views do |t|
      t.references :design, index: true, foreign_key: true
      t.references :camera_view, index: true, foreign_key: true
      t.boolean :deferred, default: true
      t.json :pixel_map, default: []
      t.string      :image
      t.timestamps
    end
  end
end
