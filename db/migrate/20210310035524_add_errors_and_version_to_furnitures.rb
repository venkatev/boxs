class AddErrorsAndVersionToFurnitures < ActiveRecord::Migration
  def change
    add_column :furnitures, :data_errors, :text
    add_column :furnitures, :version, :integer, default: 0
  end
end
