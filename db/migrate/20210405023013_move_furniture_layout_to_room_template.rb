class MoveFurnitureLayoutToRoomTemplate < ActiveRecord::Migration
  def change
    change_table :furniture_layouts do |t|
      t.references :room_template, index: true, foreign_key: true
    end

    RoomTemplate.all.each do |room_template|
      Room.where(room_template_id: room_template.id).each do |r|
        r.furniture_layouts.update_all room_template_id: room_template.id
      end
    end
  end
end
