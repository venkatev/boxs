class AddPublishedToRoomTemplates < ActiveRecord::Migration
  def change
    add_column :room_templates, :published, :boolean, index: true
  end
end
