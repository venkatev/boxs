class AddLightSettingsToFurniture < ActiveRecord::Migration
  def change
    default_lights = [
      {
        type: 'ambient',
        intensity: 0.3
      },
      {
        type: 'spot',
        intensity: 1,
        position: { x: 0, y: 3000, z: 3000 }
      }
    ]

    change_table :furnitures do |t|
      t.json :lights, default: default_lights
    end

    Furniture.update_all lights: default_lights
  end
end
