class AddStyleAndColorsToFurnitures < ActiveRecord::Migration
  def change
    add_column :furnitures, :styles, :string, array: true, default: []
    add_column :furnitures, :colors, :string, array: true, default: []
  end
end
