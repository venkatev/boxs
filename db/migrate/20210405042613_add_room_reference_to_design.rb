class AddRoomReferenceToDesign < ActiveRecord::Migration
  def change
    change_table :designs do |t|
      t.references :room, index: true, foreign_key: true
    end

    Design.all.each do |design|
      design.update room_id: design.furniture_layout.room_id
    end
  end
end
