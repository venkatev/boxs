class CreateDesigns < ActiveRecord::Migration
  def change
    create_table :designs do |t|
      t.references  :furniture_layout, index: true, foreign_key: true
      t.references  :moodboard, index: true, foreign_key: true
      t.json        :item_mapping, default: {}
      t.timestamps
    end
  end
end
