class CreateCategoryCollectionFurnitures < ActiveRecord::Migration
  def change
    create_table :category_collection_furnitures do |t|
      t.references :category_collection, index: true, foreign_key: true
      t.references :furniture, index: true, foreign_key: true
      t.timestamps
    end
  end
end
