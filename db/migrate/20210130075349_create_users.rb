class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :mobile, null: false, index: true
      t.string :otp_secret, null: false
      t.timestamps null: false
    end
  end
end
