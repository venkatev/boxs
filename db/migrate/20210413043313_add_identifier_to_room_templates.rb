class AddIdentifierToRoomTemplates < ActiveRecord::Migration
  def change
    add_column :room_templates, :identifier, :string, index: true
  end
end
