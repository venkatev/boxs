class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.references :user, index: true, foreign_key: true
      t.references :room_template, index: true, foreign_key: true
      t.string :category
      t.timestamps
    end
  end
end
