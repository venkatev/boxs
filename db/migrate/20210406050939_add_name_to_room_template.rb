class AddNameToRoomTemplate < ActiveRecord::Migration
  def change
    change_table :room_templates do |t|
      t.string :name
    end
  end
end
