class CreateTextures < ActiveRecord::Migration
  def change
    create_table :textures do |t|
      t.string :name
      t.references :furniture, index: true, foreign_key: true
      t.timestamps
    end
  end
end
