class CreateMoodboards < ActiveRecord::Migration
  def change
    create_table :moodboards do |t|
      t.string :room_category
      t.timestamps
    end
  end
end
