class CreateRoomTemplates < ActiveRecord::Migration
  def change
    create_table :room_templates do |t|
      t.string  :room_category, null: false, index: true
      t.integer :length, null: false, index: true
      t.integer :breadth, null: false, index: true
      t.integer :height, null: false, index: true
      t.timestamps null: false
    end
  end
end