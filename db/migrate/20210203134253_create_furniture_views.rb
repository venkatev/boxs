class CreateFurnitureViews < ActiveRecord::Migration
  def change
    create_table :furniture_views do |t|
      t.references :furniture, index: true, foreign_key: true
      t.float :theta, index: true
      t.float :phi, index: true
      t.float :r, index: true
      t.timestamps
    end
  end
end
