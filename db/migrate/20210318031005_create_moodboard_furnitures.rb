class CreateMoodboardFurnitures < ActiveRecord::Migration
  def change
    create_table :moodboard_furnitures do |t|
      t.belongs_to :furniture, foreign_key: true, index: true
      t.belongs_to :moodboard, foreign_key: true, index: true
      t.timestamps null: false
    end
  end
end
