# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210520034719) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "camera_views", force: :cascade do |t|
    t.integer  "room_template_id"
    t.json     "position",         default: {}
    t.json     "target",           default: {}
    t.boolean  "primary"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "camera_views", ["room_template_id"], name: "index_camera_views_on_room_template_id", using: :btree

  create_table "category_collection_furnitures", force: :cascade do |t|
    t.integer  "category_collection_id"
    t.integer  "furniture_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "category_collection_furnitures", ["category_collection_id"], name: "index_category_collection_furnitures_on_category_collection_id", using: :btree
  add_index "category_collection_furnitures", ["furniture_id"], name: "index_category_collection_furnitures_on_furniture_id", using: :btree

  create_table "category_collections", force: :cascade do |t|
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "design_views", force: :cascade do |t|
    t.integer  "design_id"
    t.integer  "camera_view_id"
    t.boolean  "deferred",       default: true
    t.json     "pixel_map",      default: []
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "design_views", ["camera_view_id"], name: "index_design_views_on_camera_view_id", using: :btree
  add_index "design_views", ["design_id"], name: "index_design_views_on_design_id", using: :btree

  create_table "designs", force: :cascade do |t|
    t.integer  "furniture_layout_id"
    t.integer  "moodboard_id"
    t.json     "item_mapping",        default: {}
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "room_id"
    t.boolean  "featured",            default: false
  end

  add_index "designs", ["furniture_layout_id"], name: "index_designs_on_furniture_layout_id", using: :btree
  add_index "designs", ["moodboard_id"], name: "index_designs_on_moodboard_id", using: :btree
  add_index "designs", ["room_id"], name: "index_designs_on_room_id", using: :btree

  create_table "furniture_layouts", force: :cascade do |t|
    t.integer  "room_id"
    t.json     "items_json",       default: []
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "room_template_id"
  end

  add_index "furniture_layouts", ["room_id"], name: "index_furniture_layouts_on_room_id", using: :btree
  add_index "furniture_layouts", ["room_template_id"], name: "index_furniture_layouts_on_room_template_id", using: :btree

  create_table "furniture_views", force: :cascade do |t|
    t.integer  "furniture_id"
    t.float    "theta"
    t.float    "phi"
    t.float    "r"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
  end

  add_index "furniture_views", ["furniture_id"], name: "index_furniture_views_on_furniture_id", using: :btree
  add_index "furniture_views", ["phi"], name: "index_furniture_views_on_phi", using: :btree
  add_index "furniture_views", ["r"], name: "index_furniture_views_on_r", using: :btree
  add_index "furniture_views", ["theta"], name: "index_furniture_views_on_theta", using: :btree

  create_table "furnitures", force: :cascade do |t|
    t.string   "variant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "identifier"
    t.integer  "required_captures", default: 0
    t.json     "dimensions"
    t.string   "model"
    t.text     "data_errors"
    t.integer  "version",           default: 0
    t.string   "styles",            default: [],                                                                                                                    array: true
    t.string   "color_id"
    t.string   "model_id"
    t.boolean  "published",         default: false
    t.json     "lights",            default: [{"type"=>"ambient", "intensity"=>0.3}, {"type"=>"spot", "intensity"=>1, "position"=>{"x"=>0, "y"=>3000, "z"=>3000}}]
    t.string   "inspiration"
    t.boolean  "approved"
    t.boolean  "archived",          default: false
    t.string   "owner"
    t.integer  "price"
  end

  create_table "inspiration_furnitures", force: :cascade do |t|
    t.integer  "inspiration_id"
    t.integer  "furniture_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "inspiration_furnitures", ["furniture_id"], name: "index_inspiration_furnitures_on_furniture_id", using: :btree
  add_index "inspiration_furnitures", ["inspiration_id"], name: "index_inspiration_furnitures_on_inspiration_id", using: :btree

  create_table "inspirations", force: :cascade do |t|
    t.string   "room_category"
    t.string   "image"
    t.string   "reference"
    t.json     "items",         default: {}
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "published",     default: false
  end

  create_table "moodboard_category_collections", force: :cascade do |t|
    t.integer  "moodboard_id"
    t.integer  "category_collection_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "moodboard_category_collections", ["category_collection_id"], name: "index_moodboard_category_collections_on_category_collection_id", using: :btree
  add_index "moodboard_category_collections", ["moodboard_id"], name: "index_moodboard_category_collections_on_moodboard_id", using: :btree

  create_table "moodboard_furnitures", force: :cascade do |t|
    t.integer  "furniture_id"
    t.integer  "moodboard_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "moodboard_furnitures", ["furniture_id"], name: "index_moodboard_furnitures_on_furniture_id", using: :btree
  add_index "moodboard_furnitures", ["moodboard_id"], name: "index_moodboard_furnitures_on_moodboard_id", using: :btree

  create_table "moodboards", force: :cascade do |t|
    t.string   "room_category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "room_templates", force: :cascade do |t|
    t.string   "room_category", null: false
    t.integer  "length",        null: false
    t.integer  "breadth",       null: false
    t.integer  "height",        null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "image"
    t.string   "name"
    t.string   "identifier"
    t.boolean  "published"
  end

  add_index "room_templates", ["breadth"], name: "index_room_templates_on_breadth", using: :btree
  add_index "room_templates", ["height"], name: "index_room_templates_on_height", using: :btree
  add_index "room_templates", ["length"], name: "index_room_templates_on_length", using: :btree
  add_index "room_templates", ["room_category"], name: "index_room_templates_on_room_category", using: :btree

  create_table "rooms", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "room_template_id"
    t.string   "room_category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rooms", ["room_template_id"], name: "index_rooms_on_room_template_id", using: :btree
  add_index "rooms", ["user_id"], name: "index_rooms_on_user_id", using: :btree

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "textures", force: :cascade do |t|
    t.string   "name"
    t.integer  "furniture_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
  end

  add_index "textures", ["furniture_id"], name: "index_textures_on_furniture_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "mobile",     null: false
    t.string   "otp_secret", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "users", ["mobile"], name: "index_users_on_mobile", using: :btree

  add_foreign_key "camera_views", "room_templates"
  add_foreign_key "category_collection_furnitures", "category_collections"
  add_foreign_key "category_collection_furnitures", "furnitures"
  add_foreign_key "design_views", "camera_views"
  add_foreign_key "design_views", "designs"
  add_foreign_key "designs", "furniture_layouts"
  add_foreign_key "designs", "moodboards"
  add_foreign_key "designs", "rooms"
  add_foreign_key "furniture_layouts", "room_templates"
  add_foreign_key "furniture_layouts", "rooms"
  add_foreign_key "furniture_views", "furnitures"
  add_foreign_key "inspiration_furnitures", "furnitures"
  add_foreign_key "inspiration_furnitures", "inspirations"
  add_foreign_key "moodboard_category_collections", "category_collections"
  add_foreign_key "moodboard_category_collections", "moodboards"
  add_foreign_key "moodboard_furnitures", "furnitures"
  add_foreign_key "moodboard_furnitures", "moodboards"
  add_foreign_key "rooms", "room_templates"
  add_foreign_key "rooms", "users"
  add_foreign_key "textures", "furnitures"
end
