FROM ruby:2.3.7
RUN apt-get update && apt-get install -y --no-install-recommends \
    nodejs
RUN gem install bundler

ADD Gemfile /boxs/
ADD Gemfile.lock /boxs/
WORKDIR /boxs
RUN bundle install

ADD . /boxs

# Save rails console and irb history
RUN echo 'IRB.conf[:SAVE_HISTORY] = 10000' > /root/.irbrc

# Symlink important files so they remain across docker restarts
RUN ln -s ~/host/.irb_history ~/
RUN ln -s ~/host/.bash_history ~/

EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
