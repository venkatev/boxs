self.benchmark("Processing 1") do
  threads = []

  10.times do |i|
    threads << Thread.new do
      tmp_image = "t1/#{i + 1}.png"
      convert = MiniMagick::Tool::Convert.new
      convert << tmp_image
      convert.resize("400x400")
      convert << "-matte"
      convert << "-virtual-pixel"
      convert << "transparent"
      convert << "-distort"
      convert << "Perspective"
      convert << "0,0 10,10 400,400 410,410 0,400 0,410 400,0 410,0"
      convert << "t2/#{i + 1}.png"
      convert.call
    end
  end

  threads.each(&:join)
end

composite_image = nil

self.benchmark("Processing 2") do
  convert = MiniMagick::Tool::Convert.new
  10.times do |i|
    convert << "-composite"
    convert << "t2/#{i + 1}.png"
    convert << "-geometry"
    convert << "+#{i * 10}+#{i * 10}"
    # image = MiniMagick::Image.new("t2/#{i + 1}.png")
    # if composite_image
    #   composite_image = composite_image.composite(image)
    # else
    #   composite_image = image
    # end
  end

  convert << "t2/output.png"
  convert.call
end

render nothing: true
return

