# == Schema Information
#
# Table name: furnitures
#
#  id                :integer          not null, primary key
#  category          :string
#  variant_id        :string
#  created_at        :datetime
#  updated_at        :datetime
#  identifier        :string
#  required_captures :integer          default("0")
#  dimensions        :json
#  model             :string
#  data_errors       :text
#  version           :integer          default("0")
#  styles            :string           default("{}"), is an Array
#  colors            :string           default("{}"), is an Array
#

FactoryBot.define do
  factory :furniture do

    factory (:brown_modern_sofa) do
      colors {["brown"]}
      styles {["modern"]}
      variant_id {"0602"}
    end
    factory (:white_modern_sofa) do
      colors {["white"]}
      styles {["modern"]}
      variant_id {"0602"}
    end
    factory (:brown_traditional_sofa) do
      colors {["brown"]}
      styles {["traditional"]}
      variant_id {"0602"}
    end
    factory (:orange_traditional_sofa) do
      colors {["orange"]}
      styles {["traditional"]}
      variant_id {"0602"}
    end

    factory (:brown_modern_dining_set) do
      variant_id {"1801"}
      colors {["brown"]}
      styles {["modern"]}
    end
    factory (:white_modern_dining_set) do
      colors {["1801"]}
      styles {["modern"]}
      variant_id {"1801"}
    end
    factory (:brown_traditional_dining_set) do
      colors {["brown"]}
      styles {["traditional"]}
      variant_id {"1801"}
    end
    factory (:orange_traditional_dining_set) do
      colors {["orange"]}
      styles {["traditional"]}
      variant_id {"1801"}
    end
  end
end
