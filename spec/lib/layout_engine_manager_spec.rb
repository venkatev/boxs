require 'spec_helper'
require 'layout_engine_manager'

RSpec.describe LayoutEngineManager, type: :model do
  describe 'normalize_2' do
    it 'return the cell with orientation 1 as the reference' do
      expect(LEM::TH.normalize_2({ room_length: 14, room_breadth: 25 }, '1N1')).to eq('1N1')

      expect(LEM::TH.normalize_2({ room_length: 14, room_breadth: 25 }, '1N2')).to eq('1A1')
      expect(LEM::TH.normalize_2({ room_length: 14, room_breadth: 25 }, '2F2')).to eq('9B1')

      expect(LEM::TH.normalize_2({ room_length: 14, room_breadth: 25 }, '2F3')).to eq('24I1')

      expect(LEM::TH.normalize_2({ room_length: 14, room_breadth: 25 }, '2F4')).to eq('6X1')
    end
  end

  describe 'normalize_2_range' do
    it 'return' do
      expect(LEM::TH.normalize_2_range({ room_length: 14, room_breadth: 25 }, '1J2-7N2')).to eq('1A1-5G1')
    end
  end
end