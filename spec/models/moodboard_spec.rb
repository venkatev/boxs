# == Schema Information
#
# Table name: moodboards
#
#  id            :integer          not null, primary key
#  room_category :string
#  created_at    :datetime
#  updated_at    :datetime
#

require 'rails_helper'

RSpec.describe Moodboard, type: :model do

  it 'should list all design options' do
    sofa_1 = create(:furniture)
    sofa_2 = create(:furniture)
    sofa_3 = create(:furniture)
    sofa_4 = create(:furniture)

    dining_1 = create(:furniture)
    dining_2 = create(:furniture)
    dining_3 = create(:furniture)
    dining_4 = create(:furniture)

    sofa_col_1 = create(:category_collection, category: FurnitureCategory::SOFA, furnitures: [sofa_1, sofa_2])
    sofa_col_2 = create(:category_collection, category: FurnitureCategory::SOFA, furnitures: [sofa_3, sofa_4])
    dining_col_1 = create(:category_collection, category: FurnitureCategory::DINING_SET, furnitures: [dining_1, dining_4])
    dining_col_2 = create(:category_collection, category: FurnitureCategory::DINING_SET, furnitures: [dining_2, dining_3])

    moodboard = create(:moodboard, room_category: RoomCategory::LIVING_ROOM, category_collections: [sofa_col_1, dining_col_2])
    design_options = moodboard.design_options

    expect(design_options).to eq([
      {
        FurnitureCategory::SOFA => sofa_1,
        FurnitureCategory::DINING_SET => dining_2
      },
      {
        FurnitureCategory::SOFA => sofa_1,
        FurnitureCategory::DINING_SET => dining_3
      },
      {
        FurnitureCategory::SOFA => sofa_2,
        FurnitureCategory::DINING_SET => dining_2
      },
      {
        FurnitureCategory::SOFA => sofa_2,
        FurnitureCategory::DINING_SET => dining_3
      }
    ])
  end
end
