# == Schema Information
#
# Table name: furniture_layouts
#
#  id               :integer          not null, primary key
#  room_id          :integer
#  items_json       :json             default("[]")
#  created_at       :datetime
#  updated_at       :datetime
#  room_template_id :integer
#

require 'rails_helper'

RSpec.describe Room, type: :model do
  it 'generate_next_design' do
    sofa_1 = create(:furniture, id: 1, variant_id: '0601', identifier: SecureRandom.hex(8))
    sofa_2 = create(:furniture, id: 2, variant_id: '0601', identifier: SecureRandom.hex(8))
    sofa_3 = create(:furniture, id: 3, variant_id: '0602', identifier: SecureRandom.hex(8))
    sofa_4 = create(:furniture, id: 4, variant_id: '0602', identifier: SecureRandom.hex(8))

    dining_1 = create(:furniture, id: 5, variant_id: '1701', identifier: SecureRandom.hex(8))
    dining_2 = create(:furniture, id: 6, variant_id: '1701', identifier: SecureRandom.hex(8))
    dining_3 = create(:furniture, id: 7, variant_id: '1702', identifier: SecureRandom.hex(8))
    dining_4 = create(:furniture, id: 8, variant_id: '1702', identifier: SecureRandom.hex(8))

    sofa_col_1 = create(:category_collection, category: FurnitureCategory::SOFA, furnitures: [sofa_1, sofa_2])
    sofa_col_2 = create(:category_collection, category: FurnitureCategory::SOFA, furnitures: [sofa_3, sofa_4])
    dining_col_1 = create(:category_collection, category: FurnitureCategory::DINING_SET, furnitures: [dining_1, dining_4])
    dining_col_2 = create(:category_collection, category: FurnitureCategory::DINING_SET, furnitures: [dining_2, dining_3])

    moodboard_1 = create(:moodboard, room_category: RoomCategory::LIVING_ROOM, category_collections: [sofa_col_1, dining_col_2])
    moodboard_2 = create(:moodboard, room_category: RoomCategory::LIVING_ROOM, category_collections: [sofa_col_1, dining_col_2])
    moodboard_3 = create(:moodboard, room_category: RoomCategory::LIVING_ROOM, category_collections: [sofa_col_1, dining_col_2])

    rt_1 = RoomTemplate.create! room_category: 'living_room', length: 10, breadth: 10

    fl_1 = create(:furniture_layout, items: [
      {
        id: 'a123',
        category: FurnitureCategory::SOFA,
        variant_id: '1'
      },
      {
        id: 'b123',
        category: FurnitureCategory::DINING_SET,
        variant_id: '3'
      }
    ], room_template: rt_1)

    fl_2 = create(:furniture_layout, items: [
      {
        id: 'a123',
        category: FurnitureCategory::SOFA,
        variant_id: '1',
        cell_range: '1A1-1C1'
      },
      {
        id: 'b123',
        category: FurnitureCategory::DINING_SET,
        variant_id: '3',
        cell_range: '1A1-1C1'
      }
    ], room_template: rt_1)

    user = create(:user, name: 'Some name', mobile: '1234512345')
    room = Room.create! room_template: rt_1, user: user

    room.designs = [
      Design.create!(
        furniture_layout: fl_1,
        item_mapping: {
          'a123' => {
            id: sofa_1.id,
            dimensions: {
              height: 1000,
              width: 1000,
              depth: 1000
            },
            cell_range: '1A1-1C1'
          },
          'b123' => {
            id: dining_2.id,
            dimensions: {
              height: 1000,
              width: 1000,
              depth: 1000
            },
            cell_range: '1A1-1C1'
          }
        }.deep_stringify_keys
      )
    ]

    # expect(Moodboard).to receive(:all).at_least(:once).and_return([moodboard_1, moodboard_2, moodboard_3])

    # We have 4 possibilities overall.
    #   sofa_1, dining_1
    #   sofa_1, dining_2
    #   sofa_2, dining_1
    #   sofa_2, dining_2
    #
    # sofa_1, dining_2 has already been taken. We must get 3 more new unique options.
    3.times do |i|
      design = room.generate_next_design
      expect(room.designs.size).eq(i + 1)

      # # The new option shouldn't be the same as any of the existing designs.
      # room.designs.each do |design|
      #   expect(design.item_mapping.values.map {|item| item['id']}).not_to match_array(design_option.values.map {|f| f['id']})
      # end

      # fl_1.designs << create(:design, furniture_layout: fl_1, item_mapping: design_option)
    end

    expect(room.generate_next_design).to be_nil
  end
end
