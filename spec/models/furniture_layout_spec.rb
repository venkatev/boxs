# == Schema Information
#
# Table name: furniture_layouts
#
#  id               :integer          not null, primary key
#  room_id          :integer
#  items_json       :json             default("[]")
#  created_at       :datetime
#  updated_at       :datetime
#  room_template_id :integer
#

require 'rails_helper'

RSpec.describe FurnitureLayout, type: :model do
  it 'next_design_option' do
    sofa_1 = build(:furniture, id: 1, category: FurnitureCategory::SOFA, variant_id: '1')
    sofa_2 = build(:furniture, id: 2, category: FurnitureCategory::SOFA, variant_id: '1')
    sofa_3 = build(:furniture, id: 3, category: FurnitureCategory::SOFA, variant_id: '2')
    sofa_4 = build(:furniture, id: 4, category: FurnitureCategory::SOFA, variant_id: '2')

    dining_1 = build(:furniture, id: 5, category: FurnitureCategory::DINING_SET, variant_id: '3')
    dining_2 = build(:furniture, id: 6, category: FurnitureCategory::DINING_SET, variant_id: '3')
    dining_3 = build(:furniture, id: 7, category: FurnitureCategory::DINING_SET, variant_id: '3')
    dining_4 = build(:furniture, id: 8, category: FurnitureCategory::DINING_SET, variant_id: '4')

    sofa_col_1 = build(:category_collection, category: FurnitureCategory::SOFA, furnitures: [sofa_1, sofa_2])
    sofa_col_2 = build(:category_collection, category: FurnitureCategory::SOFA, furnitures: [sofa_3, sofa_4])
    dining_col_1 = build(:category_collection, category: FurnitureCategory::DINING_SET, furnitures: [dining_1, dining_4])
    dining_col_2 = build(:category_collection, category: FurnitureCategory::DINING_SET, furnitures: [dining_2, dining_3])

    moodboard_1 = build(:moodboard, room_category: RoomCategory::LIVING_ROOM, category_collections: [sofa_col_1, dining_col_2])
    moodboard_2 = build(:moodboard, room_category: RoomCategory::LIVING_ROOM, category_collections: [sofa_col_1, dining_col_2])
    moodboard_3 = build(:moodboard, room_category: RoomCategory::LIVING_ROOM, category_collections: [sofa_col_1, dining_col_2])

    fl_1 = build(:furniture_layout, items: [
      {
        id: 'a123',
        category: FurnitureCategory::SOFA,
        variant_id: '1'
      },
      {
        id: 'b123',
        category: FurnitureCategory::DINING_SET,
        variant_id: '3'
      }
    ])

    fl_1.designs = [
      build(:design,
        furniture_layout: fl_1,
        item_mapping: {
          'a123' => {
            id: sofa_1.id
          },
          'b123' => {
            id: dining_2.id
          }
        }
      )
    ]

    expect(Moodboard).to receive(:all).at_least(:once).and_return([moodboard_1, moodboard_2, moodboard_3])

    # We have 4 possibilities overall.
    #   sofa_1, dining_1
    #   sofa_1, dining_2
    #   sofa_2, dining_1
    #   sofa_2, dining_2
    #
    # sofa_1, dining_2 has already been taken. We must get 3 more new UNIQUE options.
    3.times do |i|
      design_option = fl_1.next_design_option

      # The new option shouldn't be the same as any of the existing designs.
      fl_1.designs.each do |design|
        expect(design.item_mapping.values.map {|item| item['id']}).not_to match_array(design_option.values.map {|f| f['id']})
      end

      fl_1.designs << build(:design, furniture_layout: fl_1, item_mapping: design_option)
    end

    expect(fl_1.next_design_option).to be_nil
  end
end
