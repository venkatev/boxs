# == Schema Information
#
# Table name: furnitures
#
#  id                :integer          not null, primary key
#  category          :string
#  variant_id        :string
#  created_at        :datetime
#  updated_at        :datetime
#  identifier        :string
#  required_captures :integer          default("0")
#  dimensions        :json
#  model             :string
#  data_errors       :text
#  version           :integer          default("0")
#  styles            :string           default("{}"), is an Array
#  colors            :string           default("{}"), is an Array
#

require 'rails_helper'

RSpec.describe Furniture, type: :model do
  context 'import model' do
    it 'should import from a given zip file' do
      @furniture = Furniture.new variant_id: '1502', identifier: 'identifier_1', zipped_model_file: File.open(Rails.root.to_s + "/spec/data/furnitures/1.zip")
      expect(@furniture.save).to eq(true)

      expect(@furniture.model.present?).to eq(true)
      expect(@furniture.textures.size).to eq(2)
    end
  end
end
