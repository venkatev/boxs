ActiveAdmin.register FurnitureLayout do
  permit_params :room_template_id, :furniture_placements
  index do
    id_column
    column :room_template_id
    column 'Room Template Name' do |fl|
      fl.room_template.name if fl.room_template
    end
    column :furniture_placements_1
    actions
  end

  form title: 'Furniture Layout' do |f|
    f.inputs do
      f.input :room_template_id, :as => :select, :collection => RoomTemplate.all.map {|rt| ["#{rt.id} - #{rt.name}", rt.id]}
      # Sample furniture placements
      # [["0801", "3A-14G", "4", "l"], ["1102", "7K-11L", "2", "r"], ["2301", "1J-2L", "1", "r"]]
      f.input :furniture_placements, as: :text
      f.input :furniture_placements_1, label: "Furniture placements current", input_html: {disabled: true}
      f.input :items_json, input_html: {disabled: true}
    end
    actions
  end
end