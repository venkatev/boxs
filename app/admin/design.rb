ActiveAdmin.register Design do
  filter :id
  filter :featured

  permit_params :featured

  index do
    id_column
    column :featured
    column "Image" do |design|
      if design.views[0]
        image_tag design.views[0].image.url, :height => 200
      end
    end
    actions
  end

  form title: 'Design' do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.inputs do
        image_tag f.object.views[0].image.url, :height => 200
      end

      f.input :featured
    end
    actions
  end
end
