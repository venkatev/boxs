ActiveAdmin.register Inspiration do
  filter :id
  filter :room_category
  filter :reference
  filter :published

  permit_params :room_category, :reference, :image, :published

  index do
    id_column
    column :room_category
    column :reference
    column :published
    column "Furniture Count" do |inspiration|
      inspiration.furnitures.count
    end
    column "Image" do |inspiration|
      if inspiration.image
        image_tag inspiration.image.url, :height => 200
      end
    end
    actions
  end

  show do
    attributes_table do
     row :room_category
     row :reference
     row :published
     row :image do |inspiration|
        image_tag inspiration.image.url, :height => 200
      end
     row "Furnitures" do |inspiration|
      inspiration.furnitures.map{|f| "id: #{f.id}, identifier: #{f.identifier}, variant_id: #{f.variant_id}"}
     end
    end
    active_admin_comments
  end

  form title: 'Inspiration' do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :room_category
      f.input :reference
      f.input :image, as: :file, :hint => f.object.image ? image_tag(f.object.image.url) : 'Upload Inspiration'
      f.input :published
    end
    actions
  end

end
