ActiveAdmin.register RoomTemplate do
  permit_params :identifier, :name, :room_category, :length, :breadth, :height, :image, :published
  index do
    id_column
    column :identifier
    column :name
    column :room_category
    column :length
    column :breadth
    column :height
    column :published
    column :image do |rt_image|
      image_tag rt_image.image.url, :width => 50
    end
    actions
  end

  form title: 'Room Template' do |f|
    f.inputs do
      f.input :identifier
      f.input :name
      f.input :room_category
      f.input :length
      f.input :breadth
      f.input :height
      f.input :image, label: 'Room Image', as: :file, :hint => f.object.image ? image_tag(f.object.image.small.url) : 'Upload Floor Plan'
      f.input :published
    end
    actions
  end
end
