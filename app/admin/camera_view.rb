ActiveAdmin.register CameraView do
  permit_params :room_template_id, :position, :target, :image
  index do
    id_column
    column :room_template_id
    column 'Room Template Name' do |cv|
      cv.room_template.name if cv.room_template
    end
    column :position
    column :target
    column :image do |cv_image|
      image_tag cv_image.image.url, :width => 50
    end
    actions
  end

  form title: 'Camera View' do |f|
    f.inputs do
      f.input :room_template_id, :as => :select, :collection => RoomTemplate.all.map {|rt| ["#{rt.id} - #{rt.name}", rt.id]}
      f.input :position, as: :json
      f.input :target, as: :json
      f.input :image, label: 'Floor Plan', as: :file, :hint => f.object.image ? image_tag(f.object.image.small.url) : 'Upload Room View'
    end
    actions
  end
end