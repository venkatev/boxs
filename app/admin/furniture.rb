ActiveAdmin.register Furniture do
  filter :id
  filter :identifier
  filter :variant_id
  filter :data_errors
  filter :published
  filter :inspirations , :as => :select, :collection => Inspiration.all.map{|i| "#{i.id} - #{i.reference}"}


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :identifier, :variant_id, :zipped_model_file, :published, :approved, :archived, :color_id, :model_id, :owner, inspiration_ids: []
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  index do
    id_column
    column :identifier
    column :variant_id
    column :pending_captures_count
    column "Dimensions" do |furniture|
      if furniture.dimensions
        "H#{furniture.dimensions["height"].to_i},W#{furniture.dimensions["width"].to_i},D#{furniture.dimensions["depth"].to_i}"
      end
    end
    column "Model ID" do |furniture|
      furniture.model_id
    end
    column :color_id
    column :published
    column :archived
    column "Image" do |furniture|
      if furniture.furniture_views.primary.first
        image_tag furniture.furniture_views.primary.first.image.url, :height => 200
      else
        link_to "Render", renderer_furniture_url(furniture.id, c: "30,3000")
      end
    end
    column :data_errors do |furniture|
      (furniture.data_errors || "")[0..50]
    end

    actions
  end

  show do
    attributes_table do
      row :identifier
      row :variant_id
      row :pending_captures_count
      row "Dimensions" do |furniture|
        if furniture.dimensions
          "H#{furniture.dimensions["height"].to_i},W#{furniture.dimensions["width"].to_i},D#{furniture.dimensions["depth"].to_i}"
        end
      end
      row "Model ID" do |furniture|
        furniture.model_id
      end
      row :color_id
      row :published
      row :archived
      row "Image" do |furniture|
        if furniture.furniture_views.primary.first
          image_tag furniture.furniture_views.primary.first.image.url, :height => 200
        else
          link_to "Render", renderer_furniture_url(furniture.id, c: "30,3000")
        end
      end
      row :data_errors do |furniture|
        (furniture.data_errors || "")[0..50]
      end
    end
    active_admin_comments
  end

  form title: 'Furniture' do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :identifier
      f.input :variant_id
      f.input :published
      f.input :approved
      f.input :archived
      f.input :owner
      f.input :color_id, as: :string
      f.input :zipped_model_file, as: :file
      f.input :inspirations, as: :checkbox_image, collection: Inspiration.all.collect {|x| [x.reference, x.id]}
    end
    actions
  end
end
