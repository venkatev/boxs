# == Schema Information
#
# Table name: room_templates
#
#  id            :integer          not null, primary key
#  room_category :string           not null
#  length        :integer          not null
#  breadth       :integer          not null
#  height        :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  image         :string
#  name          :string
#

class RoomTemplate < ActiveRecord::Base
  has_many :camera_views, dependent: :destroy
  has_many :furniture_layouts, dependent: :destroy

  before_create :set_default_height

  mount_base64_uploader :image, RoomTemplateImageUploader

  scope :published, -> { where(published: true) }

  # Finds the layouts that acoomodate the given furniture's variant id.
  def find_layouts_for(furniture)
    self.furniture_layouts.select {|fl| fl.items.any? {|item| item.variant_id == furniture.variant_id } }
  end

  private

  def set_default_height
    self.height ||= 10
  end
end
