# == Schema Information
#
# Table name: moodboards
#
#  id            :integer          not null, primary key
#  room_category :string
#  created_at    :datetime
#  updated_at    :datetime
#

class Moodboard < ApplicationRecord
  cattr_accessor :design_options

  has_many :moodboard_category_collections, dependent: :destroy
  has_many :category_collections, through: :moodboard_category_collections

  REQUIRED_CATEGORIES = {
    RoomCategory::LIVING_ROOM => [
      FurnitureCategory::SOFA,
      FurnitureCategory::DINING_SET
    ]
  }

  validates :room_category, inclusion: { in: RoomCategory.values }

  def complete?
    required_categories = REQUIRED_CATEGORIES[self.room_category]
    (required_categories - self.category_collections.map(&:category).uniq).empty?
  end

  def self.all_design_options
    if !@@design_options
      @@design_options = []

      Moodboard.includes(:category_collections => :furnitures).each do |moodboard|
        @@design_options += moodboard.design_options
      end
    end

    return @@design_options
  end

  def design_options
    furniture_tree = self.category_collections.map {|c| c.furnitures.to_a }
    furniture_products = furniture_tree[0].product(*furniture_tree[1..-1])

    _values = []

    furniture_products.each do |furnitures|
      _map = {}

      furnitures.each do |furniture|
        _map[furniture.category] = furniture
      end

      _values << _map
    end

    return _values
  end

  # def design_options_new
  #   _val = {}

  #   self.furnitures.each do |furniture|
  #     matching_furnitures = Furniture.all_furnitures.select { |f|
  #         f.category == furniture.category &&
  #         (f.colors & furniture.colors).any? &&
  #         (f.styles & furniture.colors).any?
  #     }
  #     _val = matching_furnitures.map{ |mf| x = {}; x[furniture.category.to_sym] = mf.id}}
  #   end

  #   return _val[0].product(_val[1..-1])
  # end

end
