class Inspiration < ActiveRecord::Base
  mount_base64_uploader :image, ImageUploaderBase

  has_many :inspiration_furnitures, dependent: :destroy
  has_many :furnitures, through: :inspiration_furnitures

  scope :published, -> { where(published: true) }

  def get_furnitures(category)
    self.furnitures.select {|f| f.category == category }
  end
end
