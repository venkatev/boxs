class FurnitureViewImageUploader < ImageUploaderBase
  process :trim

  private

  def trim
    manipulate! do |img|
      img.trim
    end
  end
end
