# == Schema Information
#
# Table name: furniture_layouts
#
#  id               :integer          not null, primary key
#  room_id          :integer
#  items_json       :json             default("[]")
#  created_at       :datetime
#  updated_at       :datetime
#  room_template_id :integer
#

class FurnitureLayout < ApplicationRecord
  include LayoutEngineManager

  belongs_to :room_template
  has_many :designs, dependent: :destroy

  attr_accessor :furniture_placements
  attr_reader :furniture_placements_1 #To convert and send the items_json in the format of array

  before_save  :update_items_json, :if => :furniture_placements

  def items
    self.items_json.map {|fi| FurnitureItem.new(fi) }
  end

  def items=(value)
    self.items_json = value.to_json
  end

  # TODO Optimize
  def next_design_option
    category_to_variant_map = self.items.group_by {|item| item.category }.map {|k,v| [k, v[0].variant_id] }.to_h
    all_design_options = Moodboard.all_design_options

    matching_design_options = all_design_options.select {|design_option|
      category_to_variant_map.all? {|category, variant_id|
        design_option[category] && design_option[category].variant_id == variant_id
      }
    }

    matching_design_options = matching_design_options.map do |design_option|
      item_mapping = {}

      self.items.each do |item|
        furniture = design_option[item.category]

        if furniture.nil?
          raise "No furniture found for category #{item.category}"
        end

        item_mapping[item.id] = furniture.attributes.slice("id", "dimensions", 'category', 'variant_id')
        item_mapping[item.id]['orientation'] = item.orientation
      end

      item_mapping
    end

    # Remove already visited designs.
    matching_design_options = matching_design_options.reject do |design_option|
      self.designs.any? {|d|
        d.item_mapping.values.map{|item| item['id']}.sort == design_option.values.map {|f| f['id'] }.sort
      }
    end

    # matching_design_options = matching_design_options.group_by {|d| d.values.map(&:id) }.keys
    return matching_design_options.sample
  end

  def update_items_json
    fp_arr = eval(self.furniture_placements)
    items = fp_arr.map {|fp|
      size = LEM.lookup_variant('living_room', fp[0])[:size]
      cell_range = fp[1].split('-').map {|x| "#{x}#{fp[2]}"}.join('-')
      {
        id: fp.join("_"),
        variant_id: fp[0],
        orientation: fp[2].to_i,
        cell_range: cell_range,
        alignments: [{"h_align"=>{"ref"=>"c", "value"=>"#{fp[3]}_#{fp[3]}"}, "v_align"=>{"ref"=>"w", "value"=>"b_b"}, "d_align"=>{"ref"=>"c", "value"=>"b_b"}}],
        size: size
      }
    };

    self.items_json = items
  end

  def furniture_placements_1
    self.items_json.map { |i|
      # Break the alignment into a single letter code. This part is fragile - whenever we make a change
      # in the convention, it is going to break. Works for now.
      alignment_code = i["alignments"][0]["h_align"]["value"].split("_")[0]
      cr_1, cr_2 = i["cell_range"].split("-")
      cr_1 = cr_1[0..-2] #Last character is the orientation. Skip it.
      cr_2 = cr_2[0..-2] #Last character is the orientation. Skip it.
      cr = "#{cr_1}-#{cr_2}"
      [i["variant_id"], cr, i["orientation"].to_s, alignment_code]
    }.to_s
  end

  def get_item(identifier)
    self.items.find {|item| item.id == identifier }
  end

  def get_item_by_category(category)
    self.items.find {|item| item.category == category }
  end

  def furniture_categories
    self.items.map(&:category).uniq
  end

  class FurnitureItem
    attr_accessor :id, :category, :variant_id, :size, :cell_range, :orientation, :alignments, :impact_rank

    def initialize(attrs)
      attrs = attrs.symbolize_keys
      self.id = attrs[:id] || SecureRandom.hex(8)
      self.category = attrs[:category]
      self.variant_id = attrs[:variant_id]
      self.size = attrs[:size]
      self.cell_range = attrs[:cell_range]
      self.alignments = attrs[:alignments]
      self.orientation = get_orientation
      self.impact_rank = attrs[:impact_rank] || 0
    end

    def category
      LayoutEngineManager.get_super_category_for_variant(self.variant_id)
    end

    def get_orientation
      self.cell_range.match(/^\d+(?:[A-Z]|[a-z])([1-4])?/)
      $1.to_i
    end

    def shell?
      # FIXME_FURNISH. room_type hard-coded.
      LEM.shell?('living_room', self.category)
    end

    def wall_or_floor_surface?
      self.category == 'wall_surface' || self.category == 'floor_surface'
    end

    def furnishable?
      !shell? && !wall_or_floor_surface?
    end

    def get_relative_position(ref_bounds, dimensions, alignment)
      dimensions = dimensions.symbolize_keys
      ref_bounds = ref_bounds.symbolize_keys

      if alignment == :h_center
        if self.orientation == 1 || self.orientation == 3
          delta_width = (ref_bounds[:max_x] - ref_bounds[:min_x]) - dimensions[:width]
          return { min_x: (ref_bounds[:min_x] + delta_width / 2.0).round(2), max_x: (ref_bounds[:max_x] - delta_width / 2.0).round(2) }

        else
          delta_width = (ref_bounds[:max_z] - ref_bounds[:min_z]) - dimensions[:width]
          return { min_z: (ref_bounds[:min_z] + delta_width / 2.0).round(2), max_z: (ref_bounds[:max_z] - delta_width / 2.0).round(2) }
        end

      elsif alignment == :v_center
        delta_height = (ref_bounds[:max_y] - ref_bounds[:min_y]) - dimensions[:height]
        return { min_y: (ref_bounds[:min_y] + delta_height / 2.0).round(2), max_y: (ref_bounds[:max_y] - delta_height / 2.0).round(2) }

      elsif alignment == :d_center
        if self.orientation == 1 || self.orientation == 3
          delta_depth = (ref_bounds[:max_z] - ref_bounds[:min_z]) - dimensions[:depth]
          return { min_z: (ref_bounds[:min_z] + delta_depth / 2.0).round(2), max_z: (ref_bounds[:max_z] - delta_depth / 2.0).round(2) }

        else
          delta_depth = (ref_bounds[:max_x] - ref_bounds[:min_x]) - dimensions[:depth]
          return { min_x: (ref_bounds[:min_x] + delta_depth / 2.0).round(2), max_x: (ref_bounds[:max_x] - delta_depth / 2.0).round(2) }
        end

      elsif alignment == :left
        if self.orientation == 1
          return { min_x: ref_bounds[:min_x], max_x: ref_bounds[:min_x] + dimensions[:width] }

        elsif self.orientation == 2
          return { min_z: ref_bounds[:min_z], max_z: ref_bounds[:min_z] + dimensions[:width] }

        elsif self.orientation == 3
          return { min_x: ref_bounds[:max_x] - dimensions[:width], max_x: ref_bounds[:max_x] }

        elsif self.orientation == 4
          return { min_z: ref_bounds[:max_z] - dimensions[:width], max_z: ref_bounds[:max_z] }
        end

      elsif alignment == :right
        if self.orientation == 1
          return { min_x: ref_bounds[:max_x] - dimensions[:width], max_x: ref_bounds[:max_x] }

        elsif self.orientation == 2
          return { min_z: ref_bounds[:max_z] - dimensions[:width], max_z: ref_bounds[:max_z] }

        elsif self.orientation == 3
          return { min_x: ref_bounds[:min_x], max_x: ref_bounds[:min_x] + dimensions[:width] }

        elsif self.orientation == 4
          return { min_z: ref_bounds[:min_z], max_z: ref_bounds[:min_z] + dimensions[:width] }
        end

      elsif alignment == :top
        return { min_y: ref_bounds[:max_y] - dimensions[:height], max_y: ref_bounds[:max_y] }

      elsif alignment == :bottom
        return { min_y: ref_bounds[:min_y], max_y: ref_bounds[:min_y] + dimensions[:height] }

      elsif alignment == :back
        if self.orientation == 1
          return { min_z: ref_bounds[:min_z], max_z: ref_bounds[:min_z] + dimensions[:depth] }

        elsif self.orientation == 2
          return { min_x: ref_bounds[:max_x] - dimensions[:depth], max_x: ref_bounds[:max_x] }

        elsif self.orientation == 3
          return { min_z: ref_bounds[:max_z] - dimensions[:depth], max_z: ref_bounds[:max_z] }

        elsif self.orientation == 4
          return { min_x: ref_bounds[:min_x], max_x: ref_bounds[:min_x] + dimensions[:depth] }
        end

      elsif alignment == :front
        if self.orientation == 1
          return { min_z: ref_bounds[:max_z] - dimensions[:depth], max_z: ref_bounds[:max_z] }

        elsif self.orientation == 2
          return { min_x: ref_bounds[:min_x], max_x: ref_bounds[:min_x] + dimensions[:depth] }

        elsif self.orientation == 3
          return { min_z: ref_bounds[:min_z], max_z: ref_bounds[:min_z] + dimensions[:depth] }

        elsif self.orientation == 4
          return { min_x: ref_bounds[:max_x] - dimensions[:depth], max_x: ref_bounds[:max_x] }
        end
      end
    end

    def get_absolute_position(bounds)
      bounds = bounds.map {|key, value| [ key, (value * 305).round ] }.to_h

      if orientation == 1
        return [bounds[:min_x], bounds[:min_y], bounds[:min_z]]
      elsif orientation == 2
        return [bounds[:max_x], bounds[:min_y], bounds[:min_z]]
      elsif orientation == 3
        return [bounds[:max_x], bounds[:min_y], bounds[:max_z]]
      elsif orientation == 4
        return [bounds[:min_x], bounds[:min_y], bounds[:max_z]]
      else
        raise "Invalid Orientation"
      end
    end

    def place_with_alignments!(furniture, ref_bounds, alignments)
      bounds = {}

      alignments.each do |alignment|
        bounds.merge!(get_relative_position(ref_bounds, alignment))
      end

      self.place!(bounds)
    end

    def avg_height
      ((self.min_height + self.max_height) / 2).round
    end

    def avg_width
      ((self.min_width + self.max_width) / 2).round
    end

    def avg_depth
      ((self.min_depth + self.max_depth) / 2).round
    end

    def avg_y_position
      self.size['height'] =~ /@(.+)/
      return 0 if !$1
      ($1.split('-').map(&:to_f).sum / 2.0).ceil
    end

    def min_height
      self.min_height_ft * 305
    end

    def max_height # height = "2-3@1-1.5"
      self.max_height_ft * 305
    end

    def min_width
      self.min_width_ft * 305
    end

    def max_width
      self.max_width_ft * 305
    end

    def min_depth
      self.min_depth_ft * 305
    end

    def max_depth
      self.max_depth_ft * 305
    end

    def min_height_ft
      self.size['height'].split("-")[0].to_i
    end

    def max_height_ft # height = "2-3@1-1.5"
      self.size['height'].split("-")[1].split("@")[0].to_i
    end

    def min_width_ft
      self.size['width'].split("-")[0].to_i
    end

    def max_width_ft
      self.size['width'].split("-")[1].to_i
    end

    def min_depth_ft
      self.size['depth'].split("-")[0].to_i
    end

    def max_depth_ft
      self.size['depth'].split("-")[1].to_i
    end
  end
end
