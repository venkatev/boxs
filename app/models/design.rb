# == Schema Information
#
# Table name: designs
#
#  id                  :integer          not null, primary key
#  furniture_layout_id :integer
#  moodboard_id        :integer
#  item_mapping        :json             default("{}")
#  created_at          :datetime
#  updated_at          :datetime
#  room_id             :integer
#

require Rails.root + 'lib/layout_engine_manager'

class Design < ApplicationRecord
  belongs_to :room
  belongs_to :furniture_layout
  belongs_to :moodboard

  has_many :views, -> { order(:id) }, class_name: DesignView.name, :dependent => :destroy

  validates :item_mapping, :presence => true

  scope :for_layout, -> (furniture_layout_id) { where(furniture_layout_id: furniture_layout_id) }
  default_scope { order('designs.id ASC').includes(:views => :camera_view) }

  before_create :position_furnitures
  after_create :generate_views

  delegate :room_template, :furniture_categories, to: :furniture_layout

  # Generates one design view per camera view.
  def generate_views
    self.views = []

    # TEMP.
    self.room_template.camera_views[0..0].each do |camera_view|
      self.views.create!(camera_view: camera_view)
    end

    reload
    render_all_views
  end

  def render_all_views
    self.views.each do |view|
      render_view!(view)
    end
  end

  def render_view!(design_view, force = false)
    # Already rendered. Skip unless forced to render.
    if design_view.rendered? && !force
      return design_view
    end

    viewPort = { width: 1000, height: 675 }
    filtered_mappings = self.item_mapping.reject{|a,b| %w(floor_surface).include?(b['category'])}

    furniture_items = filtered_mappings.map {|furniture_item_id, furniture_details|
      furniture = furniture_details.slice("id", "position", "dimensions", "orientation", 'category', 'boxs_variant_id', 'price', 'bounds_map')
      o = furniture['orientation']
      dimensions = furniture['dimensions']
      h = dimensions['height'].to_i
      w = dimensions['width'].to_i
      d = dimensions['depth'].to_i

      pos_x = furniture['position'][0]
      pos_y = furniture['position'][1]
      pos_z = furniture['position'][2]

      min_x = max_x = min_z = max_z = 0
      min_y = pos_y
      max_y = pos_y + h

      case o
      when 1
        # No changes in position
        min_x = pos_x
        max_x = pos_x + w
        min_z = pos_z
        max_z = pos_z + d

      when 2
        min_x = pos_x - d
        max_x = pos_x
        min_z = pos_z
        max_z = pos_z + w

      when 3
        min_x = pos_x - w
        max_x = pos_x
        min_z = pos_z - d
        max_z = pos_z

      when 4
        min_x = pos_x
        max_x = pos_x + d
        min_z = pos_z - w
        max_z = pos_z
      end

      if furniture['id'].to_i == 955
        # FIXME
        position_bounds = {
          min_x: 0, max_x: 4270, min_y: 0, max_y: 2898, min_z: 0, max_z: 2
        }
      else
        position_bounds = {
          min_x: min_x, max_x: max_x, min_y: min_y, max_y: max_y, min_z: min_z, max_z: max_z
        }
      end

      furniture.merge!(position_bounds: position_bounds)

      [
        furniture_item_id,
        furniture
      ]
    }.to_h

    render_url = Services::CHRODE + "/designs/#{self.id}/render/#{design_view.camera_view.id}"
    puts "Rendering - #{Time.now} - #{render_url}"

    payload = {
      catalog_base_url: Rails.env.development? ? "http://localhost:3001" : 'https://boxs.in',
      room: {
        room_type: self.room_template.room_category,
        length: self.room_template.length,
        breadth: self.room_template.breadth
      },
      camera_view: design_view.camera_view.attributes
        .merge('image_url' => Rails.env.development? ? ('http://localhost:3001' + design_view.camera_view.image.url) : design_view.camera_view.image.url)
        .merge(viewport: { width: 1500, height: 1500 }), # TODO Read from the image.
      furniture_items: furniture_items
    }

    puts payload.to_json
    response = Faraday.post(render_url, { payload: payload.to_json })

    if response.status != 200
      puts response.body
      raise "Render failed."
    else
      puts "Done - #{Time.now}"
      render_data = JSON.parse(response.body)
    end

    design_view.update(image: render_data['image'], pixel_map: render_data['bounds_map'], deferred: false)

    return design_view
  end

  def position_furnitures
    self.item_mapping.values.each do |furniture|
      furniture['dimensions_ft'] = {
        'height' => (furniture['dimensions']['height'].to_i / 305.0),
        'width' => (furniture['dimensions']['width'].to_i / 305.0),
        'depth' => (furniture['dimensions']['depth'].to_i / 305.0)
      }
    end

    grouped_items = self.furniture_layout.items.group_by {|fi| [ fi.category, fi.cell_range ] }

    self.furniture_layout.items.each_with_index do |furniture_item, index|
      # FIXME_INTEGRATION. Commented the following line during merge.
      furniture = self.item_mapping[furniture_item.id]

      if furniture.nil?
        raise "No furniture found for item #{furniture_item.id}/#{furniture_item.category}/#{furniture_item.variant_id} in Layout##{self.id}"
      end

      bounds = {}

      furniture_item.alignments.each do |alignment|
        if alignment['reference_item_id']
          reference_item_furniture = self.item_mapping[alignment['reference_item_id']]
        end

        y_position = LEM.get_y_position(self.room_template.room_category, furniture_item.variant_id)

        # Default v_align from the config.
        alignment['v_align'] ||= {
          'ref' => 'w',
          'value' => "b_b",
          'offset' => y_position
        }

        bounds.merge!(compute_bounds(furniture_item, furniture, alignment, 'h_align', reference_item_furniture))
        bounds.merge!(compute_bounds(furniture_item, furniture, alignment, 'd_align', reference_item_furniture))
        bounds.merge!(compute_bounds(furniture_item, furniture, alignment, 'v_align', reference_item_furniture))
      end

      category = LEM.get_super_category_for_variant(furniture_item.variant_id)

      # FIXME
      if category == 'painting'
        bounds[:min_y] = 4
        bounds[:max_y] = 4 + furniture['dimensions_ft']['height']
      elsif category == 'wall_shelf'
        bounds[:min_y] = 4
        bounds[:max_y] = 4 + furniture['dimensions_ft']['height']
      end

      if bounds[:min_x].blank? || bounds[:max_x].blank? || bounds[:min_y].blank? || bounds[:max_y].blank? || bounds[:min_z].blank? || bounds[:max_z].blank?
        raise LayoutPositioningError.new("Invalid bounds - #{bounds.inspect}")
      end

      furniture_attrs = furniture.clone

      self.item_mapping[furniture_item.id] = furniture_attrs.merge(
        bounds: bounds,
        position: furniture_item.get_absolute_position(bounds),
        orientation: furniture_item.orientation
      )
    end

    item_mapping_will_change!
  end

  def get_furniture(item_identifier)
    Furniture.lookup(self.item_mapping[item_identifier]['id'])
  end

  # Returns a new design with the given item_identifier replaced with new_furniture.
  def change_item(item_identifier, new_furniture)
    # Get the layout that would accommodate the new furniture.
    layouts = self.room.room_template.find_layouts_for(new_furniture)
    layout = layouts.include?(self.furniture_layout) ? self.furniture_layout : layouts[0]
    new_item_mapping = {}

    # Construct the new item mapping from the current one by lookup based on the category.
    self.item_mapping.each do |identifier, item_entry|
      furniture_category = Furniture.lookup(item_entry['id']).category
      item = layout.get_item_by_category(furniture_category)

      if item_identifier == identifier
        new_item_mapping[item.id] = new_furniture.slice("id", "dimensions", 'category', 'variant_id')
      else
        new_item_mapping[item.id] = item_entry
      end
    end

    new_design = self.room.designs.create!(furniture_layout: layout, item_mapping: new_item_mapping).reload
    return new_design
  end

  def model_and_color_options(furniture)
    variant_ids = Set.new

    self.room.room_template.furniture_layouts.each do |furniture_layout|
      item = furniture_layout.get_item_by_category(furniture.category)
      variant_ids << item.variant_id if item
    end

    furnitures = Furniture.all_furnitures.select {|f| variant_ids.include?(f.variant_id) }
    color_options = furnitures.select {|f| f.color_id != furniture.color_id }.group_by(&:color_id).values.map(&:first)
    model_options = furnitures.select {|f| f.model_id != furniture.model_id }.group_by(&:model_id).values.map(&:first)

    return {
      color_options: color_options,
      model_options: model_options
    }
  end

  def other_options(furniture)
    variant_ids = []

    self.room.room_template.furniture_layouts.each do |furniture_layout|
      item = furniture_layout.get_item_by_category(furniture.category)
      variant_ids << item.variant_id if item
    end

    variant_ids.uniq!

    puts "**********"
    puts variant_ids.to_a.inspect
    furnitures = Furniture.all_furnitures.select {|f|
      variant_ids.include?(f.variant_id) && f != furniture
    }

    return furnitures
  end

  def furnitures
    furniture_ids = self.item_mapping.values.map {|entry| entry['id'] }
    return furniture_ids.map {|id| Furniture.find(id) }
  end

  private

  def compute_bounds(furniture_item, furniture, alignments, direction, ref_furniture)
    alignment = alignments[direction]

    side = {
      'h_align' => {
        'l_l' => :left,
        'r_r' => :right,
        'c_c' => :h_center
      },
      'v_align' => {
        'b_b' => :bottom,
        't_t' => :top,
        'c_c' => :v_center
      },
      'd_align' => {
        'b_b' => :back,
        'f_f' => :front,
        'c_c' => :d_center
      }
    }[direction][alignment['value']]

    if alignment['ref'] == 'p'
      # Reference based alignment.
      return furniture_item.get_relative_position(ref_furniture[:bounds], furniture['dimensions_ft'], side)

    elsif alignment['ref'] == 'w'
      bounds = furniture_item.get_relative_position( { min_y: 0, max_y: self.room_template.height }, furniture['dimensions_ft'], side)
      bounds = bounds.map {|key, value| [ key, value + (alignment['offset'] || 0)] }.to_h
      return bounds

    elsif alignment['ref'] == 'c'
      bounds = furniture_item.get_relative_position(get_bounds(furniture_item), furniture['dimensions_ft'], side)
      bounds = bounds.map {|key, value| [ key, value + (alignment['offset'] || 0)] }.to_h
    else
      raise "Unknown reference: #{alignment['ref']}"
    end
  end

  def get_bounds(furniture_item)
    all_bounds = get_cells(furniture_item.cell_range).map {|c| get_cell_bounds(c) }

    return {
      min_x: all_bounds.map {|b| b[:min_x] }.min,
      max_x: all_bounds.map {|b| b[:max_x] }.max,
      min_z: all_bounds.map {|b| b[:min_z] }.min,
      max_z: all_bounds.map {|b| b[:max_z] }.max
    }
  end

  def get_cells(placement)
    # Get the cells in order first.
    # placement = placement.split('-').sort.join('-')
    placement.match(/^(\d+)([A-Z]|[a-z])([1-4])-(\d+)([A-Z]|[a-z])[1-4]/)
    start_row = $1
    start_col = $2
    orientation = $3
    end_row = $4
    end_col = $5

    rows = (start_row..end_row).to_a
    cols = (start_col..end_col).to_a

    cells = rows.product(cols).map {|e| "#{e.join('')}#{orientation}" }
    return cells
  end

 def get_cell_bounds(cell)
    cell.match(/^(\d+)([A-Z]|[a-z])[1-4]$/)
    cell_width = LEM::CELL_SIZE
    cell_height = LEM::CELL_SIZE

    start_x = LEM::COLS.index($2) * cell_width
    end_x = start_x + cell_width
    start_y = ($1.to_i - 1) * cell_height
    end_y = start_y + cell_height

    return {
      min_x: [start_x.round(2), 0].max,
      max_x: [end_x.round(2), self.room_template.length].min,
      min_z: [start_y.round(2), 0].max,
      max_z: [end_y.round(2), self.room_template.breadth].min
    }
  end
end
