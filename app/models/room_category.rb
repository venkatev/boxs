class RoomCategory
  include Ruby::Enum

  define :LIVING_ROOM,      'living_room'
  define :MASTER_BEDROOM,   'master_bedroom'
  define :GUEST_BEDROOM,    'guest_bedroom'
  define :KIDS_BEDROOM,     'kids_bedroom'
  define :PARENTS_BEDROOM,  'parents_bedroom'
  define :STUDY_ROOM,       'study_room'
  define :KITCHEN,          'kitchen'
  define :DINING,           'dining'
end
