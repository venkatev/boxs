# == Schema Information
#
# Table name: category_collections
#
#  id         :integer          not null, primary key
#  category   :string
#  created_at :datetime
#  updated_at :datetime
#

class CategoryCollection < ApplicationRecord
  has_many :category_collection_furnitures, dependent: :destroy
  has_many :furnitures, through: :category_collection_furnitures
  has_many :moodboard_category_collections, dependent: :destroy

  validates :category, inclusion: { in: FurnitureCategory.values }
end
