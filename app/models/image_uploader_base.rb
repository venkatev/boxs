# encoding: utf-8

class ImageUploaderBase < FileUploaderBase
    # Include RMagick or MiniMagick support:
    # include CarrierWave::RMagick
    include CarrierWave::MiniMagick
end
