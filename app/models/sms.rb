class Sms
  include ActiveModel::Serialization
  attr_accessor :to, :from, :message, :status_code, :status_message

  def initialize(attributes = {})
    @to = attributes[:to]
    @message = attributes[:message]
    @status_code = @status_message = nil
  end

  # persisted is important not to get "undefined method `to_key' for" error
  def persisted?
    false
  end

  def send
    Rails.logger.debug("Sending SMS...to: #{@to}, message: #{@message}")

    # Send sms only in production mode.
    if Rails.env != "production"
      Rails.logger.debug("Stubbing SMS in non-production environment...to: #{@to}, message: #{@message}")
      return
    end

    twilio_client = Twilio::REST::Client.new(
      Rails.application.secrets.twilio_account_sid,
      Rails.application.secrets.twilio_auth_token
    )

    twilio_client.messages.create(
      from: Rails.application.secrets.twilio_from_phone_number,
      to: prepend_country_code(@to),
      body: @message
    )
  end

  def prepend_country_code(mobile)
    # FIXME: Enhance the logic
    raise if !mobile || mobile.length > 13 || mobile.length < 10
    return mobile if mobile.length == 13 && mobile.starts_with?("+91")
    return "+91#{mobile}"
  end

end