class FurnitureCategory
  include Ruby::Enum

  define :SOFA, 'sofa_set'
  define :DINING_SET, 'dining_set'
  define :CHEST_OF_DRAWERS, 'chest_of_drawers'
  define :BOOKSHELF, 'book_shelf'
  define :CONSOLE_TABLE, 'console_table'
  define :TV_UNIT, 'tv_unit'
  define :SIDEBOARD, 'sideboard'
  define :WALL_SURFACE, 'wall_surface'
  define :CROCKERY_UNIT, 'crockery_unit'
  define :FOYER_CHEST, 'foyer_chest'
  define :PAINTING, 'painting'
end
