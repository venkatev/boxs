# == Schema Information
#
# Table name: furnitures
#
#  id                :integer          not null, primary key
#  category          :string
#  variant_id        :string
#  created_at        :datetime
#  updated_at        :datetime
#  identifier        :string
#  required_captures :integer          default("0")
#  dimensions        :json
#  model             :string
#  data_errors       :text
#  version           :integer          default("0")
#  styles            :string           default("{}"), is an Array
#  colors            :string           default("{}"), is an Array
#
# encoding: utf-8

require 'zip'

class Furniture < ApplicationRecord
  class ModelUploader < FileUploaderBase; end;

  ANGLE_INTERVAL = 10

  has_many :textures, dependent: :destroy
  has_many :furniture_views, dependent: :destroy
  has_many :category_collection_furnitures, dependent: :destroy
  has_many :category_collections, through: :category_collection_furnitures

  has_many :inspiration_furnitures
  has_many :inspirations, through: :inspiration_furnitures

  attr_accessor :zipped_model_file

  after_create  :update_model, :if => :zipped_model_file
  after_update  :update_model, :if => lambda { self.zipped_model_file.present? && !model_changed? }
  before_create :set_required_captures
  validate      :check_dimensions
  validates     :identifier, presence: true, uniqueness: { case_sensitive: false }
  validate      :check_pending_captures, on: :update, :if => lambda { published? && published_changed? }

  mount_base64_uploader :model, ModelUploader
  mount_base64_uploader :inspiration, FurnitureInspirationImageUploader

  scope :published, -> { where(published: true) }
  default_scope { where(archived: false) }

  before_create :set_default_lights

  def update_model
    # Create a temp file and write the zip contents to it.
    tmp_zip_file_name = SecureRandom.hex
    tmp_zip_file = Tempfile.new(tmp_zip_file_name + ".zip")

    begin
      if self.zipped_model_file.respond_to?(:path)
        tmp_zip_file.write(File.read(self.zipped_model_file.path))
      else
        tmp_zip_file.write(self.zipped_model_file.read)
      end

      tmp_zip_file.close

      # Use the same random name as the folder to extract the contents onto.
      model_folder_path = tmp_zip_file_name
      FileUtils.mkdir(model_folder_path)

      # Extract using the unzip command.
      `unzip #{tmp_zip_file.path} -d #{model_folder_path}`

      # The unzipped contents will have a first level directory. Get past that.
      model_folder_path = Dir[model_folder_path + "/*"].reject {|e| e =~ /MACOSX/}[0]
      puts "Importing #{model_folder_path}"
      model_file_name = model_folder_path.to_s + '/model.json'

      if !File.exists?(model_file_name)
        errors.add(:model, "is required. Check whether you've model.json")
        return false
      end

      model_json = JSON.parse(File.read(model_file_name))

      model_json['images'].each_with_index do |image_entry, index|
        # The outer folder and inner folder (containing textures) names should be same.
        image_file_name = "#{model_folder_path.to_s}/#{image_entry['url']}"

        if !File.exists?(image_file_name)
          errors.add(:model, "has missing texture file #{image_entry['url']}")
          return false
        end

        relative_file_name = image_file_name.split('/')[-1]

        _texture = self.textures.create!(
          name: relative_file_name,
          image: File.open(image_file_name)
        )

        if Rails.env.development?
          image_entry['url'] = 'http://localhost:3001' + _texture.image.url
        else
          image_entry['url'] = _texture.image.url
        end
      end

      model_file = Tempfile.new(['model', '.json'])
      model_file.write(model_json.to_json)

      self.update model: model_file
    ensure
      tmp_zip_file.delete
      # Remove the temp file and folder.
      model_file.delete if model_file
      FileUtils.rm_rf(model_folder_path) if model_folder_path
    end
  end

  def self.import_from_production!(identifier)
    # url = "http://localhost:3001/furnitures/all?token=1b0bcf3e70fbc94d921e938bd72b7ae2"
    url = "https://boxs.in/furnitures/all?token=1b0bcf3e70fbc94d921e938bd72b7ae2"
    furnitures_data = JSON.parse(Faraday.get(url).body)

    furnitures_data.each do |furniture_info|
      if identifier && furniture_info['identifier'] != identifier
        next
      end

      furniture = Furniture.includes(:furniture_views).find_by_identifier(furniture_info['identifier'])

      if furniture.nil?
        furniture = Furniture.new(furniture_info.except('id', 'views'))
        furniture.save!
        puts "#{furniture_info['identifier']} > Creating furniture"
      else
        # Uncomment for updating meta.
        furniture.update furniture_info.except('id', 'views') rescue nil
        # next
        # puts "#{furniture_info['identifier']} > Exists"
      end

      views_to_be_created = 0

      furniture_info['views'].each do |view|
        furniture_view = furniture.furniture_views.to_a.find {|fv| fv.theta == view['theta'] && fv.r == view['r'] }

        if !furniture_view
          views_to_be_created += 1
          furniture_view = furniture.furniture_views.build(theta: view['theta'], r: view['r'], remote_image_url: view['image_url'])
        end
      end

      if !views_to_be_created.zero?
        puts "\t Adding #{views_to_be_created} views"
        furniture.save!
      end
      # return
    end

    return true
  end

  def self.all_data
    data = []

    Furniture.includes(:furniture_views).published.each do |furniture|
      views_data = furniture.furniture_views.map do |fv|
        {
          theta: fv.theta,
          r: fv.r,
          image_url: fv.image.url
        }
      end

      data << {
        id: furniture.id,
        identifier: furniture.identifier,
        variant_id: furniture.variant_id,
        dimensions: furniture.dimensions,
        color_id: furniture.color_id,
        model_id: furniture.model_id,
        views: views_data
      }
    end

    return data
  end

  def sub_category
    return nil if new_record? || self.variant_id.nil?
    LayoutEngineManager.get_category_for_variant(self.variant_id)
  end

  def lem_y_position
    # FIXME - hardcoding the room_type. LEM has to be modified to depend only on variant_id
    LayoutEngineManager.get_y_position('living_room', self.variant_id)
  end

  def category
    return nil if new_record? || self.variant_id.nil?
    LayoutEngineManager.get_super_category_for_variant(self.variant_id)
  end

  def attached_to_wall?
    # false
    attached_to_wall_map = LayoutEngineManager::FURNISH_CONFIG['living_room'][:categories].select{ |cat, val| val[:attached_to_wall]}
    variants_attached_to_wall = attached_to_wall_map.map{|cat, details| details[:variants].map{|v| v[:id]} }.flatten
    return variants_attached_to_wall.include?(self.variant_id)
  end

  def attached_to_ceiling?
    # FIXME
    false
  end

  def captured_percentage
    ((self.furniture_views.count / get_capture_angles.size.to_f) * 100).round
  end

  def captured_count
    self.catalog_images_count
  end

  def pending_captures_count
    self.pending_capture_angles.count
  end

  # Uses spherical angles.
  def get_capture_angles
    # TODO
    # min_r = ((1500 - self.dimensions['height'] / 2) / 1000).ceil * 1000

    theta_range = self.attached_to_wall? ? (-90..90) : (0..360)
    thetas = [*(theta_range).step(ANGLE_INTERVAL)].map {|t| (t + 360) % 360 }
    angles = thetas.map{|a| { theta: a }}
    distances =  (2..6).to_a.map {|d| { r: d * 1000 } }
    return distances.product(angles).map {|items| items[0].merge(items[1])}.uniq
  end

  def pending_capture_angles(force_all=false)
    # # All angles have been captures
    # return [] if !force_all && pending_captures_count <= 0
    _capture_angles = get_capture_angles
    _furniture_views = self.furniture_views

    # Filter and send only the angles that have not been captured yet.
    return force_all ? _capture_angles : _capture_angles.reject { |a|
      _furniture_views.find {|fv| fv[:theta] == a[:theta] && fv[:r] == a[:r] }
    }
  end

  def set_required_captures
    self.required_captures = get_capture_angles.size
  end

  def has_data_errors?
    self.data_errors.present?
  end

  def update_data_errors(message)
    if message.nil?
      update_column(:data_errors, nil)
    else
      new_message = message
      new_message = self.data_errors + ", " + new_message if self.data_errors.present?

      # Don't want the validation to be fired here. Otherwise we get the dimension check.
      update_column(:data_errors, new_message)
    end
  end

  def remove_furnitures(identifiers)

  end

  def self.all_furnitures
    if !@all
      @all = Furniture.all.select(&:published?)
    end

    return @all
  end

  def self.clear_cache!
    @all = nil
  end

  def self.lookup(id)
    all_furnitures.find {|f| f.id == id }
  end

  def self.import_from_local!
    imported_count = 0
    furniture = nil
    begin
      furniture_entries = CSV.parse(File.read(Rails.root + 'data/furnitures.csv'), headers: true)
      furniture_entries.each do |fe|
        ActiveRecord::Base.transaction do
          identifier = [fe["id"], fe["style"], fe["category"]].compact.join('_')
          folder_path = Rails.root + "data/furnitures/" + identifier
          lem_id = fe["lem_id"]
          version = fe["version"]
          model_id = fe["model_id"]
          color_id = fe["color_id"]
          # furniture_dimensions = {height: fe["height"], width: fe["width"], depth: fe["depth"]}

          furniture = Furniture.find_by_identifier(identifier)
          if furniture
            if furniture.has_data_errors?
              furniture.destroy!
              puts "[EXISTS WITH ERROR]: #{furniture.id}. Removing furniture."
            else
              puts "[EXISTS]: #{furniture.id}. Skipping..."
              next
            end
          end

          puts "Importing #{folder_path}"
          furniture = Furniture.create!(
            identifier: identifier,
            variant_id: lem_id,
            model_id: model_id,
            color_id: color_id,
            # dimensions: furniture_dimensions,
            version: version
          )
          model_file_name = folder_path.to_s + '/model.json'
          if !File.exists?(model_file_name)
            furniture.update_data_errors("Model file missing #{model_file_name}")
            puts "[ERROR] Model file missing #{model_file_name}"
            next
          end

          if !LayoutEngineManager.lookup_variant('living_room', lem_id)
            furniture.update_data_errors("LEM id #{lem_id} not found")
            puts "LEM id #{lem_id} not found"
            next
          end


          model_json = JSON.parse(File.read(model_file_name))

          if model_json['images'].size > 50
            furniture.update_data_errors("Too many textures (#{model_json['images'].size})")
            puts "[ERROR] Too many textures (#{model_json['images'].size})"
            next
          end

          model_json['images'].each_with_index do |image_entry, index|
            # The outer folder and inner folder (containing textures) names should be same.
            image_file_name = "#{folder_path.to_s}/#{image_entry['url']}"

            if !File.exists?(image_file_name)
              furniture.update_data_errors("Texture file missing #{image_file_name}")
              puts "[ERROR] Texture file missing #{image_file_name}"
              next
            end

            relative_file_name = image_file_name.split('/')[-1]
            _texture = furniture.textures.create!(
              name: relative_file_name,
              image: File.open(image_file_name)
            )

            if Rails.env.development?
              image_entry['url'] = 'http://localhost:3001' + _texture.image.url
            else
              image_entry['url'] = _texture.image.url
            end
          end

          model_file = Tempfile.new(['model', '.json'])
          model_file.write(model_json.to_json)

          furniture.update! model: model_file

          imported_count = imported_count + 1
          puts "[IMPORTED]: #{furniture.id}"
        end #Transaction end
      end #Loop end

      new_entries = furniture_entries.map {|fe| "#{fe["id"]}_#{fe["style"]}_#{fe["category"]}"}
      existing_entries = Furniture.pluck(:identifier)
      delete_entries = existing_entries - new_entries
      # Do we need to remove? This will require us to have the excel and the system to be in sync.
      # The entries in the system can be managed from the system itself, and not from excel.
      puts "[Entries in DB and not in Excel]: #{delete_entries}"

    rescue => e
      furniture.update_data_errors(e.message) if furniture && !furniture.new_record?
      puts "[ERROR]: #{e.message}"
      puts e.backtrace
    end
    puts "############################################"
    puts "Total Furniture Imported: #{imported_count}"
  end

  def lem_dimensions
    LayoutEngineManager.lookup_variant('living_room', self.variant_id)[:size]
  end

  def max_height
    (lem_dimensions[:height].split('-')[1].to_f * 305.0).to_i
  end

  def min_height
    (lem_dimensions[:height].split('-')[0].to_f * 305.0).to_i
  end

  def max_width
    (lem_dimensions[:width].split('-')[1].to_f * 305.0).to_i
  end

  def min_width
    (lem_dimensions[:width].split('-')[0].to_f * 305.0).to_i
  end

  def max_depth
    (lem_dimensions[:depth].split('-')[1].to_f * 305.0).to_i
  end

  def min_depth
    (lem_dimensions[:depth].split('-')[0].to_f * 305.0).to_i
  end

  def height
    return nil if dimensions.nil? || dimensions["height"].nil?
    dimensions["height"].to_i
  end

  def width
    return nil if dimensions.nil? || dimensions["width"].nil?
    dimensions["width"].to_i
  end

  def depth
    return nil if dimensions.nil? || dimensions["depth"].nil?
    dimensions["depth"].to_i
  end

  def within_allowed_range?(value, min, max)
    # Allow up to 50mm aberration.
    threshold = 50

    if value < min
      return (min - value) <= threshold

    elsif value > max
      return (value - max) <= threshold

    else
      return true
    end
  end

  def check_dimensions(force = false)
    # Let's now worry about the dimensions when we are creating.
    if (persisted? && dimensions_changed?) || force
      errors.add(:dimensions, "height is missing") if height.zero?

      # Now we are not validating the height
      # if !height.between?(min_height, max_height)
      #   self.errors.add(:dimensions, "Height out of range")
      # end

      if !within_allowed_range?(width, min_width, max_width)
        self.errors.add(:dimensions, "Width out of range. Got: #{width}. Expected Range: #{min_width} to #{max_width}. #{category}/#{self.variant_id}")
      end

      if !within_allowed_range?(depth, min_depth, max_depth)
        self.errors.add(:dimensions, "Depth out of range. Got: #{depth}. Expected Range: #{min_depth} to #{max_depth}. #{category}/#{self.variant_id}")
      end
    end
  end

  def image_url
    sample_view = self.furniture_views.of_r(3000).of_theta(30.0).first
    sample_view&.image&.url
  end

  def check_pending_captures
    if pending_captures_count != 0
      errors.add(:published, "needs all views to be captured")
    end
  end

  def set_default_lights
    self.lights = [
      { type: 'ambient', intensity: 0.3, angle: 0.7, penumbra: 0 },
      { type: 'spot', intensity: 1, position: { x: 0, y: 3000, z: 3000 }, angle: Math::PI / 3, penumbra: 0 },
      { type: 'spot', intensity: 0, position: { x: 0, y: 3000, z: 3000 }, angle: Math::PI / 3, penumbra: 0 },
      { type: 'spot', intensity: 0, position: { x: 0, y: 3000, z: 3000 }, angle: Math::PI / 3, penumbra: 0 }
    ]
  end
end
