# == Schema Information
#
# Table name: textures
#
#  id           :integer          not null, primary key
#  name         :string
#  furniture_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#  image        :string
#

class Texture < ApplicationRecord
  belongs_to :furniture
  mount_base64_uploader :image, TextureImageUploader
end
