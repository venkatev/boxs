# == Schema Information
#
# Table name: camera_views
#
#  id               :integer          not null, primary key
#  room_template_id :integer
#  position         :json             default("{}")
#  target           :json             default("{}")
#  primary          :boolean
#  image            :string
#  created_at       :datetime
#  updated_at       :datetime
#

class CameraView < ActiveRecord::Base
  belongs_to :room_template

  validates :room_template,
            :position,
            :target,
            :presence => true

  default_scope { order('id ASC') }

  mount_base64_uploader :image, CameraViewImageUploader

  def to_sketchup
    puts <<-DOC
      view = Sketchup.active_model.active_view;
      mc = Sketchup::Camera.new;
      mc.fov = 90;
      mc.set([#{self.position['z'] / 25.4}, #{self.position['x'] / 25.4}, #{self.position['y'] / 25.4}], [#{self.target['z'] / 25.4}, #{self.target['x'] / 25.4}, #{self.target['y'] / 25.4}], [0, 0, 1]);
      view.camera = mc;
    DOC

    return nil
  end

  # Returns the Boxs camera view equivalent of the given view.
  # To be run in Sketchup console.
  def to_boxs
    require 'json'
    view = Sketchup.active_model.active_view
    camera = view.camera
    camera.fov = 90
    camera.set([camera.eye.x, camera.eye.y, 60], [camera.target.x, camera.target.y, 60], [0, 0, 1])
    view.camera = camera

    # y and z are flipped in Sketchup.
    puts "Position"
    puts({ x: camera.eye.y.to_mm.round, y: camera.eye.z.to_mm.round, z: camera.eye.x.to_mm.round }.to_json)

    puts "Target"
    puts({ x: camera.target.y.to_mm.round, y: camera.target.z.to_mm.round, z: camera.target.x.to_mm.round }.to_json)

    return nil
  end; 1
end
