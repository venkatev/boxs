# == Schema Information
#
# Table name: design_views
#
#  id             :integer          not null, primary key
#  design_id      :integer
#  camera_view_id :integer
#  deferred       :boolean          default("true")
#  pixel_map      :json             default("[]")
#  image          :string
#  created_at     :datetime
#  updated_at     :datetime
#

class DesignView < ApplicationRecord
  belongs_to :design
  belongs_to :camera_view

  validates :design,
            :image,
            :presence => true,
            unless: :deferred?

  mount_base64_uploader :image, RenderImageUploader
  delegate :primary?, :to => :camera_view

  before_validation :set_deferred, on: :create
  scope :primary, -> { joins(:camera_view).where(camera_views: { primary: true }) }
  scope :deferred, -> { where(deferred: true) }
  scope :rendered, -> { where(deferred: false) }

  # TODO Rename pixel_map attribute to bounds_map.
  def bounds_map
    self.pixel_map
  end

  def copy
    _render = self.dup
    _render.image = self.image
    return _render
  end

  def rendered?
    !self.deferred
  end

  def render!(force: false)
    self.design.render_view!(self, force)
  end

  private

  def set_deferred
    self.deferred = true
  end
end
