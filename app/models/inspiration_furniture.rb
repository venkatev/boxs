class InspirationFurniture < ActiveRecord::Base
  belongs_to :inspiration
  belongs_to :furniture

  validates :inspiration, :furniture, presence: true
  validates :inspiration, uniqueness: { scope: :furniture }
  validate :check_furniture_is_published

  private

  def check_furniture_is_published
    errors.add(:furniture, "must be published") unless self.furniture.published?
  end
end
