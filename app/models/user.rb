# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  mobile     :string           not null
#  otp_secret :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ActiveRecord::Base
  OTP_VALIDITY = 60

  has_many :rooms, dependent: :destroy
  before_create :generate_otp_secret

  def self.lookup(mobile)
    self.find_by_mobile(mobile)
  end

  def get_otp
    totp = ROTP::TOTP.new(self.otp_secret, interval: OTP_VALIDITY)
    totp.now
  end

  def verify_otp(otp)
    totp = ROTP::TOTP.new(self.otp_secret, interval: OTP_VALIDITY)
    totp.verify(otp)
  end

  def save_design!(design)
    _design = design.is_private? ? design : design.copy
    _design.user = self
    _design.save!

    return _design
  end

  private

  def generate_otp_secret
    self.otp_secret = ROTP::Base32.random
  end
end
