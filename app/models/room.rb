# == Schema Information
#
# Table name: rooms
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  room_template_id :integer
#  room_category    :string
#  created_at       :datetime
#  updated_at       :datetime
#

class Room < ApplicationRecord
  belongs_to :user
  belongs_to :room_template, required: true

  has_many :designs, dependent: :destroy

  delegate :length, :breadth, :height, :room_category, :camera_views, :furniture_layouts, to: :room_template

  # Generates the next random design.
  # If previous_design and item_identifier are given, randomizes only that furniture within the design.
  def generate_next_design(previous_design = nil, item_identifier = nil)
    all_furnitures = Furniture.all_furnitures.shuffle
    design = nil
    visited_layouts = []

    # If a design is given, use that layout, else explore all layouts.
    pending_layouts = previous_design ? [previous_design.furniture_layout] : self.furniture_layouts.to_a
    is_item_missing = false

    while !design && visited_layouts.size != pending_layouts.size
      all_furnitures = all_furnitures.shuffle
      layout = (pending_layouts - visited_layouts).sample
      item_mapping = previous_design ? previous_design.item_mapping.dup : {}
      items = item_identifier ? layout.items.select {|item| item.id == item_identifier } : layout.items
      prev_furniture_id = previous_design.item_mapping[item_identifier]['id'] if previous_design

      items.each do |item|
        furniture = all_furnitures.find {|f|
          f.variant_id == item.variant_id && (previous_design ? f.id != prev_furniture_id : true)
        }

        if furniture
          item_mapping[item.id] = furniture.attributes.slice("id", "dimensions", 'variant_id', 'price').merge(orientation: item.orientation, category: furniture.category)
        else
          is_item_missing = true
        end
      end

      if !is_item_missing
        # puts item_mapping
        # design = item_mapping
        # Reload is required to convert the attributes to JSON format.
        # pp item_mapping
        # return
        design = self.designs.create!(furniture_layout: layout, item_mapping: item_mapping).reload
      end

      visited_layouts << layout
    end

    return design
  end

  def create_design_from_inspiration(previous_design, inspiration, category = nil)
    pending_layouts = category ? [previous_design.furniture_layout] : self.furniture_layouts.to_a
    design = nil
    visited_layouts = []
    is_item_missing = false
    is_full_furnish = !category

    while !design && visited_layouts.size != pending_layouts.size
      layout = (pending_layouts - visited_layouts).sample

      if is_full_furnish
        # Changing the whole design. Furnish all items afresh.
        items = layout.items
        item_mapping = {}
      else
        prev_furniture = previous_design.furnitures.find {|f| f.category == category }
        items = layout.items.select {|item| item.category == category }
        item_mapping = previous_design.item_mapping.dup
      end

      items.each do |item|
        # Get all the furnitures from the inspiration matching the item category.
        furnitures_from_inspiration = inspiration.get_furnitures(item.category)

        if furnitures_from_inspiration.any?
          # And, among those, get those matching the variant.
          furnitures = furnitures_from_inspiration.select {|f| f.variant_id == item.variant_id }
          furnitures = furnitures - [prev_furniture] unless is_full_furnish
          furniture = furnitures[0] if furnitures.any?

          # Inspiration furniture cannot be used. Find one matching the color from the full catalog.
          if !furniture
            color_ids = furnitures_from_inspiration.map(&:color_id).reject(&:blank?)
            furnitures_matching_color = Furniture.published.where(variant_id: item.variant_id, color_id: color_ids)
            furniture = furnitures_matching_color[0] if furnitures_matching_color.any?
          end
        else
          # No furniture in the inspiration for this category. Get one at random.
          furniture = Furniture.published.where(variant_id: item.variant_id).sample
        end

        if furniture
          item_mapping[item.id] = furniture.attributes.slice("id", "dimensions", 'variant_id', 'price').merge(orientation: item.orientation, category: furniture.category)
        else
          is_item_missing = true
        end
      end

      if !is_item_missing
        design = self.designs.create!(furniture_layout: layout, item_mapping: item_mapping).reload
      end

      visited_layouts << layout
    end

    return design
  end
end
