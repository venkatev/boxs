# == Schema Information
#
# Table name: moodboard_category_collections
#
#  id                     :integer          not null, primary key
#  moodboard_id           :integer
#  category_collection_id :integer
#  created_at             :datetime
#  updated_at             :datetime
#

class MoodboardCategoryCollection < ApplicationRecord
  belongs_to :moodboard
  belongs_to :category_collection
end
