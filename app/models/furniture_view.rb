# == Schema Information
#
# Table name: furniture_views
#
#  id           :integer          not null, primary key
#  furniture_id :integer
#  theta        :float
#  phi          :float
#  r            :float
#  created_at   :datetime
#  updated_at   :datetime
#  image        :string
#

class FurnitureView < ApplicationRecord
  belongs_to :furniture
  mount_base64_uploader :image, FurnitureViewImageUploader
  scope :of_r,           -> (r)       {where(r: r)}
  scope :of_theta,       -> (theta)   {where("ROUND(theta) = ?", theta.round)}
  scope :primary,        ->           {where(r: 3000, theta: 30)}
end
