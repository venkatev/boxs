# == Schema Information
#
# Table name: category_collection_furnitures
#
#  id                     :integer          not null, primary key
#  category_collection_id :integer
#  furniture_id           :integer
#  created_at             :datetime
#  updated_at             :datetime
#

class CategoryCollectionFurniture < ApplicationRecord
  belongs_to :category_collection
  belongs_to :furniture

  validate :category_integrity

  private

  def category_integrity
    if self.furniture.category != self.category_collection.category
      errors.add(:furniture, "category #{self.furniture.category} is not the same as the collection category #{self.category_collection.category}")
    end
  end
end
