class RenderImageUploader < ImageUploaderBase
  process :trim
  # process :store_dimensions

  version :small do
    process :resize_to_fit => [800, 800]
  end

  private

  def trim
    manipulate! do |img|
      img.trim
    end
  end

  def store_dimensions
    if file && model
      model.width, model.height = ::MiniMagick::Image.open(file.file)[:dimensions]
      # model.width, model.height = 1600, 1600
    end
  end
end
