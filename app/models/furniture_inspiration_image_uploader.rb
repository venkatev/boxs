class FurnitureInspirationImageUploader < ImageUploaderBase
  version :small do
    process :resize_to_fit => [250, 250]
  end
end

