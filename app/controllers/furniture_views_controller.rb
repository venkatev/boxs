class FurnitureViewsController < ApplicationController
  def create
    @furniture = Furniture.find(params[:furniture_id])
    @furniture_view = @furniture.furniture_views.where(theta: params[:theta], r: params[:r]).first_or_initialize
    @furniture_view.image = params[:image]
    @furniture_view.save!

    render :nothing => true
  end
end
