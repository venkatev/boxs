require 'layout_engine_manager'

class MoodboardsController < ApplicationController
  def index
    @is_draft = params[:published] == 'false'
    @moodboards = @is_draft ? Moodboard.draft : Moodboard.published
  end

  def show
    respond_to do |format|
      format.json do
        @moodboard = Moodboard.find(params[:id])

        render json: MoodboardSerializer.new(@moodboard).attributes
      end

      format.html do
        @moodboard = Moodboard.find(params[:id])
        super_categories = @moodboard.required_super_categories_map.map {|e| e[0] }

        # So, get the category list from the super category list which Grobr understands.
        categories = super_categories.map {|super_category|
          LEM::FURNISH_CONFIG[@moodboard.room_type][:categories].select {|category, details| details[:status] != 'new' && details[:super_category] == super_category }.keys
        }.flatten
        variant_ids = categories.map {|category|
          LEM::FURNISH_CONFIG[@moodboard.room_type][:categories][category][:variants]
        }.flatten.map {|v| v[:id] }

        furnitures_url = Services::GROBR + "/furnitures.json?boxs_variant_ids=#{variant_ids.join(',')}"
        response = Faraday.get(furnitures_url)

        # Convert them back to super category so that the moodboard can understand it.
        super_category_furnitures = JSON.parse(response.body).group_by {|f|
          category = LEM.get_category_of(@moodboard.room_type, f['boxs_variant_id'])
          LEM.get_super_category_of(@moodboard.room_type, category)[:name]
        }

        furnitures_data = super_category_furnitures.map {|super_category, furnitures|
          [
            super_category,
            furnitures.group_by {|furniture| furniture['id'] }.map {|id, entries| [ id, entries[0]] }.to_h
          ]
        }.to_h

        @furnitures_data = furnitures_data
      end
    end
  end

  def update
    @moodboard = Moodboard.find(params[:id])

    furniture_map = []

    params[:furniture_ids_by_super_category].each do |super_category, furniture_ids|
      furniture_map << { super_category: super_category, furniture_ids: furniture_ids || [] }
    end

    @moodboard.update furniture_map: furniture_map

    if @moodboard.valid?
      render nothing: true
    else
      render status: 400, text: @moodboard.errors.full_messages
    end
  end

  def publish
    @moodboard = Moodboard.find(params[:id])
    @moodboard.publish!

    if @moodboard.valid?
      flash[:success] = "Moodboard published successfully."
      redirect_to moodboards_path(published: true)
    else
      flash[:alert] = @moodboard.errors.full_messages.join(". ")
      redirect_to moodboards_path
    end
  end

  def unpublish
    @moodboard = Moodboard.find(params[:id])
    @moodboard.unpublish!
    redirect_to moodboards_path(published: false)
  end

  def archive
    @moodboard = Moodboard.find(params[:id])
    @furniture.archive!
    redirect_to moodboards_path(published: false)
  end
end
