class SessionsController < ApplicationController
  def user
    @user = current_user
    render json: @user
  end

  def request_otp
    @user = User.lookup(params[:mobile])

    if @user.nil?
      @user = User.create!(mobile: params[:mobile])
    end

    # otp = @user.get_otp

    # Sms.new(
    #   :to => params[:mobile],
    #   :message => "Your OTP for logging into Boxs is #{otp}."
    # ).send

    login_user(@user)
    # render json: @user

    render json: UserSerializer.new(@user).attributes
  end

  def login
    @user = User.lookup(params[:mobile]) || User.create!(mobile: params[:mobile], name: 'Guest')

    login_user(@user)
    render json: @user
  end

  def logout
    logout_user
    redirect_to root_path
  end
end
