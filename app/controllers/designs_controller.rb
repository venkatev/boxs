class DesignsController < ApplicationController
  # before_action :set_design, only: [:show, :load, :similar, :replace_furniture, :save_to_my_designs, :generate_renders, :generate_render]

  def index
    respond_to do |format|
      format.json do
        @designs = Design.for_layout(params[:layout_id])
        render json: @designs.map {|d| DesignSerializer.new(d).attributes }
      end
    end
  end

  def show
    respond_to do |format|
      format.html do
        @data = {
          room_id: params[:room_id],
          furniture_layout_id: params[:furniture_layout_id],
          view_index: params[:view_index],
          design_index: params[:design_index]
        }
      end

      format.json do
        @design = Design.find(params[:id])
        render json: DesignSerializer.new(@design).attributes
      end
    end
  end

  def save_to_my_designs
    @my_design = current_user.save_design!(@design)

    render json: { design_id: @my_design.id }
  end

  def design_renderer
    renderer
  end

  def renderer
    @design = Design.find(params[:id])
    @camera_view = @design.layout.room_template.camera_views.find(params[:camera_view_id])
    @viewPort = { width: 1000, height: 675 }
    filtered_mappings = @design.furniture_mapping.reject{|a,b| %w(floor_surface).include?(b['category'])}
    # filtered_mappings = @design.furniture_mapping

    @furniture_items = filtered_mappings.map {|furniture_item_id, furniture_details|
      furniture = furniture_details.slice("id", "position", "dimensions", "orientation", "surface_dimensions", 'category', 'boxs_variant_id')
      o = furniture['orientation']
      dimensions = furniture['dimensions']
      h = dimensions['height']
      w = dimensions['width']
      d = dimensions['depth']

      pos_x = furniture['position'][0]
      pos_y = furniture['position'][1]
      pos_z = furniture['position'][2]

      min_x = max_x = min_z = max_z = 0
      min_y = pos_y
      max_y = pos_y + h

      case o
      when 1
        # No changes in position
        min_x = pos_x
        max_x = pos_x + w
        min_z = pos_z
        max_z = pos_z + d

      when 2
        min_x = pos_x - d
        max_x = pos_x
        min_z = pos_z
        max_z = pos_z + w

      when 3
        min_x = pos_x - w
        max_x = pos_x
        min_z = pos_z - d
        max_z = pos_z

      when 4
        min_x = pos_x
        max_x = pos_x + d
        min_z = pos_z - w
        max_z = pos_z
      end

      if furniture['id'].to_i == 955
        # FIXME
        position_bounds = {
          min_x: 0, max_x: 4270, min_y: 0, max_y: 2898, min_z: 0, max_z: 2
        }
      else
        position_bounds = {
          min_x: min_x, max_x: max_x, min_y: min_y, max_y: max_y, min_z: min_z, max_z: max_z
        }
      end

      furniture.merge!(position_bounds: position_bounds)

      [
        furniture_item_id,
        furniture
      ]
    }.to_h

    render layout: false
  end

  def get_random_color
    letters = '79BDF'
    color = '#'

    6.times do
      color += letters[(rand * 5).floor]
    end

    return color;
  end

  def similar
    @layout = @design.layout
    @furniture_item = @layout.get_furniture_item(params[:furniture_item_id])
    @recommendations = Furniture.recommendations(params[:furniture_id], @furniture_item.variant_id)

    render json: @recommendations.map(&:attributes)
  end

  def furniture_recommendations
    @design = Design.find params[:id]
    @furniture = Furniture.find(params[:furniture_id])

    @other_options = @design.other_options(@furniture)
    @inspirations = Inspiration.all

    render :json => @other_options.map {|f|
      inspiration = f.inspirations.first
      sample_url = inspiration ? inspiration.image.url : f.image_url
      { furniture_id: f.id, image_url: sample_url }
    }
  end

  def replace_furniture
    @design = Design.find params[:id]
    @room = @design.room
    @inspiration = Inspiration.find(params[:inspiration_id])
    @new_design = @room.create_design_from_inspiration(@design, @inspiration, params[:category])

    if @new_design
      render json: DesignSerializer.new(@new_design).attributes
    else
      render status: 422, nothing: true
    end
  end

  def generate_renders
    @design.generate_renders!
    render json: DesignSummarySerializer.new(@design).attributes
  end

  # TODO This doesn't belong here. Move to a separate controller.
  def furniture
    Catalog.load
    furniture_id = params[:id]
    furniture_category = params[:category]
    theta = params[:t].to_i
    distance = params[:r].to_i
    tmp_image = "#{SecureRandom.hex}.png"

    if furniture_category == 'wall_surface'
      theta = 0
    end

    @catalog_entry = Catalog.lookup(furniture_id, theta, distance)
    # image_url = "https://zenterior-uploads.s3.amazonaws.com/uploads/catalog_image/image/#{@catalog_entry['u']}/file.png"
    # puts image_url

    open(tmp_image, 'wb') do |file|
      file << File.read(Rails.root + "data/catalog/#{@catalog_entry['u']}/file.png")
    end

    if furniture_category == 'wall_surface'
      d = MiniMagick::Image.open(tmp_image).dimensions
      params[:cp] = "0_#{d[1]}|0_#{d[1]}|0_0|0_0|456_#{d[1]}|#{d[0]}_#{d[1]}|#{d[0]}_0|#{d[0]}_0"
    end

    centroid_projections = params[:cp].split('|').map {|p| p.split('_').map(&:to_i)}
    target_projections = params[:tp].split('|').map {|p| p.split('_').map(&:to_i)}

    cp_min_x = centroid_projections.map {|p| p[0] }.min
    cp_min_y = centroid_projections.map {|p| p[1] }.min
    cp_max_x = centroid_projections.map {|p| p[0] }.max
    cp_max_y = centroid_projections.map {|p| p[1] }.max

    tp_min_x = target_projections.map {|p| p[0] }.min
    tp_min_y = target_projections.map {|p| p[1] }.min
    tp_max_x = target_projections.map {|p| p[0] }.max
    tp_max_y = target_projections.map {|p| p[1] }.max

    cp_width = cp_max_x - cp_min_x
    cp_height = cp_max_y - cp_min_y
    tp_width = tp_max_x - tp_min_x
    tp_height = tp_max_y - tp_min_y

    width_ratio = tp_width / cp_width.to_f
    height_ratio = tp_height / cp_height.to_f

    centroid_projections = centroid_projections.map {|p| [p[0] - cp_min_x, p[1] - cp_min_y] }
    target_projections = target_projections.map {|p| [p[0] - tp_min_x, p[1] - tp_min_y] }

    puts "***********"
    puts centroid_projections.inspect
    puts target_projections.inspect

    # Resize the centroid projections to the target projections to avoid clipping.
    centroid_projections = centroid_projections.map {|p| [(p[0] * width_ratio).round, (p[1] * height_ratio).round] }

    # Pair each of the corresponding points in the projections.
    projection_map = centroid_projections.zip(target_projections)

    # image_url = self.benchmark("Get image URL") do
    #   Faraday.get(info_url).body
    # end

    # image = nil

    # self.benchmark("Load image") do
    #   puts image_url
    #   image = MiniMagick::Image.open(image_url)
    # end

    # puts image.dimensions

    # self.benchmark("Resize") do
    #   # Resize to target size.
    #   image.resize "#{tp_width}x#{tp_height}"
    # end

    # Construct a perspective map of the following format where cxi is the centroid x 1st point and tyi is the corresponding target point.
    # cx1,cy1 tx1,ty1 cx2,cy2 tx2,ty2, ....
    perspective_args = projection_map.map {|tuple| tuple[0].join(',') + ',' + tuple[1].join(',')}.join('  ')
    # perspective_args += ' --virtual-pixel transparent'

    puts perspective_args.inspect

    image = MiniMagick::Tool::Convert.new
    image << tmp_image
    image.resize("#{tp_width}x#{tp_height}!")
    # image << tmp_image
    # image.call

    # puts "Resize"
    # puts "#{tp_width}x#{tp_height}!"
    # puts tmp_image
    # image = MiniMagick::Tool::Convert.new
    # image << tmp_image
    image << "-matte"
    image << "-virtual-pixel"
    image << "transparent"
    image << "-distort"
    image << "Perspective"
    image << perspective_args
    image << tmp_image
    image.call

    # image.trim

    # self.benchmark("Distort") do
    #   image.matte
    #   image.virtual_pixel("transparent")

    #   # Do a perspective distortion to change to the target's shape.
    #   image.distort("Perspective", perspective_args)
    # end

    # self.benchmark("Trim") do
    #   image.trim
    # end

    @data = File.read(tmp_image)
    File.delete(tmp_image)
    send_data @data, type: 'image/png', disposition: 'inline'
  end

  # Returns or generates if required, the given camera view within the design.
  def view
    @design = Design.includes(:views).find(params[:id])

    @design.views.each do |view|
      @design.render_view!(view)
    end

    render json: DesignViewSerializer.new(@design_view).attributes
  end

  private

  def set_design
    @design = Design.find(params[:id])
  end
end
