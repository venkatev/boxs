class FurnituresController < ApplicationController
  def all
    if params[:token] != '1b0bcf3e70fbc94d921e938bd72b7ae2'
      render status: 403, body: "Unauthorized"
      return
    end

    render json: Furniture.all_data
  end

  def index
  end

  def show
    @furniture = Furniture.find(params[:id])

    render json: {
      lights: @furniture.lights
    }
  end

  def update_dimensions
    dimension_params = params.require(:furniture).permit(dimensions: [:height, :width, :depth])
    @furniture = Furniture.find(params[:id])
    if @furniture.update(dimension_params)
      @furniture.update_data_errors(nil)
      render json: @furniture
    else
      data_errors = @furniture.errors.full_messages.join(", ")
      @furniture.update_data_errors(data_errors)
      render json: {error_message: data_errors}, status: :unprocessable_entity
    end
  end

  def loader
    @furniture = Furniture.find(params[:id])
    render :json => @furniture.model
  end

  def perspective
    @furniture = Furniture.find(params[:id])
    furniture_category = params[:category]
    theta = params[:t].to_i
    distance = params[:r].to_i
    tmp_image = "tmp/#{SecureRandom.hex}.png"

    # Find the catalog image where the sum of (differences of the theta and distance) is the minimum
    # @catalog_image = CatalogImage.where("furniture_id = ?", furniture_id)
    #                               .order("abs((theta - #{theta}) / #{theta}::NUMERIC) + abs((r - #{r}) / #{r}::NUMERIC)")
    #                               .first

    @furniture_view = @furniture.furniture_views.where(theta: theta, r: distance)[0]

    if @furniture_view.nil?
        raise "No view found for Furniture ##{@furniture.id} (theta: #{theta}, r: #{distance})"
    end

    open(tmp_image, 'wb') do |file|
      image_url = Rails.env.development? ?
        "http://localhost:3001#{@furniture_view.image.url}" :
        @furniture_view.image.url

      file << Faraday.get(image_url).body
      # file << File.read(Rails.root + "data/catalog/#{@catalog_entry['u']}/file.png")
      # file << File.read(Rails.root + "public/catalog/2.png")
    end

    centroid_projections = params[:cp].split('|').map {|p| p.split('_').map(&:to_i)}
    target_projections = params[:tp].split('|').map {|p| p.split('_').map(&:to_i)}

    cp_min_x = centroid_projections.map {|p| p[0] }.min
    cp_min_y = centroid_projections.map {|p| p[1] }.min
    cp_max_x = centroid_projections.map {|p| p[0] }.max
    cp_max_y = centroid_projections.map {|p| p[1] }.max

    tp_min_x = target_projections.map {|p| p[0] }.min
    tp_min_y = target_projections.map {|p| p[1] }.min
    tp_max_x = target_projections.map {|p| p[0] }.max
    tp_max_y = target_projections.map {|p| p[1] }.max

    cp_width = cp_max_x - cp_min_x
    cp_height = cp_max_y - cp_min_y
    tp_width = tp_max_x - tp_min_x
    tp_height = tp_max_y - tp_min_y

    width_ratio = tp_width / cp_width.to_f
    height_ratio = tp_height / cp_height.to_f

    centroid_projections = centroid_projections.map {|p| [p[0] - cp_min_x, p[1] - cp_min_y] }
    target_projections = target_projections.map {|p| [p[0] - tp_min_x, p[1] - tp_min_y] }

    puts "***********"
    puts centroid_projections.inspect
    puts target_projections.inspect

    # Resize the centroid projections to the target projections to avoid clipping.
    centroid_projections = centroid_projections.map {|p| [(p[0] * width_ratio).round, (p[1] * height_ratio).round] }

    # Pair each of the corresponding points in the projections.
    projection_map = centroid_projections.zip(target_projections)

    # image_url = self.benchmark("Get image URL") do
    #   Faraday.get(info_url).body
    # end

    # image = nil

    # self.benchmark("Load image") do
    #   puts image_url
    #   image = MiniMagick::Image.open(image_url)
    # end

    # puts image.dimensions

    # self.benchmark("Resize") do
    #   # Resize to target size.
    #   image.resize "#{tp_width}x#{tp_height}"
    # end

    # Construct a perspective map of the following format where cxi is the centroid x 1st point and tyi is the corresponding target point.
    # cx1,cy1 tx1,ty1 cx2,cy2 tx2,ty2, ....
    perspective_args = projection_map.map {|tuple| tuple[0].join(',') + ',' + tuple[1].join(',')}.join('  ')
    # perspective_args += ' --virtual-pixel transparent'

    puts perspective_args.inspect

    image = MiniMagick::Tool::Convert.new
    image << tmp_image
    image.resize("#{tp_width}x#{tp_height}!")
    # image << tmp_image
    # image.call

    # puts "Resize"
    # puts "#{tp_width}x#{tp_height}!"
    # puts tmp_image
    # image = MiniMagick::Tool::Convert.new
    # image << tmp_image
    image << "-matte"
    image << "-virtual-pixel"
    image << "transparent"
    image << "-distort"
    image << "Perspective"
    image << perspective_args
    image << tmp_image
    image.call

    # image.trim

    # self.benchmark("Distort") do
    #   image.matte
    #   image.virtual_pixel("transparent")

    #   # Do a perspective distortion to change to the target's shape.
    #   image.distort("Perspective", perspective_args)
    # end

    # self.benchmark("Trim") do
    #   image.trim
    # end

    @data = File.read(tmp_image)
    File.delete(tmp_image)
    send_data @data, type: 'image/png', disposition: 'inline'
  end

  def renderer
    @furniture = Furniture.find(params[:id])

    if params[:c].present?
      theta, r = *(params[:c].split(',').map(&:to_i))
      @camera_positions = [{ theta: theta, r: r }]

      if @furniture.furniture_views.where(theta: theta, r: r).present? && params[:dont_force]
        # If view is already present and dont_force is set, just render an empty response.
        @auto_close = true
      end
    else
      @camera_positions = @furniture.pending_capture_angles
    end

    @y_position = @furniture.lem_y_position
  end

  def chrode_renderer
    remote_response = Faraday.get(Services::CHRODE + "/furnitures/#{params[:id]}/renderer")
    render json: remote_response.body
  end

  def chrode_status
    remote_response = Faraday.get(Services::CHRODE + "/furnitures/status")
    render json: remote_response.body
  end

  def update_lights
    @furniture = Furniture.find(params[:id])
    @furniture.update lights: params[:lights]

    render nothing: true
  end

  def remove_views
    @furniture = Furniture.find(params[:id])
    @furniture.furniture_views.destroy_all
    @camera_positions = @furniture.reload.pending_capture_angles

    render json: @camera_positions
  end
end
