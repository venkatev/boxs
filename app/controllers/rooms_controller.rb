require 'layout_engine_manager'

class RoomsController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:create]

  def new
    respond_to do |format|
      format.html do
        @is_app = true
        render :new_auto_layout
      end

      format.json do
        @room_type = params[:room_type]
        @room_sizes = [
          { length1: 11, length2: 21, length1_range: "11-12", length2_range: "21-23", active: true },
          { length1: 11, length2: 22, length1_range: "11-12", length2_range: "21-23", active: true },
          { length1: 11, length2: 23, length1_range: "11-12", length2_range: "21-23", active: true },
          { length1: 12, length2: 21, length1_range: "11-12", length2_range: "21-23", active: true },
          { length1: 12, length2: 22, length1_range: "11-12", length2_range: "21-23", active: true },
          { length1: 12, length2: 23, length1_range: "11-12", length2_range: "21-23", active: true },
          { length1: 13, length2: 24, length1_range: "13-14", length2_range: "24-26", active: true },
          { length1: 13, length2: 25, length1_range: "13-14", length2_range: "24-26", active: true },
          { length1: 13, length2: 26, length1_range: "13-14", length2_range: "24-26", active: true },
          { length1: 14, length2: 24, length1_range: "13-14", length2_range: "24-26", active: true },
          { length1: 14, length2: 25, length1_range: "13-14", length2_range: "24-26", active: true },
          { length1: 14, length2: 26, length1_range: "13-14", length2_range: "24-26", active: true },
          { length1: 15, length2: 26, length1_range: "15-17", length2_range: "24-26", active: false },
          { length1: 15, length2: 28, length1_range: "15-17", length2_range: "27-30", active: false },
          { length1: 15, length2: 30, length1_range: "15-17", length2_range: "27-30", active: false }
        ]

        shell_categories = LEM.get_shell_categories(@room_type)
        @shell_objects_variants = shell_categories.to_a.map {|category, info|
          info[:variants].map {|variant|
            variant.slice(:id, :name, :size, :y_position).merge(category: category)
          }
        }.flatten

        render json: {
          room_sizes: @room_sizes,
          shell_objects_info: @shell_objects_variants
        }
      end
    end
  end

  def create
    if params[:inspiration_id]
      @inspiration = Inspiration.includes(:furnitures).find(params[:inspiration_id])

      RoomTemplate.published.shuffle.each do |room_template|
        if @room
          @room.update room_template: room_template
        else
          @room = Room.create!(room_template: room_template)
        end

        @design = @room.create_design_from_inspiration(nil, @inspiration)
        break if @design
      end
    else
      @room = Room.create!(room_template_id: params[:room_template_id])
      @inspirations = Inspiration.published.includes(:furnitures)

      @inspirations.sort_by {|i| i.furnitures.size }.reverse.each do |inspiration|
        @design = @room.create_design_from_inspiration(nil, inspiration)
        break if @design
      end
    end

    if @design.nil?
      render status: 422, nothing: true
    else
      render json: @room.id
    end
  end

  def index
    respond_to do |format|
      format.html do
      end

      format.json do
        @rooms = current_user.rooms
        render json: @rooms.map { |room| RoomListingSerializer.new(room).attributes }
      end
    end
  end

  def show
    respond_to do |format|
      format.html do
        @room = Room.find(params[:id])
      end

      format.json do
        @room = Room.includes(:designs => :views, :room_template => :camera_views).find(params[:id])
        render json: RoomShowSerializer.new(@room).attributes
      end
    end
  end

  def update
    @room = Room.find(params[:id])
    @room.update user_id: params[:user_id]
    render nothing: true
  end

  def layouts
    respond_to do |format|
      format.html do
        @room_id = params[:id]
      end

      format.json do
        room = Room.includes(:room_template => :furniture_layouts).find(params[:id])
        render json: RoomLayoutsSerializer.new(room).attributes
      end
    end
  end

  def save_design
    @room = Room.find(params[:id])
    @moodboard = Moodboard.find(params[:moodboard_id])

    render json: RoomShowSerializer.new(@room).attributes
  end

  def templates
    respond_to do |format|
      format.html do
      end

      format.json do
        @room_templates = RoomTemplate.where(room_category: params[:room_type])
        @room_templates = @room_templates.published unless admin?
        render json: @room_templates.map {|rt| RoomTemplatesSerializer.new(rt).attributes }
      end
    end
  end
end
