require 'layout_engine_manager'

class LayoutEngineController < ApplicationController
  def catalog_configs
    render json: LEM.get_config_for_catalog
  end

  def manage
    if params[:token] != '506b139a971c681f'
      render file: "#{Rails.root}/public/404.html" , layout: false
    end

    @data = LEM.get_rules(true)
    @cell_size = LEM::CELL_SIZE

    # reset_data!
    furnish_config = LEM::FURNISH_CONFIG
    @folders = [{ :key => 'room_type', items: furnish_config.keys, selected: nil }]

    # Room Type
    if params[:room_type]
      @room_type = params[:room_type]
      furnish_config = furnish_config[@room_type]
      wall_sizes = furnish_config[:wall_sizes]
      @folders[-1][:selected] = @room_type

      furnishable_super_categories = LEM::FURNISH_CONFIG[@room_type][:super_categories].select {|mc| mc[:reference] }
      categories = furnishable_super_categories.map {|super_category|
        LEM::FURNISH_CONFIG[@room_type][:categories].select {|category, details|
          details[:status] != 'new' && details[:super_category] == super_category[:name]
        }.keys
      }.flatten.compact

      @folders << { :key => 'object_category', items: categories, selected: nil }

      # Object Type
      if params[:object_category]
        @object_category = params[:object_category]
        furnish_config = furnish_config[:categories][@object_category]
        @folders[-1][:selected] = @object_category
        object_variants = furnish_config[:variants].select {|variant| variant[:status] != 'new' }
        @folders << { :key => 'object_variant', items: object_variants, selected: nil }

        # Object Size
        if params[:object_variant]
          @object_variant_id = params[:object_variant]

          @folders[-1][:selected] = @object_variant_id
          reference_super_category = LEM.get_super_category_of(@room_type, @object_category)[:reference]
          reference_categories = LEM::FURNISH_CONFIG[@room_type][:categories].select {|category, details| details[:status] != 'new' && details[:super_category] == reference_super_category }.keys
          @folders << { :key => 'ref_category', items: reference_categories, selected: nil }

          # Reference object category
          if params[:ref_category]
            @ref_category = params[:ref_category]
            @folders[-1][:selected] = @ref_category
            ref_variants = LEM::FURNISH_CONFIG[@room_type][:categories][@ref_category][:variants].select {|v| v[:status] != 'new' }
            @folders << { :key => 'ref_variant', items: ref_variants, selected: nil }

            if params[:ref_variant]
              @ref_variant_id = params[:ref_variant]
              @folders[-1][:selected] = @ref_variant_id

              room_sizes = wall_sizes.product(wall_sizes).map {|e| { 'room_length' => e[0], 'room_breadth' => e[1] } }
              @folders << { :key => 'room_size', items: room_sizes, selected: nil }

              if params[:room_size]
                @room_size = params[:room_size]
                @flip_room_size = { room_breadth: @room_size[:room_length], room_length: @room_size[:room_breadth] }
                @folders[-1][:selected] = @room_size

                if LEM.only_direct_placement?(@room_type, @ref_category)
                  # The reference object doesn't have any further references. Get the list of direct positions.
                  placements_1 = @data['direct'][@ref_category][@ref_variant_id][{ 'room_length' => @room_size['room_length'] }.to_json] || [] rescue nil
                  placements_2 = []
                else
                  # Get only the room sizes and the placements of this object, skipping the intermediary reference for which they are defined.
                  # PLEASE DON'T ASK ME WHAT THE BELOW LOGIC DOES :-)
                  ref_placements = @data['relative'][@ref_category][@ref_variant_id]
                                    .values # Skip all the parent reference categories of the reference. We don't need relative to what it was defined.
                                    .map(&:values) # And, the parent reference sizes as well.
                                    .flatten
                                    .map{|e|
                                      e.map{|a,b|
                                        [a, b.values.flatten]
                                      }.to_h
                                    }.flatten rescue []

                  # Merge all values to create a single.
                  ref_placements = ref_placements.map(&:to_a)
                                    .flatten(1)
                                    .group_by {|e| e[0]}
                                    .map{|a,b|
                                      [a, b.map {|x| x[1] }.flatten.uniq ]
                                    }.to_h

                  placements_1 = ref_placements[@room_size.to_json] || []
                  placements_2 = ref_placements[@flip_room_size.to_json] || []

                  placements_1 = placements_1.select {|placement| [1, 3].include?(LEM.get_orientation(placement)) }
                  placements_2 = placements_2.select {|placement| [2, 4].include?(LEM.get_orientation(placement)) }
                end

                avg_room_length = (@room_size[:room_length].split('-').map(&:to_f).sum / 2).round
                avg_room_breadth = (@room_size[:room_breadth].split('-').map(&:to_f).sum / 2).round

                abs_room_size = { room_length: avg_room_length, room_breadth: avg_room_breadth }
                LEM::TH.init(@room_type, abs_room_size)

                placements_1 = placements_1.map {|placement|
                  cell_range = LEM.get_cell_range(placement)
                  LEM::TH.normalize(@room_type, @room_size.symbolize_keys, cell_range)
                }

                placements_2 = placements_2.map {|placement|
                  cell_range = LEM.get_cell_range(placement)
                  LEM::TH.normalize(@room_type, @room_size.symbolize_keys, cell_range, flipped_bounds: true)
                }

                placements = (placements_1 + placements_2).uniq.sort
                @folders << { :key => 'ref_placement', items: placements, selected: nil }

                if params[:ref_placement]
                  @ref_placement = params[:ref_placement]
                  @folders[-1][:selected] = @ref_placement
                end
              end
            end
          end
        end
      end
    end

    if @ref_placement
      object_variant = LEM::FURNISH_CONFIG[@room_type][:categories][@object_category][:variants].find {|v| v[:id] == @object_variant_id }
      ref_variant = LEM::FURNISH_CONFIG[@room_type][:categories][@ref_category][:variants].find {|v| v[:id] == @ref_variant_id }

      @object_width = (object_variant[:size][:width].split('-').map(&:to_f).sum / 2.0).round
      @object_depth = (object_variant[:size][:depth].split('-').map(&:to_f).sum / 2.0).round

      @ref_width      = (ref_variant[:size][:width].split('-').map(&:to_f).sum / 2.0).round
      @ref_depth      = (ref_variant[:size][:depth].split('-').map(&:to_f).sum / 2.0).round

      @room_length    = (@room_size['room_length'].split('-').map(&:to_f).sum / 2.0).round
      @room_breadth   = (@room_size['room_breadth'].split('-').map(&:to_f).sum / 2.0).round

      @placements = @data['relative'][@object_category][@object_variant_id][@ref_category][@ref_variant_id][@room_size.to_json][@ref_placement] || [] rescue []
    end

    @all_params = params
  end

  def get_placements
    @data = LEM.get_rules(true)
    pp = params
    placements = @data['relative'][pp[:object_category]][pp[:object_variant]][pp[:ref_category]][pp[:ref_variant]][pp[:room_size].to_json][pp[:ref_placement]] || [] rescue []

    render json: placements
  end

  def save
    @data = LEM.get_rules(true)
    pp = params

    data = @data['relative']

    [:object_category, :object_variant, :ref_category, :ref_variant, :room_size].each do |key|
      value = pp[key]
      value = value.to_json if value.is_a?(Hash)
      data[value] ||= {}
      data = data[value]
    end

    data[pp[:ref_placement]] = pp[:placements]

    LEM::DataStore.write!(@data)
    render :json => { url:  manage_layout_engine_path(params) }
  end
end
