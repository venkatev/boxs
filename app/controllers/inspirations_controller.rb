class InspirationsController < ApplicationController
  def index
    @inspirations = admin? ? Inspiration.all : Inspiration.published

    render json: @inspirations.map {|inspiration| InspirationSerializer.new(inspiration).attributes }
  end
end
