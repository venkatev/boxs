class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  skip_before_action :verify_authenticity_token
  before_action :prepare_meta_tags, :if => "request.get?"

  def current_user
    session[:user_id] ? User.find(session[:user_id]) : nil
  end
  helper_method :current_user

  def login_user(user)
    session[:user_id] = user.id
  end

  def logout_user
    session[:user_id] = nil
  end

  def logged_in?
    current_user.present?
  end

  helper_method :logged_in?

  def admin?
    request.host =~ /elasticbeanstalk.com/ || Rails.env.development?
  end
  helper_method :admin?

  private

  def prepare_meta_tags(options={})
    site_name = 'Boxs'
    title = 'Design Your Own Home'
    description = "Try how different design ideas for your home. Choose from hundreds of furniture models, decors and wall papers to create that perfect look for your home. It's fun and it's FREE."
    image = '/boxs_logo.png'
    current_url = request.url

    # Let's prepare a nice set of defaults
    defaults = {
        site: site_name,
        title: title,
        image: image,
        description: description,
        og: {
            url: current_url,
            site_name: site_name,
            title: title,
            image: image,
            description: description,
            type: 'website'
        }
    }

    options.reverse_merge!(defaults)
    options[:reverse] = true

    set_meta_tags options
  end
end
