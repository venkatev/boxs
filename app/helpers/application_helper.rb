module ApplicationHelper
  def room_type_name(room_type)
    room_type.humanize
  end

  def asset_url(path)
    "http://localhost:3001#{path}"
  end

  def open_url(path)
    `open http://localhost:3001#{path}`
  end

  def track_in_full_story?
    Rails.env.production? && !admin?
  end
end
