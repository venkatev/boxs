module LayoutEngineHelper
  def item_label(key, item)
    case(key)
      when 'room_type', 'object_category', 'ref_category'
        item.humanize

      when 'object_variant', 'ref_variant'
        size = item[:size]
        "W: #{size_average(size[:width])}ft. x D: #{size_average(size[:depth])}ft."

      when 'room_size'
        "W: #{size_average(item['room_length'])}ft. x D: #{size_average(item['room_breadth'])}ft."

      when 'ref_placement'
        item

      else
        raise "Unknown key #{key}"
    end
  end

  def item_value(key, item)
    case(key)
      when 'room_type', 'object_category', 'ref_category', 'room_size', 'ref_placement'
        item

      when 'object_variant', 'ref_variant'
        item[:id]

      else
        raise "Unknown key #{key}"
    end
  end

  def layout_facet_params(params, folders, key, value)
    key_index = folders.find_index {|f| f[:key] == key }
    keys_upto_current = folders[0..(key_index)].map {|f| f[:key] }
    params.slice(*keys_upto_current).merge(key => value)
  end

  def size_average(size_range)
    (size_range.split('-').map(&:to_f).sum / 2.0).round
  end
end