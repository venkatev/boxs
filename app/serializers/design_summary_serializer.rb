class DesignSummarySerializer < ActiveModel::Serializer
  attributes :id,
             :style,
             :cover_url,
             :layout

  def cover_url
    render = object.renders.reject(&:deferred?)[0]
    render ? render.image.url : nil
  end

  def layout
    {
      id: object.layout.id,
      furniture_items: object.layout.furniture_items
    }
  end
end
