class VendorFurnitureSerializer < ActiveModel::Serializer
  attributes :id,
             :vendor,
             :price

  def vendor
    { name: object.vendor.name, city: object.vendor.city, area: object.vendor.area }
  end
end
