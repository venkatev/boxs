class InspirationSerializer < ActiveModel::Serializer
  attributes :id, :image_url, :categories

  def image_url
    object.image.url
  end

  def categories
    object.furnitures.map(&:category).uniq
  end
end

