class CustomDesignSummarySerializer < ActiveModel::Serializer
  attributes :id,
             :moodboard_id,
             :image_url,
             :room_id,
             :deferred,
             :layout_number,
             :layout

  def moodboard_id
    object.design.moodboard_id
  end

  def deferred
    object.deferred?
  end

  def image_url
    if !object.deferred?
      object.design.primary_render.image.small.url
    end
  end

  def layout
    LayoutSerializer.new(object.layout) if object.layout
  end
end
