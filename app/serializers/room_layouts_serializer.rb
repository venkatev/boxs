class RoomLayoutsSerializer < ActiveModel::Serializer
  attributes :id,
             :room_category,
             :length,
             :breadth,
             :furniture_layouts

  def furniture_layouts
    object.furniture_layouts.map {|layout|
      {
        id: layout.id,
        items: layout.items
      }
    }
  end
end
