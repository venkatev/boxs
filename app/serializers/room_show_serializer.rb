class RoomShowSerializer < ActiveModel::Serializer
  attributes :id,
             :room_category,
             :length,
             :breadth,
             :height,
             # :designs,
             :camera_views,
             :designs

  def camera_views
    object.camera_views.map {|view| CameraViewSerializer.new(view).attributes }
  end

  def designs
    object.designs.map {|design| { id: design.id } }
  end
end
