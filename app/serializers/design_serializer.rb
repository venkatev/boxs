class DesignSerializer < ActiveModel::Serializer
  attributes :id,
             :item_mapping,
             :views,
             :categories

  def item_mapping
    _items = {}

    object.item_mapping.each {|identifier, value|
      _items[identifier] = {
        id: value['id'],
        furniture_id: value['id'],
        identifier: identifier,
        variant_id: value['variant_id'],
        category: LayoutEngineManager.get_super_category_for_variant(value['variant_id']),
        image_url: Furniture.find(value['id']).image_url,
        price: value['price']
      }
    }

    return _items
  end

  def views
    object.views[0..1].map do |view|
      DesignViewSerializer.new(view).attributes
    end
  end

  def categories
    object.furnitures.map(&:category).uniq
  end
end
