# == Schema Information
#
# Table name: camera_views
#
#  id                   :integer          not null, primary key
#  name                 :string
#  grobr_camera_view_id :string
#  created_at           :timestamp        not null
#  updated_at           :timestamp        not null
#  furniture_layout_id  :integer
#

class CameraViewSerializer < ActiveModel::Serializer
  attributes :id
end
