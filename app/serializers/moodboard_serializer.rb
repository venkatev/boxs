class MoodboardSerializer < ActiveModel::Serializer
  attributes :id,
             :room_type,
             :required_super_categories_map,
             :furniture_map,
             :furniture_ids_by_super_category

  def furniture_ids_by_super_category
    object.furniture_map.group_by {|e| e['super_category'] }
      .map {|super_category, entries| [super_category, entries[0]['furniture_ids']] }
      .to_h
  end
end
