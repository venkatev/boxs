class CustomDesignSerializer < ActiveModel::Serializer
  attributes :id,
             :moodboard_id,
             :room_id,
             :furniture_details,
             :furniture_mapping,
             :renders

  def moodboard_id
    object.design.moodboard_id
  end

  def furniture_details
    object.design.furniture_details
  end

  def furniture_mapping
    object.design.furniture_mapping
  end

  def renders
    object.design.renders.map do |render|
      RenderSerializer.new(render).attributes
    end
  end
end
