class RoomListingSerializer < ActiveModel::Serializer
  attributes :id,
             :room_category,
             :cover_image

  def cover_image
    rendered_views = object.designs.map(&:views).map(&:rendered).flatten

    if rendered_views.any?
      rendered_views[0].image.url
    else
      nil
    end
  end
end
