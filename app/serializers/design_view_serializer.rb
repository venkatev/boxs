class DesignViewSerializer < ActiveModel::Serializer
  attributes :id,
             :image_url,
             :deferred,
             :hotspots,
             :bounds_map

  def image_url
    object.image.url if object.rendered?
  end

  def hotspots
    map = {}

    object.bounds_map.each do |item_identifier, bounds|
      map[item_identifier] = {
        x: (((bounds['min_x'] + bounds['max_x']) / 2) / 1500.0).round(2),
        y: ((bounds['min_y'] + bounds['max_y']) / 2 / 1500.0).round(2)
      }
    end

    return map
  end

  def bounds_map
    map = {}

    object.bounds_map.each do |item_identifier, bounds|
      map[item_identifier] = {
        min_x: (bounds['min_x'] / 1500.0).round(2),
        max_x: (bounds['max_x'] / 1500.0).round(2),
        min_y: (bounds['min_y'] / 1500.0).round(2),
        max_y: (bounds['max_y'] / 1500.0).round(2)
      }
    end

    return map
  end
end
