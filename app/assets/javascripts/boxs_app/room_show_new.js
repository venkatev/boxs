'use strict';

angular.module('boxsApp')
.controller('DesignShowController', [
    '$scope', '$rootScope', '$http', '$routeParams', '$location', '$window', 'authService',
    function ($scope, $rootScope, $http, $routeParams, $location, $window, authService) {
        $rootScope.skipNav = true;
        $scope.isCustomizationMode = false;
        $scope.furnitureRecommendations = {};
        $scope.selectedHotspotIndex = null

        $scope.loadDesign = function(designId, renderIndex, hotspotIndex) {
            $scope.designId = designId;

            $http.get(
                '/designs/' + $scope.designId + '.json', {
                    cache: true
                }
            ).success(function(response) {
                $scope.design = response;

                if (renderIndex) {
                    $scope.setActiveRenderIndex(parseInt(renderIndex))
                }

                if (hotspotIndex) {
                    $scope.customize(true)
                    $scope.selectHotspot(parseInt(hotspotIndex), true)
                }
            });

            $rootScope.logEvent('load_design', 'design_id', $scope.designId);
        }

        $scope.customize = function(value) {
            $scope.isCustomizationMode = value;
        };

        /**
        * Returns the position of the currently viewed render within the design.
        */
        $scope.getActiveRenderIndex = function() {
            if (!$scope.design) {
                return null;
            }

            return _.findIndex($scope.design.renders, { active: true });
        };

        $scope.setActiveRenderIndex = function(index) {
            _.each($scope.design.renders, function(render, renderIndex) {
                render.active = renderIndex === index;
            });
        };

        /**
        * Switches to the render at the give +renderIndex+.
        */
        $scope.showRender = function(renderIndex) {
            const curIndex = $scope.getActiveRenderIndex()
            const update = () => {
                $scope.setActiveRenderIndex(renderIndex);
            }

            if ($scope.design.renders[renderIndex].deferred) {
                $scope.generateRender($scope.design.renders[renderIndex]).then(update)
            }
            else {
                update()
            }
        };

        $scope.selectHotspot = function(hotspotIndex, force) {
            if (!force && $scope.selectedHotspotIndex === hotspotIndex) {
                return;
            }

            $scope.selectedHotspotIndex = hotspotIndex;
            var activeRenderIndex = $scope.getActiveRenderIndex();

            if (activeRenderIndex === -1) {
                activeRenderIndex = 0;
            }

            const render = $scope.design.renders[activeRenderIndex];
            const hotspot = render.hotspots[hotspotIndex];

            $scope.selectedFurnitureItemId = hotspot.furniture_item_id;
            $scope.selectedFurniture = $scope.design.furniture_mapping[$scope.selectedFurnitureItemId];

            $scope.loadRecommendations(hotspot);
            $rootScope.logEvent('select_hotspot', 'hotspot_index', hotspotIndex);
        };

        $scope.deselectHotspot = function() {
            $scope.selectedHotspotIndex = null;
            $scope.selectedFurnitureItemId = null;
            $scope.selectedFurniture = null;
            $rootScope.logEvent('deselect_hotspot');
        };

        $scope.loadRecommendations = function(hotspot) {
            const furnitureId = $scope.design.furniture_mapping[hotspot.furniture_item_id].id;

            $http.get(
                '/designs/' + $scope.design.id + '/similar?furniture_item_id=' + $scope.selectedFurnitureItemId + '&furniture_id=' + furnitureId
            ).success(function(response) {
                $scope.furnitureRecommendations[furnitureId] = response;
            });
        };

        $scope.showRecommendation = function(furniture) {
            const oldHotspotIndex = $scope.selectedHotspotIndex;
            const oldRenderIndex = $scope.getActiveRenderIndex();

            $rootScope.messageProgressBar.show(
                ['Selecting furnitures', 'Arranging in your room', 'Rendering', 'Preparing your design' ]
            );

            $http.post(
                `/designs/${$scope.design.id}/replace_furniture`,
                {
                    item_identifier: $scope.selectedFurnitureItemId,
                    furniture: furniture,
                    render_index: oldRenderIndex
                }
            ).success(function(updatedDesign) {
                $window.location.href = `/designs/${updatedDesign.id}?render_index=${oldRenderIndex}&hotspot_index=${oldHotspotIndex}`;
            })
            .error(function() {
                $rootScope.messageProgressBar.close();
                $rootScope.modal.info("Sorry, we couldn't apply the changes to the design. Our support team will call you shortly.");
            });
        };

        $scope.getActiveRender = function() {
            return $scope.design.renders[$scope.getActiveRenderIndex()];
        };

        $scope.getActiveCameraView = function() {
            return $scope.getActiveRender().camera_view;
        };

        $scope.prevRender = function() {
            const curIndex = $scope.getActiveRenderIndex();
            const newIndex = ($scope.design.renders.length + curIndex - 1) % $scope.design.renders.length;
            $rootScope.logEvent('prev_render', 'current_index', curIndex);
            $scope.showRender(newIndex)
        };

        $scope.nextRender = function() {
            const curIndex = $scope.getActiveRenderIndex();
            const newIndex = ($scope.design.renders.length + curIndex + 1) % $scope.design.renders.length;
            $rootScope.logEvent('next_render', 'current_index', curIndex)
            $scope.showRender(newIndex)
        };

        $scope.generateRender = function(render) {
            $rootScope.messageProgressBar.show(
                ['Rendering'], 100
            );

            return $http.post(`/designs/${$scope.design.id}/generate_render?render_id=${render.id}`)
                .success(function(response) {
                    $.extend(render, response)
                })
                .error(function() {
                    $rootScope.modal.info("Sorry, something is not right. We are looking into it.");
                })
                .finally(function() {
                    $rootScope.messageProgressBar.close();
                });
        }

        $scope.saveDesign = function() {
            $rootScope.messageProgressBar.show(
                ['Saving the design'], 100
            );

            authService.login().then(function() {
                $http.post(`/designs/${$scope.design.id}/save_to_my_designs`)
                .success(function(response) {
                    $window.location.href = `/designs/${response.design_id}`;
                })
                .error(function() {
                    $rootScope.modal.info("Sorry, something is not right. We are looking into it.");
                })
                .finally(function() {
                    $rootScope.messageProgressBar.close();
                });
            });
        };

        $scope.applyToMyRoom = function() {
            authService.login().then(function() {
                $http.get('/rooms.json')
                .success(function(rooms) {
                    const showNewRoomDialog = function() {
                        $rootScope.modal.open({
                            templateUrl: 'newRoom.html',
                            controller: 'NewRoomController',
                            windowClass: 'new_room_dialog',
                            resolve: {
                                roomType: function() { return $scope.design.room_type },
                                moodboardId: function() { return $scope.design.moodboard_id },
                                isSample: function() { return false },
                                successCallback: function() {
                                    return function(room) {
                                        $window.location.href = '/rooms/' + room.id;
                                    };
                                }
                            }
                        });
                    }.bind(this);

                    if (rooms.length) {
                        $rootScope.modal.open({
                            templateUrl: 'roomSelector.html',
                            windowClass: 'room_selector_dialog',
                            controller: ['$scope', function ($selectorScope) {
                                $selectorScope.rooms = rooms;

                                $selectorScope.selectRoom = function(room) {
                                    $selectorScope.selectedRoom = room;
                                };

                                $selectorScope.submit = function() {
                                    $rootScope.modal.close();
                                    $rootScope.messageProgressBar.show(
                                        ['Selecting Furnitures', 'Arranging in your room', 'Rendering', 'Preparing your design' ], 7
                                    );

                                    $http.post(
                                        '/rooms/' + $selectorScope.selectedRoom.id + '/save_design',
                                        {
                                            moodboard_id: $scope.design.moodboard_id
                                        }
                                    )
                                    .success(function(response) {
                                        $window.location.href = `/designs/${response.design_id}`;
                                    })
                                    .error(function() {
                                        $rootScope.messageProgressBar.close();
                                        $rootScope.modal.info("Sorry, something is not right. We are looking into it.");
                                    });
                                };

                                $selectorScope.addRoom = function() {
                                    showNewRoomDialog();
                                }
                            }]
                        });
                    }
                    else {
                        showNewRoomDialog();
                    }
                });
            });
        };
    }
]);