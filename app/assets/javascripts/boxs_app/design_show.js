'use strict';

angular.module('boxsApp')
.controller('DesignShowController', [
'$scope', '$rootScope', '$http', '$routeParams', '$location', '$window', 'authService',
    function ($scope, $rootScope, $http, $routeParams, $location, $window, authService) {
        $rootScope.skipNav = true;
        $scope.isCustomizationMode = false;
        $scope.inspirationCategory = null
        $scope.inspirations = []
        $scope.selectedItemIdentifier = null
        $scope.designOptions = []
        $scope.activeInspiration = null
        $scope.inspirationFilters = {
            options: [],
            selected: null
        }
        $scope.showPrices = false
        $scope.numChanges = 5
        $scope.MIN_CHANGES_FOR_LOGIN = 0

        $scope.init = function(roomId) {
            $scope.roomId = roomId;
            $scope.activeDesignIndex = 0;
            $scope.previousDesign = null;
            $scope.activeDesign = null;

            $scope.roomLoader = $http.get(
                '/rooms/' + $scope.roomId + '.json'
            ).success(function(response) {
                $scope.room = response
                $scope.loadDesign(0)
            });
        }

        $scope.loadDesign = function(designIndex) {
            $scope.activeDesignIndex = designIndex
            $scope.previousDesign = $scope.activeDesign
            $scope.activeDesign = $scope.room.designs[$scope.activeDesignIndex]
            $scope.designOptions = [$scope.activeDesign, $scope.previousDesign]
            $scope.selectedItemIdentifier = null

            if (!$scope.activeDesign.loaded) {
                $http.get(
                    '/designs/' + $scope.activeDesign.id + '.json', {
                        cache: true
                    }
                ).success(function(response) {
                    $.extend($scope.activeDesign, response)
                    $scope.activeDesign.loaded = true
                    $scope.loadInspirations()
                });
            }

            $rootScope.logEvent('load_design', 'design_id', $scope.activeDesign.id);
        }

        $scope.prevDesign = function() {
            if (!$scope.hasPrevDesign()) {
                return false;
            }

            $scope.activeDesignIndex--;
            $scope.loadDesign($scope.activeDesignIndex)
        }

        $scope.nextDesign = function() {
            if (!$scope.hasNextDesign()) {
                return
            }

            if ($scope.activeDesignIndex < $scope.room.designs.length - 1) {
                $scope.activeDesignIndex++;
                $scope.loadDesign($scope.activeDesignIndex)
            }
        }

        $scope.toggleHotspot = function($event, itemIdentifier) {
            $event.stopPropagation();

            if ($scope.selectedItemIdentifier === itemIdentifier) {
                $scope.deselectHotspot()
            }
            else {
                $scope.selectHotspot(itemIdentifier)
            }
        }

        $scope.selectHotspot = function(itemIdentifier, force) {
            if (!force && $scope.selectedItemIdentifier === itemIdentifier) {
                return;
            }

            $scope.selectedItemIdentifier = itemIdentifier
            const view = $scope.activeDesign.views[0]
            const hotspot = view.hotspots[itemIdentifier]
            $scope.loadInspirations()
            $rootScope.logEvent('select_hotspot', 'hotspot_index', itemIdentifier);
            $scope.inspirationFilters.selected = _.find($scope.inspirationFilters.options, (item) => item.value === $scope.activeDesign.item_mapping[itemIdentifier].category)
        }

        $scope.deselectHotspot = function() {
            $scope.selectedItemIdentifier = null
            $rootScope.logEvent('deselect_hotspot')
            $scope.inspirationFilters.selected = $scope.inspirationFilters.options[0]
        }

        $scope.customize = function(value) {
            $scope.isCustomizationMode = value;
        };

        $scope.loadInspirations = function() {
            var params = {}

            if ($scope.activeDesign && $scope.inspirationCategory) {
                params.furniture = $scope.inspirationCategory
            }

            $http.get('/inspirations.json', { params: params })
                .success(function(response) {
                    $scope.inspirations = response;

                    const categories = _.uniq(_.flatten(_.map($scope.inspirations, 'categories')))

                    const newFilterOptions = []
                    let selectedFilter = null

                    newFilterOptions.push({
                        value: null,
                        label: 'Looks'
                    })

                    _.each(categories, (category) => {
                        newFilterOptions.push({
                            value: category,
                            label: $scope.getCategoryDisplayName(category)
                        })
                    })

                    if ($scope.inspirationFilters.selected) {
                        selectedFilter = _.find(newFilterOptions, (item) => item.value === $scope.inspirationFilters.selected.value)
                    }

                    if (!selectedFilter) {
                        selectedFilter = newFilterOptions[0]
                    }

                    $scope.inspirationFilters.options = newFilterOptions
                    $scope.inspirationFilters.selected = selectedFilter

                    _.each($scope.inspirations, (inspiration) => {
                        inspiration.categoriesCount = inspiration.categories.length
                    })
                });
        };

        $scope.getCategoryDisplayName = function(category) {
            return {"sofa_set":"Sofa set","dining_set":"Dining set","chest_of_drawers":"Chest of drawers","book_shelf":"Book shelf","console_table":"Console table","tv_unit":"Tv unit","sideboard":"Sideboard","wall_surface":"Wall surface","crockery_unit":"Crockery unit","foyer_chest":"Foyer chest","painting":"Painting",'wall_shelf':'Wall shelf'}[category]
        }

        $scope.shouldShowInspiration = function(inspiration) {
            if (!$scope.activeDesign.categories) {
                return false
            }

            if (!inspiration.categories.length) {
                // No furniture tagged. Skip this inspiration.
                return false
            }

            if ($scope.inspirationFilters.selected && !$scope.inspirationFilters.selected.value) {
                // If 'All' filter is selected, then show this inspiration if it contains all the categories needed for the room.
                if (_.intersection($scope.activeDesign.categories, inspiration.categories).length === $scope.activeDesign.categories.length) {
                    return true
                }
            }

            return _.contains(inspiration.categories, $scope.inspirationFilters.selected.value)
        }

        $scope.showApplyBox = function(inspiration) {
            $scope.activeInspiration = inspiration
        }

        $scope.hideApplyBox = function($event, inspiration) {
            $event.stopPropagation();
            $scope.activeInspiration = null
        }

        $scope.applyInspiration = function(inspiration) {
            $scope.numChanges += 1

            function perform() {
                $rootScope.messageProgressBar.show(
                    ['Preparing your design' ]
                );

                $http.post(
                    `/designs/${$scope.activeDesign.id}/replace_furniture`,
                    {
                        inspiration_id: inspiration.id,
                        category: $scope.inspirationFilters.selected.value
                    }
                ).success(function(newDesign) {
                    $rootScope.messageProgressBar.close();
                    newDesign.loaded = true
                    $scope.room.designs.push(newDesign)
                    $scope.activeDesignIndex = $scope.room.designs.length - 1
                    $scope.loadDesign($scope.activeDesignIndex)
                    $scope.loadInspirations()
                })
                .error(function(response, status) {
                    $rootScope.messageProgressBar.close();

                    if (status === 422) {
                        $rootScope.modal.info("Sorry, this design couldn't applied.");
                    }
                    else {
                        $rootScope.modal.info("Sorry, we couldn't apply the changes to the design. Our support team will call you shortly.");
                    }
                });
            }

            if ($scope.numChanges > $scope.MIN_CHANGES_FOR_LOGIN) {
                authService.login().then((user) => {
                    $http.put(`/rooms/${$scope.roomId}`, { user_id: user.id }).success(perform)
                })
            }
            else {
                perform()
            }
        };

        $scope.saveDesign = function() {
            $rootScope.messageProgressBar.show(
                ['Saving the design'], 100
            );

            authService.login().then(function() {
                $http.post(`/designs/${$scope.activeDesign.id}/save_to_my_designs`)
                .success(function(response) {
                    $window.location.href = `/designs/${response.design_id}`;
                })
                .error(function() {
                    $rootScope.modal.info("Sorry, something is not right. We are looking into it.");
                })
                .finally(function() {
                    $rootScope.messageProgressBar.close();
                });
            });
        };

        $scope.hasPrevDesign = function() {
            return $scope.activeDesignIndex > 0
        }

        $scope.hasNextDesign = function() {
            return $scope.activeDesignIndex < $scope.room.designs.length - 1
        }

        $scope.showDebugInfo = function() {
            const message = $scope.activeDesign

            $rootScope.modal.open({
                templateUrl: "info-modal",
                windowClass: 'room_selector_dialog',
                controller: ['$scope', function ($scope) {
                    $scope.message = message;
                }]
            });
        }

        $scope.getTotalPrice = function() {
            if (!$scope.activeDesign.views[0].hotspots) {
                return 0
            }

            let totalPrice = 0

            _.each($scope.activeDesign.item_mapping, (item, identifier) => {
                if ($scope.activeDesign.views[0].hotspots[identifier]) {
                    totalPrice += item.price
                }
            })

            return totalPrice
        }

        $scope.togglePrice = function() {
            $scope.showPrices = !$scope.showPrices
        }
    }
]);

$(function() {
    $('body').keydown(function (event) {
        const eventCharCode = String.fromCharCode(event.which).toLowerCase();
        const scope = angular.element('.design_show').scope()

        if (eventCharCode === 'i' && event.ctrlKey) {
            // Ctrl + I.
            scope.$apply(function () {
                scope.showDebugInfo()
            });
        }
    })
});
