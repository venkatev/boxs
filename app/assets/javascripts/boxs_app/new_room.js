'use strict';

angular.module('boxsApp')
.controller('NewRoomController', [
  '$scope', '$rootScope', '$http', '$window', '$timeout', 'moodboardId', 'roomType', 'successCallback', 'isSample',
  function ($scope, $rootScope, $http, $window, $timeout, moodboardId, roomType, successCallback, isSample) {
    $scope.step = 1;
    $scope.allOrientations = [ 1, 2, 3, 4 ];
    $scope.orientation = 1;
    $scope.selectedIndex = null;
    $scope.pixelsPerFeet = $window.innerWidth > 768 ? 40 : 20;
    $scope.bottomShift = $window.innerWidth > 768 ? 150 : 250;
    $scope.objetMoveStep = 1;
    $scope.superCategories = [
        { name: 'sofa_set',         label: 'Sofa',               status: 'published', selected: true, force_major: true, major: true },
        { name: 'tv_unit',          label: 'Entertainment Unit', status: 'published', selected: true, force_major: true, major: true },
        { name: 'dining_set',       label: 'Dining Set',         status: 'published', selected: true, force_major: true, major: true },
        { name: 'crockery_unit',    label: 'Crockery',           status: 'published', selected: true, major: true },
        { name: 'foyer_chest',      label: 'Foyer Chest',        status: 'published', selected: true, major: true },
        { name: 'buffet',           label: 'Buffet',             status: 'published', selected: true, major: false, optional: true },
        { name: 'sideboard',        label: 'Sideboard',          status: 'draft' },
        { name: 'chest_of_drawers', label: 'Additional Storage', status: 'draft' }
    ];

    $scope.room = {
        type: roomType,
        height: 10,
        length: null,
        breadth: null,
        entranceWall: null,
        entranceSide: null,
        objects: []
    };

    $scope.init = function() {
        $http.get(`/rooms/new.json?room_type=${$scope.room.type}`)
            .success(function(data) {
                $scope.roomSizes = data.room_sizes;
                $scope.shellObjectsInfo = data.shell_objects_info;

                // Convert dimension ranges to average numbers.
                $scope.shellObjectsInfo = _.map($scope.shellObjectsInfo, (info) => {
                    info.size = {
                        height: $scope.getAverageSize(info.size.height),
                        width: $scope.getAverageSize(info.size.width),
                        depth: $scope.getAverageSize(info.size.depth)
                    };

                    return info;
                });

                $scope.getActualRoomSize = function() {
                    return $scope.roomSizes[parseInt($scope.roomSizeIndex)];
                };

                $scope.getGridRoomSize = function() {
                    const actualSize = $scope.getActualRoomSize();

                    return {
                        length1: $scope.getAverageSize(actualSize.length1_range),
                        length2: $scope.getAverageSize(actualSize.length2_range)
                    };
                }
            });
    };

    $scope.setEntranceSide = function(value) {
        $scope.room.entranceSide = value;
    };

    $scope.getAverageSize = function(valueRange) {
        const values = valueRange.split('-');
        return Math.round((parseFloat(values[0]) + parseFloat(values[1])) / 2);
    };

    $scope.back = function() {
        if ($scope.step !== 1) {
            $scope.step--;
        }
    };

    $scope.proceed = function() {
        if ($scope.step == 1) {
            const gridSize = $scope.getGridRoomSize();

            if ($scope.room.entranceWall === '1') {
                $scope.room.length = gridSize.length1;
                $scope.room.breadth = gridSize.length2;
            }
            else {
                $scope.room.length = gridSize.length2;
                $scope.room.breadth = gridSize.length1;
            }

            $scope.step = 2;
        }
        else if ($scope.step == 2) {
            $scope.room.objects = [];

            // FIXME
            var entranceDoorLeft = $scope.room.entranceSide === 'r' ? $scope.room.length - 3 : 0;
            $scope.addObject($scope.shellObjectsInfo[0].category, $scope.shellObjectsInfo[0].id, entranceDoorLeft);
            $scope.deselectObject();

            $scope.step = 3;
        }
        else if ($scope.step == 3) {
            const outerScope = $scope;

            swal({
                title: 'Have you added all door and windows?',
                showCancelButton: true,
                confirmButtonText: 'Yes, Proceed',
                cancelButtonText: "No, I haven't"
            }, function (isConfirmed) {
                if (isConfirmed) {
                    outerScope.$apply(function () {
                        outerScope.step = 4;
                    })
                }
            });
        }
        else if ($scope.step == 4) {
            $rootScope.messageProgressBar.show(
                ['Creating your room', 'Arranging furnitures', 'Preparing the layouts' ], 2
            );

            $http.post('/rooms/create_with_layout', {
                room: $scope.room,
                moodboard_id: moodboardId,
                is_sample: isSample,
                super_categories: $scope.superCategories
            }).success(function(response) {
                $window.location.href = `/rooms/${response.room_id}`;
            })
            .error(function() {
                $rootScope.messageProgressBar.close();
                $rootScope.modal.info("Sorry, we couldn't apply the design to your room. Our support team will call you shortly.");
            });
        }
    };

    $scope.getRoomCSSStyle = function() {
        const style = {
            '--roomHeight': $scope.room.height,
            '--pixelRatio': $scope.pixelsPerFeet + 'px',
            '--bottomShift': $scope.bottomShift + 'px'
        };

        if ($scope.orientation == 1 || $scope.orientation == 3) {
            style['--roomLength'] = $scope.room.length;
            style['--roomBreadth'] = $scope.room.breadth;
        }
        else {
            style['--roomLength'] = $scope.room.breadth;
            style['--roomBreadth'] = $scope.room.length;
        }

        return style;
    };

    $scope.selectObject = function(index) {
        if (index === 0) {
            return;
        }

        $scope.selectedIndex = index;
    };

    $scope.deselectObject = function() {
        $scope.selectedIndex = null;
    };

    $scope.addObject = function(category, variantId, leftPosition) {
        const details = _.find($scope.shellObjectsInfo, { id: variantId });

        $scope.room.objects.push({
            category: category,
            variant_id: variantId,
            name: details.name,
            size: details.size,
            orientation: $scope.orientation,
            left: leftPosition || 0,
            bottom: details.y_position
        });

        $scope.selectObject($scope.room.objects.length - 1);
    };

    $scope.removeObject = function() {
        $scope.room.objects.splice($scope.selectedIndex, 1);
        $scope.deselectObject();
    };

    $scope.getWallWidth = function(orientation) {
        return (orientation == 1 || orientation == 3) ? $scope.room.length : $scope.room.breadth;
    };

    $scope.getWallStyle = function(orientation) {
        // TODO
        return {
            height: $scope.feetToPixel($scope.room.height),
            width: $scope.feetToPixel($scope.getWallWidth(orientation))
        };
    };

    $scope.pixelToFeet = function(pixel) {
        return pixel / $scope.pixelsPerFeet;
    };

    $scope.feetToPixel = function(feet) {
        return (feet * $scope.pixelsPerFeet) + 'px';
    };

    $scope.getObjectStyle = function(object) {
        return {
            height: $scope.feetToPixel(object.size.height),
            width: $scope.feetToPixel(object.size.width),
            bottom: $scope.feetToPixel(object.bottom),
            left: $scope.feetToPixel(object.left)
        };
    };

    $scope.getOrientation = function(position) {
        switch (position) {
            case 'main':
            return $scope.orientation;

            case 'left':
            return ((4 + ($scope.orientation - 1 - 1)) % 4) + 1;

            case 'right':
            return ((4 + ($scope.orientation - 1 + 1)) % 4) + 1;

            case 'back':
            return ((4 + ($scope.orientation - 1 + 2)) % 4) + 1;
        }
    };

    $scope.turn = function(direction) {
        $scope.orientation = $scope.getOrientation(direction);
    };

    $scope.move = function(direction) {
        const object = $scope.room.objects[$scope.selectedIndex];
        object.left = object.left + (direction === 'left' ? -$scope.objetMoveStep : $scope.objetMoveStep);
        const wallWidth = $scope.getWallWidth(object.orientation);

        if ((object.left + object.size.width) > wallWidth) {
            $scope.align('right');
        }
        else if (object.left < 0) {
            $scope.align('left');
        }
    };

    $scope.align = function(direction) {
        const object = $scope.room.objects[$scope.selectedIndex];

        if (direction === 'left') {
            object.left = 0;
        }
        else if (direction === 'right') {
            const objectWidth = object.size.width;
            const wallWidth = $scope.getWallWidth(object.orientation);
            object.left = wallWidth - objectWidth;
        }
        else if (direction === 'center') {
            const objectWidth = object.size.width;
            const wallWidth = $scope.getWallWidth(object.orientation);
            object.left = (wallWidth - objectWidth) / 2;
        }
    };

    $scope.toggleSuperCategory = (superCategory) => {
        if (superCategory.status === 'published') {
            superCategory.selected = !superCategory.selected
        }
    };
  }
])