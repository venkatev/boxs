'use strict';

angular.module('boxsApp')
.controller('RoomIndexController', [
    '$scope', '$http', '$rootScope',
    function ($scope, $http, $rootScope) {
        $scope.init = function() {
            $rootScope.skipNav = false;
            $rootScope.logEvent('my_rooms');
            $scope.roomLoader = $http.get(
                '/rooms.json'
            ).success(function(response) {
                $scope.rooms = response;
            });
        }
    }
]);