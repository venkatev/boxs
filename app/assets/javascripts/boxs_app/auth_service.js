'use strict';

angular.module('boxsApp')
.service('authService', ['$modal', '$rootScope', '$http', '$q', '$window', function ($modal, $rootScope, $http, $q, $window) {
    return {
        getCurrentUser: function() {
            $http.get(
                '/sessions/user'
            ).then(
                function(response) {
                    $rootScope.currentUser = response.data;
                }, function() {
                    // Not logged in.
                }
            );
        },
        login: function () {
            if ($rootScope.currentUser && $rootScope.currentUser.name !== 'Guest') {
                return $q(function (resolve, reject) {
                    resolve($rootScope.currentUser);
                });
            }
            else {
                var modalInstance = $rootScope.modal.open({
                    templateUrl: 'login.htm',
                    controller: 'LoginModalCtrl',
                    windowClass: 'login_dialog',
                    keyboard: false,
                    backdrop: 'static'
                });

                return modalInstance.result.then(function (user) {
                    $rootScope.currentUser = user;
                    return user;
                });
            }
        },
        logout: function () {
            $http.delete(
                '/sessions/logout'
            ).then(
                function(response) {
                    $rootScope.currentUser = null;
                    $window.location = '/';
                }
            );
        }
    }
}])
.controller('LoginModalCtrl', ['$scope', '$http', '$rootScope', '$window', function ($scope, $http, $rootScope, $window) {
    $scope.mobile = null;

    $scope.submitUserLogin = function () {
        if (!/^\d{10}$/.test($scope.mobile)) {
            alert('The mobile number is invalid');
            return;
        }

        $scope.errors = null;

        $http.post('/sessions/login', {
            mobile: $scope.mobile
        }).success(function (currentUser) {
            if ($window.gtag) {
              $window.gtag('set', {'user_id': currentUser.id});
            }

            $rootScope.modal.close(currentUser);
        }).error(function (message) {
            $rootScope.modal.info(message);
        }).finally(function () {
        });
    };
}]);