'use strict';

var app = angular.module('boxsApp', [
    'ui.bootstrap',
    'angular-underscore',
    'ngDialog',
    'ngRoute',
    'Devise',
    'sticky',
    'ui.select'
])
.run([
    '$http', '$rootScope', '$modal', '$window', '$timeout', 'authService',
    function ($http, $rootScope, $modal, $window, $timeout, authService) {
        $rootScope.initApp = function() {
            authService.getCurrentUser();
        };

        $rootScope.login = function() {
            authService.login();
        };

        $rootScope.logout = function() {
            authService.logout();
        };

        $rootScope.logEvent = function (eventAction, eventLabel, eventValue) {
            if ($window.gtag) {
              $window.gtag('event', eventAction, {
                'event_label': eventLabel,
                'value': eventValue
              });
            };
        };

        $rootScope.messageProgressBar = {
            allMessages: [],
            speed: 5,
            index: 0,
            message: null,
            active: false,
            progress: 0,
            show: function(messages, speed) {
                $rootScope.messageProgressBar.allMessages = messages.concat(['Completing...']);
                $rootScope.messageProgressBar.speed = $rootScope.messageProgressBar.speed || speed;
                $rootScope.messageProgressBar.active = true;
                $rootScope.messageProgressBar.index = 0;
                $rootScope.messageProgressBar.tickerCallback();

                $rootScope.modal.open({
                    templateUrl: "progress_modal",
                    windowClass: 'progress_modal',
                    backdropClass: 'progress_backdrop',
                    keyboard: false,
                    backdrop: 'static',
                    controller: ['$scope', function ($scope) {

                    }]
                });
            },
            close: function() {
                $rootScope.messageProgressBar.active = false;
                $rootScope.modal.close();
            },
            tickerCallback: function() {
                if (!$rootScope.messageProgressBar.active) {
                    return;
                }

                $rootScope.messageProgressBar.progress = Math.round((($rootScope.messageProgressBar.index + 1) / $rootScope.messageProgressBar.allMessages.length) * 100);
                $rootScope.messageProgressBar.message = $rootScope.messageProgressBar.allMessages[$rootScope.messageProgressBar.index];

                if ($rootScope.messageProgressBar.index < $rootScope.messageProgressBar.allMessages.length - 1) {
                    $rootScope.messageProgressBar.index += 1;
                    $timeout($rootScope.messageProgressBar.tickerCallback, $rootScope.messageProgressBar.speed * 1000);
                }
            }
        };

        $rootScope.modal = {
            _dialogs: [],
            open: function (opts) {
                var dialog = $modal.open(opts);

                dialog.opened.then(function () {
                    if (opts.callbacks && opts.callbacks.opened) {
                        opts.callbacks.opened();
                    }
                });

                if (opts.callbacks && opts.callbacks.finally) {
                    dialog.result.finally(opts.callbacks.finally);
                }

                delete opts.callbacks;

                $rootScope.modal._dialogs.push(dialog);
                return dialog;
            },
            ok: function () {
                var dialog = $rootScope.modal._dialogs.pop();

                if (dialog) {
                    dialog.close();
                }
            },
            close: function (data) {
                var dialog = $rootScope.modal._dialogs.pop();

                if (dialog) {
                    dialog.close(data);
                }
            },
            dismiss: function () {
                var dialog = $rootScope.modal._dialogs.pop();

                if (dialog) {
                    dialog.dismiss();
                }
            },
            info: function (message) {
                $rootScope.modal.open({
                    templateUrl: "info-modal",
                    windowClass: 'info-modal',
                    controller: ['$scope', function ($scope) {
                        $scope.message = message;
                    }]
                });
            },
            isOpen: function () {
                return $('.modal-dialog').is(':visible');
            }
        };

        $rootScope.loader = {
            show: function(message) {
                swal({
                    title: message,
                    showCancelButton: false,
                    showConfirmButton: false,
                    animation: true,
                    type: 'info'
                });
            },
            close: function() {
                swal.close();
            }
        };

        $rootScope.furnitureItemImage = function(variantIdentifier, orientation) {
            return `/layout_engine/top_view_images/${variantIdentifier}_${orientation}.png`;
        };

        $rootScope.startDesigning = function() {
            $rootScope.modal.open({
                templateUrl: 'room_selector.html',
                controller: 'RoomSelectorController',
                windowClass: 'room_selector_dialog',
                backdrop: 'static',
                keyboard: false,
                resolve: {

                }
            });
        }

        $rootScope.applyToMyRoom = function() {
            authService.login().then(function() {
                $http.get('/rooms.json')
                .success(function(rooms) {
                    const showNewRoomDialog = function() {
                        $rootScope.modal.open({
                            templateUrl: 'newRoom.html',
                            controller: 'NewRoomController',
                            windowClass: 'new_room_dialog',
                            backdrop: 'static',
                            keyboard: false,
                            resolve: {
                                roomType: function() { return 'living_room' },
                                // TODO Hard-code the moodboard.
                                moodboardId: function() { return 80 },
                                isSample: function() { return false },
                                successCallback: function() {
                                    return function(room) {
                                        $window.location.href = '/rooms/' + room.id;
                                    };
                                }
                            }
                        });
                    }.bind(this);

                    // if (rooms.length) {
                    //     $rootScope.modal.open({
                    //         templateUrl: 'roomSelector.html',
                    //         windowClass: 'room_selector_dialog',
                    //         controller: ['$scope', function ($selectorScope) {
                    //             $selectorScope.rooms = rooms;

                    //             $selectorScope.selectRoom = function(room) {
                    //                 $selectorScope.selectedRoom = room;
                    //             };

                    //             $selectorScope.submit = function() {
                    //                 $rootScope.modal.close();
                    //                 $rootScope.messageProgressBar.show(
                    //                     ['Selecting Furnitures', 'Arranging in your room', 'Rendering', 'Preparing your design' ], 7
                    //                 );

                    //                 $http.post(
                    //                     '/rooms/' + $selectorScope.selectedRoom.id + '/save_design',
                    //                     {
                    //                         moodboard_id: $scope.design.moodboard_id
                    //                     }
                    //                 )
                    //                 .success(function(response) {
                    //                     $window.location.href = `/designs/${response.design_id}`;
                    //                 })
                    //                 .error(function() {
                    //                     $rootScope.messageProgressBar.close();
                    //                     $rootScope.modal.info("Sorry, something is not right. We are looking into it.");
                    //                 });
                    //             };

                    //             $selectorScope.addRoom = function() {
                    //                 showNewRoomDialog();
                    //             }
                    //         }]
                    //     });
                    // }
                    // else {
                        showNewRoomDialog();
                    // }
                });
            });
        };
    }
])
.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/', {
       templateUrl: 'dashboard.htm',
       controller: 'DashboardController'
    })
    .when('/room/:roomId', {
       templateUrl: 'room.htm',
       controller: 'RoomController'
    })
    .when('/room/:roomId/design/:designId', {
        templateUrl: 'design.htm',
        controller: 'DesignController'
     })
     .otherwise ({
       redirectTo: '/'
    });
 }])
.directive('imgOnLoad', ['$parse', function ($parse) {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        $(elem).closest('.design').hide()

        elem.on('load', function (event) {
          setTimeout(() => {
              scope.$apply(function() {
                  $(elem).closest('.design').show()
              });
          }, 100)
        });
      }
    };
  }])
;
