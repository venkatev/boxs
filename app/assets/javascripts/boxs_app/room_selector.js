'use strict';

angular.module('boxsApp')
.controller('RoomSelectorController', [
  '$scope', '$rootScope', '$http', '$window', '$timeout',
  function ($scope, $rootScope, $http, $window, $timeout) {
    $scope.roomTypes = [
      { id: 'living_room', name: 'Living area', image: '/room_layouts/room_types/living_room.jpg' },
      { id: 'dining_room', name: 'Dining area', image: '/room_layouts/room_types/dining.jpg', draft: true },
      { id: 'master_bedroom', name: 'Master bedroom', image: '/room_layouts/room_types/master_bedroom.jpg', draft: true },
      { id: 'kids_room', name: 'Kids room', image: '/room_layouts/room_types/kids_room.jpg', draft: true },
      { id: 'kitchen', name: 'Kitchen', image: '/room_layouts/room_types/dining.jpg', draft: true }
    ]

    $scope.selectedRoomType = null
    $scope.selectedTemplate = null
    $scope.step = 1

    $scope.init = function(roomId) {
        $scope.selectedRoomType = $scope.roomTypes[0]
    }

    $scope.selectRoomType = function(roomType) {
        if (roomType.draft) {
            return
        }

        $scope.selectedRoomType = roomType
    }

    $scope.selectTemplate = function(roomTemplate) {
        $scope.selectedTemplate = roomTemplate
    }

    $scope.prev = function() {
        if ($scope.step = 1) {
            return
        }

        $scope.step = $scope.step - 1;
    }

    $scope.next = function() {
        if ($scope.step === 1) {
            $http.get(`/rooms/select.json?room_type=${$scope.selectedRoomType.id}`)
              .success(function(response) {
                  $scope.step = 2
                  $scope.roomTemplates = response;
                  $scope.selectedTemplate = $scope.roomTemplates[0]
              });
        }
        else if ($scope.step === 2) {
            $rootScope.messageProgressBar.show(
                ['Creating your room', 'Selecting furnitures', 'Preparing the design'], 100
            );

            $http.post('/rooms', { room_template_id: $scope.selectedTemplate.id })
            .success(function(roomId) {
                $window.location.href = `/rooms/${roomId}`;
            })
            .error(function() {
                $rootScope.messageProgressBar.close();
                alert('Sorry, somethign went wrong');
            });
        }
    }
  }
])