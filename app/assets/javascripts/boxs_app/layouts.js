'use strict';

angular.module('boxsApp')
.controller('LayoutsController', [
    '$scope', '$rootScope', '$http', '$window',
    function ($scope, $rootScope, $http, $window) {
        $scope.init = function(roomId) {
            $scope.roomId = roomId;

            $http.get(
              '/rooms/' + $scope.roomId + '/layouts.json'
            ).success(function(response) {
              $scope.room = response;
            });
        }

        $scope.layoutPreview = {
            PIXELS_PER_FEET: 15,
            CELL_FEET_SIZE: 1,
            ROWS: _.map(_.range(1, 50), (i) => { return i.toString() } ),
            COLUMNS: _.map(_.range(65, 65 + 26).concat(_.range(97, 97 + 26)), (i) => { return String.fromCharCode(i)} ),

            isHorizontal: function(orientation) {
                return orientation == 1 || orientation == 3;
            },
            getStartRow: function(cellRange) {
                const matches = cellRange.match(/^\d+/);
                return _.indexOf($scope.layoutPreview.ROWS, matches[0]);
            },
            getStartColumn: function(cellRange) {
                const matches = cellRange.match(/^\d+([A-Z]|[a-z])/);
                return _.indexOf($scope.layoutPreview.COLUMNS, matches[1]);
            },
            getObjectRows: function(orientation, dimensions) {
                return Math.round(($scope.layoutPreview.isHorizontal(orientation) ? dimensions.depth : dimensions.width) / $scope.layoutPreview.CELL_FEET_SIZE);
            },
            getObjectCols: function(orientation, dimensions) {
                return Math.round(($scope.layoutPreview.isHorizontal(orientation) ? dimensions.width : dimensions.depth) / $scope.layoutPreview.CELL_FEET_SIZE);
            },
            getDimensions: function(furnitureItem) {
                function average(range) {
                    const values = _.map(range.split('-'), (v) => { return parseFloat(v) });
                    return (values[0] + values[1]) / 2;
                };

                return {
                    height: average(furnitureItem.size.height),
                    width: average(furnitureItem.size.width),
                    depth: average(furnitureItem.size.depth)
                };
            },
            getRoomStyle: function() {
                return {
                    width: $scope.room.length * $scope.layoutPreview.PIXELS_PER_FEET,
                    height: $scope.room.breadth * $scope.layoutPreview.PIXELS_PER_FEET,
                    backgroundSize: $scope.layoutPreview.PIXELS_PER_FEET * 4
                }
            },
            getFurnitureItemStyle: function(furnitureItem) {
                const objectStyle = {};
                const dimensions = $scope.layoutPreview.getDimensions(furnitureItem);

                if (furnitureItem.orientation === 1 || furnitureItem.orientation === 3) {
                  objectStyle.width = (dimensions.width * 100 / $scope.room.length) + '%';
                  objectStyle.height = (dimensions.depth * 100 / $scope.room.breadth) + '%';
                }
                else {
                  objectStyle.height = (dimensions.width * 100 / $scope.room.breadth) + '%';
                  objectStyle.width = (dimensions.depth * 100 / $scope.room.length) + '%';
                }

                var refBounds;

                var left = $scope.layoutPreview.getStartColumn(furnitureItem.cell_range) * $scope.layoutPreview.CELL_FEET_SIZE;
                var top = $scope.layoutPreview.getStartRow(furnitureItem.cell_range) * $scope.layoutPreview.CELL_FEET_SIZE;

                const alignment = furnitureItem.alignments[0];

                if (alignment.h_align.ref === 'c' || alignment.d_align.ref === 'c') {
                  const startRowIndex = $scope.layoutPreview.getStartRow(furnitureItem.cell_range);
                  const startColIndex = $scope.layoutPreview.getStartColumn(furnitureItem.cell_range);
                  const endRowIndex = startRowIndex + ($scope.layoutPreview.getObjectRows(furnitureItem.orientation, dimensions) - 1);
                  const endColIndex = startColIndex + ($scope.layoutPreview.getObjectCols(furnitureItem.orientation, dimensions) - 1);

                  refBounds = {
                    min_x: startColIndex * $scope.layoutPreview.CELL_FEET_SIZE,
                    max_x: Math.min((endColIndex + 1) * $scope.layoutPreview.CELL_FEET_SIZE, $scope.room.length),
                    min_z: startRowIndex * $scope.layoutPreview.CELL_FEET_SIZE,
                    max_z: Math.min((endRowIndex + 1) * $scope.layoutPreview.CELL_FEET_SIZE, $scope.room.breadth),
                  };
                }

                if (refBounds) {
                  if (
                    (furnitureItem.orientation == 1 && alignment.h_align.value == 'r_r') ||
                    (furnitureItem.orientation == 3 && alignment.h_align.value == 'l_l')
                  ) {
                    left += refBounds.max_x - refBounds.min_x - (dimensions.width);
                  }

                  if (
                    (furnitureItem.orientation == 2 && alignment.d_align.value == 'b_b') ||
                    (furnitureItem.orientation == 4 && alignment.d_align.value == 'f_f')
                  ) {
                    left += refBounds.max_x - refBounds.min_x - (dimensions.depth);
                  }

                  if (
                    (furnitureItem.orientation == 1 && alignment.h_align.value == 'c_c') ||
                    (furnitureItem.orientation == 3 && alignment.h_align.value == 'c_c')
                  ) {
                    left += (refBounds.max_x - refBounds.min_x - dimensions.width) / 2;
                  }

                  if (
                    (furnitureItem.orientation == 2 && alignment.d_align.value == 'c_c') ||
                    (furnitureItem.orientation == 4 && alignment.d_align.value == 'c_c')
                  ) {
                    left += (refBounds.max_x - refBounds.min_x - dimensions.depth) / 2;
                  }

                  if (
                    (furnitureItem.orientation == 1 && alignment.h_align.value == 'l_l') ||
                    (furnitureItem.orientation == 3 && alignment.h_align.value == 'r_r')
                  ) {
                    // Already the placement is at the start of the cell, so no change.
                  }

                  if (
                    (furnitureItem.orientation == 2 && alignment.d_align.value == 'f_f') ||
                    (furnitureItem.orientation == 4 && alignment.d_align.value == 'b_b')
                  ) {
                    // Already the placement is at the start of the cell, so no change.
                  }

                  if (
                    (furnitureItem.orientation == 1 && alignment.d_align.value == 'f_f') ||
                    (furnitureItem.orientation == 3 && alignment.d_align.value == 'b_b')
                  ) {
                    top += refBounds.max_z - refBounds.min_z - (dimensions.depth);
                  }

                  if (
                    (furnitureItem.orientation == 2 && alignment.h_align.value == 'r_r') ||
                    (furnitureItem.orientation == 4 && alignment.h_align.value == 'l_l')
                  ) {
                    top += refBounds.max_z - refBounds.min_z - (dimensions.width);
                  }

                  if (
                    (furnitureItem.orientation == 1 && alignment.d_align.value == 'c_c') ||
                    (furnitureItem.orientation == 3 && alignment.d_align.value == 'c_c')
                  ) {
                    top += (refBounds.max_z - refBounds.min_z - dimensions.depth) / 2;
                  }

                  if (
                    (furnitureItem.orientation == 2 && alignment.h_align.value == 'c_c') ||
                    (furnitureItem.orientation == 4 && alignment.h_align.value == 'c_c')
                  ) {
                    top += (refBounds.max_z - refBounds.min_z - dimensions.width) / 2;
                  }

                  if (
                    (furnitureItem.orientation == 1 && alignment.d_align.value == 'b_b') ||
                    (furnitureItem.orientation == 3 && alignment.d_align.value == 'f_f')
                  ) {
                    // Already the placement is at the start of the cell, so no change.
                  }

                  if (
                    (furnitureItem.orientation == 2 && alignment.h_align.value == 'l_l') ||
                    (furnitureItem.orientation == 4 && alignment.h_align.value == 'r_r')
                  ) {
                    // Already the placement is at the start of the cell, so no change.
                  }
                }

                objectStyle.left = (100 * left / $scope.room.length) + '%';
                objectStyle.top = (100 * top / $scope.room.breadth) + '%';

                return objectStyle;
            }
        };
    }
]);