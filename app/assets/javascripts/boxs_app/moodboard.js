'use strict';

angular.module('boxsApp')
.controller('MoodboardController', [
    '$scope', '$rootScope', '$http', '$routeParams', '$location', '$window', 'authService',
    function ($scope, $rootScope, $http, $routeParams, $location, $window, authService) {
        $scope.slot = {};

        $scope.init = function(moodboardId) {
            $scope.moodboardId = moodboardId;
            $scope.furnituresBySuperCategory = baseData.furnitures;

            if ($scope.moodboardId) {
                $http.get(
                    '/moodboards/' + $scope.moodboardId + '.json'
                ).success(function(response) {
                    $scope.moodboard = response;
                });
            }
            else {
                $scope.moodboard = {};
            }
        };

        $scope.selectSlot = function(superCategory, rank) {
            $scope.slot = { superCategory: superCategory, rank: rank };
        };

        $scope.deselectSlot = function() {
            $scope.slot = { };
        };

        $scope.isActiveSlot = function(superCategory, rank) {
            return $scope.slot.superCategory == superCategory && $scope.slot.rank == rank;
        };

        $scope.isAnySlotSelected = function() {
            return !_.isEmpty(_.values($scope.slot));
        };

        $scope.assignSlot = function(furnitureId) {
            if ($scope.slot == {}) {
                alert('Select a slot');
            }
            else {
                if (!$scope.moodboard.furniture_ids_by_super_category[$scope.slot.superCategory]) {
                    $scope.moodboard.furniture_ids_by_super_category[$scope.slot.superCategory] = [];
                }

                $scope.moodboard.furniture_ids_by_super_category[$scope.slot.superCategory][$scope.slot.rank] = furnitureId;
                $scope.deselectSlot();
            }
        };

        $scope.save = function() {
            $http.put(
                '/moodboards/' + $scope.moodboardId, {
                    furniture_ids_by_super_category: $scope.moodboard.furniture_ids_by_super_category
                }
            ).success(function(response) {
                alert('Saved');
            })
            .error(function(response) {
                alert(response);
            });
        };
        
        $scope.newDesign = function() {
            $rootScope.modal.open({
                templateUrl: 'newRoom.html',
                controller: 'NewRoomController',
                windowClass: 'new_room_dialog',
                resolve: {
                    roomType: function() { return $scope.moodboard.room_type },
                    moodboardId: function() { return $scope.moodboardId },
                    isSample: function() { return true },
                    successCallback: function() {
                        return function() {
                            $window.location.reload();
                        }
                    }
                }
            });
        };
    }
]);