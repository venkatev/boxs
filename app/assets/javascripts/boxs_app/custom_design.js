'use strict';

angular.module('boxsApp')
.controller('CustomDesignController', [
    '$scope', '$rootScope', '$http', '$location', '$window', 'authService',
    function ($scope, $rootScope, $http, $location, $window, authService) {
        $rootScope.skipNav = true;

        $rootScope.furnitureRecommendations = $rootScope.furnitureRecommendations || {};

        $scope.selectedHotspotIndex = null;
        $scope.selectedFurnitureItemId = null;
        $scope.selectedFurniture = null;

        $scope.loadingRecommendations = {};
        $scope.listingView = $location.search().view;

        $scope.isCustomizationMode = false;

        $scope.loadDesign = function(roomId, customDesignId, renderIndex, hotspotIndex) {
            $scope.roomId = roomId;
            $scope.customDesignId = customDesignId;
            $scope.renderIndex = renderIndex;
            $scope.hotspotIndex = hotspotIndex;

            function prepareDesign() {
                $scope.showRender($scope.renderIndex);
                $scope.deselectHotspot();

                if ($scope.hotspotIndex !== -1) {
                    $scope.selectHotspot(parseInt($scope.hotspotIndex), true);
                }
            }

            // Else, load the design.
            $http.get(
                `/rooms/${$scope.roomId}/custom_designs/${$scope.customDesignId}.json`, {
                    cache: true
                }
            ).success(function(response) {
                $scope.customDesign = response;
                prepareDesign();
            });

            $rootScope.logEvent('load_design', 'design_id', $scope.custonDesignId);
        }

        $scope.customize = function(value) {
            $scope.isCustomizationMode = value;
        };

        /**
        * Returns the position of the currently viewed render within the design.
        */
        $scope.getActiveRenderIndex = function() {
            if (!$scope.customDesign) {
                return null;
            }

            return _.findIndex($scope.customDesign.renders, { active: true });
        };

        $scope.setActiveRenderIndex = function(index) {
            _.each($scope.customDesign.renders, function(render, renderIndex) {
                render.active = renderIndex === index;
            });
        };

        /**
        * Switches to the render at the give +renderIndex+.
        */
        $scope.showRender = function(renderIndex) {
            $scope.deselectHotspot();

            _.each($scope.customDesign.renders, function(render) { render.active = false; });
            $scope.customDesign.renders[parseInt(renderIndex)].active = true;
        };

        $scope.selectHotspot = function(hotspotIndex, force) {
            if (!force && $scope.selectedHotspotIndex === hotspotIndex) {
                return;
            }

            $scope.selectedHotspotIndex = hotspotIndex;
            var activeRenderIndex = $scope.getActiveRenderIndex();

            if (activeRenderIndex === -1) {
                activeRenderIndex = 0;
            }

            const render = $scope.customDesign.renders[activeRenderIndex];
            const hotspot = render.hotspots[hotspotIndex];

            $scope.selectedFurnitureItemId = hotspot.furniture_item_id;
            $scope.selectedFurniture = $scope.customDesign.furniture_mapping[$scope.selectedFurnitureItemId];

            $scope.loadRecommendations(hotspot);
            $rootScope.logEvent('select_hotspot', 'hotspot_index', hotspotIndex);
        };

        $scope.deselectHotspot = function() {
            $scope.selectedHotspotIndex = null;
            $scope.selectedFurnitureItemId = null;
            $scope.selectedFurniture = null;
            $rootScope.logEvent('deselect_hotspot');
        };

        $scope.loadRecommendations = function(hotspot) {
            const furnitureId = $scope.customDesign.furniture_mapping[hotspot.furniture_item_id].id;

            $http.get(
                '/rooms/' + $scope.roomId + '/custom_designs/' + $scope.customDesign.id + '/similar?furniture_item_id=' + $scope.selectedFurnitureItemId + '&furniture_id=' + furnitureId
            ).success(function(response) {
                $scope.furnitureRecommendations[furnitureId] = response;
            });
        };

        $scope.getActiveRender = function() {
            return $scope.customDesign.renders[$scope.getActiveRenderIndex()];
        };

        $scope.getActiveCameraView = function() {
            return $scope.getActiveRender().camera_view;
        };

        $scope.showRecommendation = function(recommendation) {
            const oldHotspotIndex = $scope.selectedHotspotIndex;
            const oldRenderIndex = $scope.getActiveRenderIndex();

            if (recommendation.design) {
                $window.location.href = `/custom_designs/${recommendation.design.id}?render_index=${oldRenderIndex}&hotspot_index=${oldHotspotIndex}`;
                $rootScope.logEvent('show_recommendation', 'design_id', recommendation.design.id);
            }
            else {
                $rootScope.messageProgressBar.show(
                    ['Selecting furnitures', 'Arranging in your room', 'Rendering', 'Preparing your design' ]
                );

                $http.post(
                    `/rooms/${$scope.roomId}/custom_designs/${$scope.customDesign.id}/replace_furniture`,
                    {
                        old_furniture_id: $scope.selectedFurniture.id,
                        new_furniture_id: recommendation.id
                    }
                ).success(function(newDesign) {
                    $window.location.href = `/rooms/${$scope.roomId}/custom_designs/${newDesign.id}?render_index=${oldRenderIndex}&hotspot_index=${oldHotspotIndex}`;
                })
                .error(function() {
                    $rootScope.messageProgressBar.close();
                    $rootScope.modal.info("Sorry, we couldn't apply the changes to the design. Our support team will call you shortly.");
                });
            }
        };

        $scope.prevRender = function() {
            const curIndex = $scope.getActiveRenderIndex();
            const newIndex = ($scope.customDesign.renders.length + curIndex - 1) % $scope.customDesign.renders.length;
            $scope.setActiveRenderIndex(newIndex);
            $rootScope.logEvent('prev_render', 'current_index', curIndex);
        };

        $scope.nextRender = function() {
            const curIndex = $scope.getActiveRenderIndex();
            const newIndex = ($scope.customDesign.renders.length + curIndex + 1) % $scope.customDesign.renders.length;
            $scope.setActiveRenderIndex(newIndex);
            $rootScope.logEvent('next_render', 'current_index', curIndex);
        };
    }
]);