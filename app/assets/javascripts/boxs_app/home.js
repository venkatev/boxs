'use strict';

angular.module('boxsApp')
.controller('HomeController', [
'$scope', '$rootScope', '$http', '$routeParams', '$location', '$window', 'authService',
    function ($scope, $rootScope, $http, $routeParams, $location, $window, authService) {
        $scope.inspirations = []

        $scope.init = function() {
            $scope.loadInspirations()
        }

        $scope.loadInspirations = function() {
            $http.get('/inspirations.json')
                .success(function(response) {
                    $scope.inspirations = response;

                    _.each($scope.inspirations, (inspiration) => {
                        inspiration.categoriesCount = inspiration.categories.length
                    })
                });
        };

        $scope.applyInspiration = function(inspiration) {
            $rootScope.messageProgressBar.show(
                ['Creating your room', 'Selecting furnitures', 'Preparing the design'], 100
            );

            $http.post('/rooms', { inspiration_id: inspiration.id })
                .success(function(roomId) {
                    $window.location.href = `/rooms/${roomId}`;
                })
                .error(function() {
                    $rootScope.messageProgressBar.close();
                    alert('Sorry, somethign went wrong');
                });
        };
    }
]);