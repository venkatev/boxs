'use strict';

angular.module('boxsApp')
.controller('DesignRenderController', [
    '$scope', '$rootScope', function ($scope, $rootScope) {
        $rootScope.skipNav = true
        $scope.designId = null
        $scope.renderStatus = {}
        $scope.CANVAS_SIZE = { x: 1600, y: 1600 }
        $scope.isCustomizationMode = false

        $scope.init = function(designId) {
            $scope.designId = designId
            $scope.furnitureItems = RenderAPI.furnitureItems
            $scope.cameraView = RenderAPI.cameraView
            $scope.scene = new THREE.Scene();
            $scope.camera = new THREE.PerspectiveCamera(90, 1, 1, 100000);

            // We need to see rotations in terms of yaw/theta (w.r.t y-axis), so change the order.
            $scope.camera.rotation.order = 'YXZ'
            $scope.camera.up = new THREE.Vector3(0, 1, 0);
            $scope.renderer = new THREE.WebGLRenderer();
            $scope.renderer.setViewport(0, 0, $scope.CANVAS_SIZE.x, $scope.CANVAS_SIZE.y);
            $scope.setCamera()
            $scope.computeProjections()
        }

        $scope.customize = function(value) {
            $scope.isCustomizationMode = value
        }

        $scope.setCamera = function() {
            $scope.camera.position.set($scope.cameraView.position.x, $scope.cameraView.position.y, $scope.cameraView.position.z);
            $scope.camera.lookAt(new THREE.Vector3($scope.cameraView.target.x, $scope.cameraView.target.y, $scope.cameraView.target.z));
            $scope.renderer.render($scope.scene, $scope.camera);
        }

        $scope.project = function(pointIn3D) {
            const vector = new THREE.Vector3(pointIn3D.x, pointIn3D.y, pointIn3D.z)
            const projection = vector.project($scope.camera)
            return { left: Math.round($scope.CANVAS_SIZE.x / 2 * (projection.x + 1)), top: Math.round($scope.CANVAS_SIZE.y / 2 * (1 - projection.y)), z: projection.z }
        }

        $scope.getAngleInfo = function(point, orientation) {
            var vectorPoint = new THREE.Vector3(point.x, point.y, point.z)
            const relPoint = new THREE.Vector3(point.x - $scope.cameraView.position.x, point.y - $scope.cameraView.position.y, point.z - $scope.cameraView.position.z)

            // Move the camera to origin find the angle with the relative point.
            $scope.camera.position.set(0, 0, 0)
            $scope.camera.lookAt(relPoint)
            var distance = $scope.camera.position.distanceTo(relPoint)
            var theta = ($scope.camera.rotation.y * 180 / Math.PI)

            // Reset camera back.
            $scope.setCamera()
            // theta = (360 + theta) % 360

            switch(orientation) {
                case 1:
                    // No-op
                    break

                case 2:
                    theta = theta + 90
                    break

                case 3:
                    theta = theta + 180
                    break

                case 4:
                    theta = theta + 270
                    break
            }

            theta = (-theta + 360 * 2) % 360

            // Round theta to the nearest 10 degree and distance to 1000.
            theta = Math.round(((360 + theta) % 360) / 10) * 10
            distance = Math.round(distance / 1000) * 1000

            if (distance > 6000) {
                distance = 6000
            }

            return { theta: Math.round(theta), r: Math.round(distance) }
        }

        $scope.computeProjections = function() {
            _.each($scope.furnitureItems, (furnitureItem, itemIdentifier) => {
                furnitureItem.itemIdentifier = itemIdentifier
                console.log(`${furnitureItem.category} - ${furnitureItem.id} - ${furnitureItem.orientation}`)

                const pb = furnitureItem.position_bounds
                const points = [
                    { x: pb.min_x, y: pb.min_y, z: pb.min_z},
                    { x: pb.min_x, y: pb.min_y, z: pb.max_z},
                    { x: pb.min_x, y: pb.max_y, z: pb.min_z},
                    { x: pb.min_x, y: pb.max_y, z: pb.max_z},
                    { x: pb.max_x, y: pb.min_y, z: pb.min_z},
                    { x: pb.max_x, y: pb.min_y, z: pb.max_z},
                    { x: pb.max_x, y: pb.max_y, z: pb.min_z},
                    { x: pb.max_x, y: pb.max_y, z: pb.max_z}
                ]

                furnitureItem.centroid = { x: (pb.min_x + pb.max_x) / 2, y: (pb.min_y + pb.max_y) / 2, z: (pb.min_z + pb.max_z) / 2 }

                $scope.setCamera()
                const angleInfo = $scope.getAngleInfo(furnitureItem.centroid, furnitureItem.orientation)
                furnitureItem.angleInfo = angleInfo
                furnitureItem.projections = _.map(points, (point) => $scope.project(point))
                $scope.camera.lookAt(new THREE.Vector3(furnitureItem.centroid.x, furnitureItem.centroid.y, furnitureItem.centroid.z))
                $scope.renderer.render($scope.scene, $scope.camera)

                furnitureItem.centroidProjections = _.map(points, (point) => $scope.project(point))

                const leftPositions = _.map(furnitureItem.projections, 'left')
                const topPositions = _.map(furnitureItem.projections, 'top')
                const zValues = _.map(furnitureItem.projections, (p) => { return 1 - p.z })
                const minLeft = _.min(leftPositions)
                const maxLeft = _.max(leftPositions)
                const minTop = _.min(topPositions)
                const maxTop = _.max(topPositions)
 
                const tp = _.map(furnitureItem.projections, (p) => `${p.left}_${p.top}`).join('|')
                const cp = _.map(furnitureItem.centroidProjections, (p) => `${p.left}_${p.top}`).join('|')

                const projectedWidth = maxLeft - minLeft
                const projectedHeight = maxTop - minTop

                var visibleWidth, visibleHeight

                if (minLeft <= 0) {
                    if (maxLeft <= 1350) {
                        // Extending outside left boundary and ending within the viewport.
                        visibleWidth = maxLeft
                    }
                    else {
                        // Within the viewport
                        visibleWidth = 1350
                    }
                }
                else if (minLeft > 1350) {
                    // Starting after the viewport on the right.
                    visibleWidth = 0
                }
                else {
                    // Left within the viewport.
                    if (maxLeft <= 1350) {
                        // Right within the viewport.
                        visibleWidth = maxLeft - minLeft
                    }
                    else {
                        // Right outside viewport.
                        visibleWidth = 1350 - minLeft
                    }
                }

                if (minTop <= 0) {
                    if (maxTop <= 1350) {
                        // Extending outside top boundary and ending within the viewport.
                        visibleHeight = maxTop
                    }
                    else {
                        // Within the viewport
                        visibleHeight = 1350
                    }
                }
                else if (minTop > 1350) {
                    // Starting after the viewport on the bottom.
                    visibleHeight = 0
                }
                else {
                    // Top within the viewport.
                    if (maxTop <= 1350) {
                        // And, bottom within the viewport.
                        visibleHeight = maxTop - minTop
                    }
                    else {
                        // But, bottom outside viewport.
                        visibleHeight = 1350 - minTop
                    }
                }

                const totalWidth = maxLeft - minLeft
                const totalHeight = maxTop - minTop

                furnitureItem.visibleWidthPercentage = visibleWidth / totalWidth
                furnitureItem.visibleHeightPercentage = visibleHeight / totalHeight

                console.log(furnitureItem.visibleHeightPercentage)
                console.log(furnitureItem.visibleWidthPercentage)

                furnitureItem.renderPosition = { left: minLeft, top: minTop }
                furnitureItem.renderSize = { width: projectedWidth, height: projectedHeight }
                furnitureItem.maxZ = _.max(zValues)
                furnitureItem.imageURL = `/furnitures/${furnitureItem.id}?t=${angleInfo.theta}&r=${angleInfo.r}&cp=${cp}&tp=${tp}&category=${furnitureItem.category}`
                furnitureItem.isVisible = furnitureItem.visibleWidthPercentage > 0.2 && furnitureItem.visibleHeightPercentage > 0.2
                furnitureItem.catalogURL = `https://boxsapp.com/furnitures/${furnitureItem.id}/catalog_image?theta=${angleInfo.theta}&r=${angleInfo.r}`
                furnitureItem.renderURL = `https://boxsapp.com/furnitures/${furnitureItem.id}/renderer?c=${angleInfo.theta},${angleInfo.r}`
                // console.log(pb)
            })

            $scope.furnitureItems = _.filter(_.values($scope.furnitureItems), (f) => f.isVisible)
            $scope.furnitureItems = _.sortBy($scope.furnitureItems, (item, index) => {
                // Render wall and floor surfaces before anything else.
                if (item.category === 'wall_surface' || item.category === 'floor_surface') {
                    return 0
                }
                else {
                    return item.maxZ
                }
            })

            _.each($scope.furnitureItems, (item, index) => {
                item.zIndex = index
                $scope.renderStatus[index] = false
            })
        }

        $scope.onRoomRender = function() {
            $scope.roomRendered = true
            $scope.updateRenderedStatus()
        }

        $scope.onFurnitureRender = function(index) {
            $scope.renderStatus[index] = true
            $scope.updateRenderedStatus()
        }

        $scope.updateRenderedStatus = function() {
            if (_.all(_.values($scope.renderStatus))) {
                $scope.rendered = true
                $scope.hotspots = {}

                _.each($scope.furnitureItems, (furnitureItem) => {
                    $scope.hotspots[furnitureItem.itemIdentifier] = {
                        x: Math.round(furnitureItem.renderPosition.left + furnitureItem.renderSize.width / 2),
                        y: Math.round(furnitureItem.renderPosition.top + furnitureItem.renderSize.height / 2) - 450
                    }
                })
            }
        }

        $scope.selectHotspot = function(hotspotIndex, force) {
            if (!force && $scope.selectedHotspotIndex === hotspotIndex) {
                return;
            }

            $scope.selectedHotspotIndex = hotspotIndex;
            const hotspot = $scope.hotspots[hotspotIndex]

            $scope.selectedFurnitureItemId = hotspot.furniture_item_id;
            $scope.selectedFurniture = $scope.design.furniture_mapping[$scope.selectedFurnitureItemId];

            $scope.loadRecommendations(hotspot);
            $rootScope.logEvent('select_hotspot', 'hotspot_index', hotspotIndex);
        }
    }
])
.directive('onImageLoad', ['$parse', function ($parse) {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        var fn = $parse(attrs.onImageLoad);
        elem.on('load', function (event) {
          scope.$apply(function() {
            fn(scope, { $event: event });
          });
        });
      }
    };
}]);
