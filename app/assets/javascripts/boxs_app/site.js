// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require angular
//= require angular-route
//= require angular-animate
//= require angular-bootstrap
//= require ngDialog
//= require underscore
//= require angular-underscore
//= require bootstrap-sprockets
//= require angular-bootstrap/ui-bootstrap-tpls
//= require AngularDevise/lib/devise-min
//= require ngSticky
//= require boxs_app/app
//= require boxs_app/auth_service
