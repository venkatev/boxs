'use strict';

angular.module('boxsApp')
.controller('LayoutEngineController', [
    '$scope', '$http', '$window',
    function ($scope, $http, $window) {
      $scope.rows = _.map(_.range(1, 50), (i) => { return i.toString() } );
      $scope.columns = _.map(_.range(65, 65 + 26).concat(_.range(97, 97 + 26)), (i) => { return String.fromCharCode(i)} );

      $scope.isSelectionMode = true;

      $scope.init = function(cellSize) {
        $scope.cellFeetSize = cellSize;
        $scope.cellPixelSize = 20 * $scope.cellFeetSize;

        $scope.roomType = baseData.roomType;
        $scope.isGlobal = baseData.isGlobal;
        $scope.objectCategory = baseData.objectCategory;
        $scope.roomLength = baseData.roomLength;
        $scope.roomBreadth = baseData.roomBreadth;
        $scope.objectVariantIdentifier = baseData.objectVariantIdentifier;
        $scope.objectWidth = baseData.objectWidth;
        $scope.objectDepth = baseData.objectDepth;
        $scope.placements = _.map(baseData.placements, $scope.parsePlacement);
        $scope.refCategory = baseData.refCategory;
        $scope.refVariantIdentifier = baseData.refVariantIdentifier;
        $scope.refWidth = baseData.refWidth;
        $scope.refDepth = baseData.refDepth;
        $scope.refPlacement = baseData.refPlacement;
        $scope.objectOrientation = 1;
        $scope.objectAlignment = 'hcenter_hcenter';
        $scope.allParams = baseData.allParams;
        $scope.numChanges = 0;
        $scope.curPlacement = {};

        $scope.randomColors = [];

        function getRandomColor() {
          var letters = '79BDF';
          var color = '#';
          for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 5)];
          }

          return color;
        }

        _.times(30, (i) => {
          $scope.randomColors.push(getRandomColor());
        });

        if ($scope.refPlacement) {
          $scope.refOrientation = $scope.getOrientation($scope.refPlacement);

          $scope.refStyle = {
            left: (100 * $scope.getStartColumn($scope.refPlacement) / $scope.getNumCols()) + '%',
            top: (100 * $scope.getStartRow($scope.refPlacement) / $scope.getNumRows()) + '%'
          };

          if ($scope.isHorizontal($scope.refOrientation)) {
            $scope.refStyle.width = ($scope.refWidth * 100 / $scope.roomLength) + '%';
            $scope.refStyle.height = ($scope.refDepth * 100 / $scope.roomBreadth) + '%';
          }
          else {
            $scope.refStyle.height = ($scope.refWidth * 100 / $scope.roomLength) + '%';
            $scope.refStyle.width = ($scope.refDepth * 100 / $scope.roomBreadth) + '%';
          }
        }
      };

      $scope.copyPlacements = function(refPlacement) {
        $scope.copiedRefPlacement = refPlacement;

        var params = _.clone($scope.allParams);
        _.extend(params, { ref_placement: $scope.copiedRefPlacement });

        $http.post('/layout_engine/get_placements', params)
          .success(function(placements) {
            $scope.copiedPlacements = _.map(placements, $scope.parsePlacement);
          });        
      };

      $scope.pastePlacements = function() {
        if ($scope.copiedPlacements) {
          $scope.placements = _.union($scope.placements, $scope.copiedPlacements);
          $scope.savePlacements();
          $scope.discardCopiedPlacements();
        }
      };

      $scope.discardCopiedPlacements = function() {
          $scope.copiedRefPlacement = null;
          $scope.copiedPlacements = [];
      };

      $scope.getOrientation = function(placement) {
        const match = placement.match(/^\d+(?:[A-Z]|[a-z])([1-4])/);
        return parseInt(match[1]);
      };

      $scope.isHorizontal = function(orientation) {
        return orientation == 1 || orientation == 3;
      };

      $scope.parsePlacement = function(placement) {
        const matches = placement.match(/^(\d+(?:[A-Z]|[a-z])[1-4](?:-\d+(?:[A-Z]|[a-z])[1-4])?)@(.+)?@(.+)/);
        return { cellRange: matches[1], hAlign: matches[2], dAlign: matches[3] };
      };

      $scope.selectPlacement = function(index) {
        $scope.selectedPlacementIndex = index;        
      };

      $scope.removePlacement = function(index) {
        if (confirm("Sure?")) {
          $scope.placements.splice(index, 1);
          $scope.savePlacements();
        }
      };

      $scope.markPlacement = function() {
        $scope.placements.push($scope.curPlacement);
        $scope.savePlacements();
      };

      $scope.clearPlacement = function() {
          $scope.curPlacement = {};
      };

      $scope.clearAllPlacements = function() {
        $scope.clearPlacement();
        $scope.placements = [];
        $scope.savePlacements();
      };

      $scope.savePlacements = function(skipAlignments) {
        const placements =  _.map($scope.placements, (p) => {
          return `${p.cellRange}@${p.hAlign}@${p.dAlign}`; 
        });

        const params = _.extend($scope.allParams, { placements: placements });

        $http.post('/layout_engine/save', params)
          .success(function(response) {
            $scope.clearPlacement();
            $scope.numChanges = 0;
          });
      };

      $scope.getStartRow = function(placement) {
        const matches = placement.match(/^\d+/);
        return _.indexOf($scope.rows, matches[0]);
      };

      $scope.getStartColumn = function(placement) {
        const matches = placement.match(/^\d+([A-Z]|[a-z])/);
        return _.indexOf($scope.columns, matches[1]);
      };

      $scope.getNumCols = function() {
        return Math.round($scope.roomLength / $scope.cellFeetSize);
      };

      $scope.getNumRows = function() {
        return Math.round($scope.roomBreadth / $scope.cellFeetSize);
      };

      $scope.getObjectRows = function(orientation) {
        return Math.round(($scope.isHorizontal(orientation) ? $scope.objectDepth : $scope.objectWidth) / $scope.cellFeetSize);
      };

      $scope.getObjectCols = function(orientation) {
        return Math.round(($scope.isHorizontal(orientation) ? $scope.objectWidth : $scope.objectDepth) / $scope.cellFeetSize);
      };

      $scope.getRefRows = function(orientation) {
        return Math.round(($scope.isHorizontal(orientation) ? $scope.refDepth : $scope.refWidth) / $scope.cellFeetSize);
      };

      $scope.getRefCols = function(orientation) {
        return Math.round(($scope.isHorizontal(orientation) ? $scope.refWidth : $scope.refDepth) / $scope.cellFeetSize);
      };

      $scope.getMarkerStyle = function(placement, index) {
        const orientation = $scope.getOrientation(placement.cellRange);
        const color = $scope.randomColors[index];
        const styles = {
          background: color,
          width: (100 * $scope.getObjectCols(orientation) / $scope.getNumCols()) + '%',
          height: (100 * $scope.getObjectRows(orientation) / $scope.getNumRows()) + '%',
          left: $scope.getStartColumn(placement.cellRange) * $scope.cellPixelSize,
          top: $scope.getStartRow(placement.cellRange) * $scope.cellPixelSize
        };

        const borderProperty = { 1: 'border-bottom', 2: 'border-left', 3: 'border-top', 4: 'border-right' }[orientation];
        styles[borderProperty] = '5px solid #AA0000';

        return styles;
      };

      $scope.getLayerStyle = function(index) {
        const color = $scope.randomColors[index];
        return { background: color };
      };

      $scope.setOrientation = function(o) {
        $scope.objectOrientation = o;
      };

      $scope.placeObject = function(row, column) {
        if (!$scope.isSelectionMode) {
          return;
        }

        const startRowIndex = _.indexOf($scope.rows, row);
        const startColIndex = _.indexOf($scope.columns, column);

        const endRowIndex = startRowIndex + ($scope.getObjectRows($scope.objectOrientation) - 1);
        const endColIndex = startColIndex + ($scope.getObjectCols($scope.objectOrientation) - 1);

        if (endRowIndex >= $scope.getNumRows() || endColIndex >= $scope.getNumCols()) {
          $scope.error = "Can't place in that position";
          return;
        }

        $scope.error = null;

        if (startRowIndex == endRowIndex && startColIndex == endColIndex) {
          // Single cell.
          $scope.curPlacement.cellRange =  `${$scope.rows[startRowIndex]}${$scope.columns[startColIndex]}${$scope.objectOrientation}`;
        }
        else {
          // Multiple column, multiple rows, like 1A-3C.
          $scope.curPlacement.cellRange = `${$scope.rows[startRowIndex]}${$scope.columns[startColIndex]}${$scope.objectOrientation}-${$scope.rows[endRowIndex]}${$scope.columns[endColIndex]}${$scope.objectOrientation}`;
        }

        const alignments = $scope.getImplicitAlignments();
        $scope.curPlacement.hAlign = alignments.hAlign;
        $scope.curPlacement.dAlign = alignments.dAlign;

        $scope.setObjectStyle();
      }

      $scope.getImplicitAlignments = function() {
        var startRowIndex = $scope.getStartRow($scope.curPlacement.cellRange);
        var startColIndex = $scope.getStartColumn($scope.curPlacement.cellRange);
        var endRowIndex = startRowIndex + ($scope.getObjectRows($scope.objectOrientation) - 1);
        var endColIndex = startColIndex + ($scope.getObjectCols($scope.objectOrientation) - 1);

        const cellBounds = {
          min_x: startColIndex * $scope.cellFeetSize,
          max_x: Math.min((endColIndex + 1) * $scope.cellFeetSize, $scope.roomLength),
          min_z: startRowIndex * $scope.cellFeetSize,
          max_z: Math.min((endRowIndex + 1) * $scope.cellFeetSize, $scope.roomBreadth),
        };

        startRowIndex = $scope.getStartRow($scope.refPlacement);
        startColIndex = $scope.getStartColumn($scope.refPlacement);
        endRowIndex = startRowIndex + ($scope.getRefRows($scope.refOrientation) - 1);
        endColIndex = startColIndex + ($scope.getRefCols($scope.refOrientation) - 1);

        const refBounds = {
          min_x: startColIndex * $scope.cellFeetSize,
          max_x: Math.min((endColIndex + 1) * $scope.cellFeetSize, $scope.roomLength),
          min_z: startRowIndex * $scope.cellFeetSize,
          max_z: Math.min((endRowIndex + 1) * $scope.cellFeetSize, $scope.roomBreadth),
        };

        var hAlign, dAlign;

        if (cellBounds.min_x === 0) {
          if      ($scope.objectOrientation === 1) hAlign = 'c_l_l';
          else if ($scope.objectOrientation === 2) dAlign = 'c_f_f';
          else if ($scope.objectOrientation === 3) hAlign = 'c_r_r';
          else if ($scope.objectOrientation === 4) dAlign = 'c_b_b';
        }

        if (cellBounds.max_x === $scope.roomLength) {
          if      ($scope.objectOrientation === 1) hAlign = 'c_r_r';
          else if ($scope.objectOrientation === 2) dAlign = 'c_b_b';
          else if ($scope.objectOrientation === 3) hAlign = 'c_l_l';
          else if ($scope.objectOrientation === 4) dAlign = 'c_f_f';
        }

        if (cellBounds.min_y === 0) {
          if      ($scope.objectOrientation === 1) dAlign = 'c_b_b';
          else if ($scope.objectOrientation === 2) hAlign = 'c_l_l';
          else if ($scope.objectOrientation === 3) dAlign = 'c_f_f';
          else if ($scope.objectOrientation === 4) hAlign = 'c_r_r';
        }

        if (cellBounds.max_y === $scope.roomBreadth) {
          if      ($scope.objectOrientation === 1) dAlign = 'c_f_f';
          else if ($scope.objectOrientation === 2) hAlign = 'c_r_r';
          else if ($scope.objectOrientation === 3) dAlign = 'c_b_b';
          else if ($scope.objectOrientation === 4) hAlign = 'c_l_l';
        }

        hAlign = hAlign || 'c_c_c';
        dAlign = dAlign || 'c_c_c';

        return { hAlign: hAlign, dAlign: dAlign };
      };

      $scope.setObjectStyle = function() {
        $scope.objectStyle = {};

        if ($scope.isHorizontal($scope.objectOrientation)) {
          $scope.objectStyle.width = ($scope.objectWidth * 100 / $scope.roomLength) + '%';
          $scope.objectStyle.height = ($scope.objectDepth * 100 / $scope.roomBreadth) + '%';
        }
        else {
          $scope.objectStyle.height = ($scope.objectWidth * 100 / $scope.roomBreadth) + '%';
          $scope.objectStyle.width = ($scope.objectDepth * 100 / $scope.roomLength) + '%';
        }

        var hRefBounds, dRefBounds;

        var left = $scope.getStartColumn($scope.curPlacement.cellRange) * $scope.cellFeetSize;
        var top = $scope.getStartRow($scope.curPlacement.cellRange) * $scope.cellFeetSize;

        var startRowIndex = $scope.getStartRow($scope.curPlacement.cellRange);
        var startColIndex = $scope.getStartColumn($scope.curPlacement.cellRange);
        var endRowIndex = startRowIndex + ($scope.getObjectRows($scope.objectOrientation) - 1);
        var endColIndex = startColIndex + ($scope.getObjectCols($scope.objectOrientation) - 1);

        const cellBounds = {
          min_x: startColIndex * $scope.cellFeetSize,
          max_x: Math.min((endColIndex + 1) * $scope.cellFeetSize, $scope.roomLength),
          min_z: startRowIndex * $scope.cellFeetSize,
          max_z: Math.min((endRowIndex + 1) * $scope.cellFeetSize, $scope.roomBreadth),
        };

        startRowIndex = $scope.getStartRow($scope.refPlacement);
        startColIndex = $scope.getStartColumn($scope.refPlacement);
        endRowIndex = startRowIndex + ($scope.getRefRows($scope.refOrientation) - 1);
        endColIndex = startColIndex + ($scope.getRefCols($scope.refOrientation) - 1);

        const refBounds = {
          min_x: startColIndex * $scope.cellFeetSize,
          max_x: Math.min((endColIndex + 1) * $scope.cellFeetSize, $scope.roomLength),
          min_z: startRowIndex * $scope.cellFeetSize,
          max_z: Math.min((endRowIndex + 1) * $scope.cellFeetSize, $scope.roomBreadth),
        };

        switch ($scope.curPlacement.hAlignRef) {
          case 'c':
            hRefBounds = cellBounds;
            break;

          case 'p':
            hRefBounds = refBounds;
            break;
        }

        switch ($scope.curPlacement.dAlignRef) {
          case 'c':
            dRefBounds = cellBounds;
            break;

          case 'p':
            dRefBounds = refBounds;
            break;
        }

        var borderSides = [];

        if (hRefBounds) {
          if (
            ($scope.objectOrientation == 1 && $scope.curPlacement.hAlignVal == 'r_r') ||
            ($scope.objectOrientation == 3 && $scope.curPlacement.hAlignVal == 'l_l')
          ) {
            left = hRefBounds.max_x - $scope.objectWidth;
            borderSides.push('borderRight');
          }

          if (
            ($scope.objectOrientation == 1 && $scope.curPlacement.hAlignVal == 'c_c') ||
            ($scope.objectOrientation == 3 && $scope.curPlacement.hAlignVal == 'c_c')
          ) {
            left = hRefBounds.min_x + (hRefBounds.max_x - hRefBounds.min_x) / 2 - ($scope.objectWidth / 2);
            borderSides.push('borderLeft');
            borderSides.push('borderRight');
          }

          if (
            ($scope.objectOrientation == 1 && $scope.curPlacement.hAlignVal == 'l_l') ||
            ($scope.objectOrientation == 3 && $scope.curPlacement.hAlignVal == 'r_r')
          ) {
            left = hRefBounds.min_x;
            borderSides.push('borderLeft');
          }

          if (
            ($scope.objectOrientation == 2 && $scope.curPlacement.hAlignVal == 'r_r') ||
            ($scope.objectOrientation == 4 && $scope.curPlacement.hAlignVal == 'l_l')
          ) {
            top = hRefBounds.max_z - $scope.objectWidth;
            borderSides.push('borderBottom');
          }

          if (
            ($scope.objectOrientation == 2 && $scope.curPlacement.hAlignVal == 'c_c') ||
            ($scope.objectOrientation == 4 && $scope.curPlacement.hAlignVal == 'c_c')
          ) {
            top = hRefBounds.min_z + (hRefBounds.max_z - hRefBounds.min_z) / 2 - ($scope.objectWidth / 2);
            borderSides.push('borderTop');
            borderSides.push('borderBottom');
          }

          if (
            ($scope.objectOrientation == 2 && $scope.curPlacement.hAlignVal == 'l_l') ||
            ($scope.objectOrientation == 4 && $scope.curPlacement.hAlignVal == 'r_r')
          ) {
            top = hRefBounds.min_z;
            borderSides.push('borderTop');
          }          
        }

        if (dRefBounds) {
          if (
            ($scope.objectOrientation == 2 && $scope.curPlacement.dAlignVal == 'b_b') ||
            ($scope.objectOrientation == 4 && $scope.curPlacement.dAlignVal == 'f_f')
          ) {
            left = dRefBounds.max_x - $scope.objectDepth;
            borderSides.push('borderRight');
          }

          if (
            ($scope.objectOrientation == 2 && $scope.curPlacement.dAlignVal == 'c_c') ||
            ($scope.objectOrientation == 4 && $scope.curPlacement.dAlignVal == 'c_c')
          ) {
            left = dRefBounds.min_x + (dRefBounds.max_x - dRefBounds.min_x) / 2 - ($scope.objectDepth / 2);
            borderSides.push('borderLeft');
            borderSides.push('borderRight');
          }

          if (
            ($scope.objectOrientation == 2 && $scope.curPlacement.dAlignVal == 'f_f') ||
            ($scope.objectOrientation == 4 && $scope.curPlacement.dAlignVal == 'b_b')
          ) {
            left = dRefBounds.min_x;
            borderSides.push('borderLeft');
          }

          if (
            ($scope.objectOrientation == 1 && $scope.curPlacement.dAlignVal == 'f_f') ||
            ($scope.objectOrientation == 3 && $scope.curPlacement.dAlignVal == 'b_b')
          ) {
            top = dRefBounds.max_z - $scope.objectDepth;
            borderSides.push('borderBottom');
          }

          if (
            ($scope.objectOrientation == 1 && $scope.curPlacement.dAlignVal == 'c_c') ||
            ($scope.objectOrientation == 3 && $scope.curPlacement.dAlignVal == 'c_c')
          ) {
            top = dRefBounds.min_z + (dRefBounds.max_z - dRefBounds.min_z) / 2 - ($scope.objectDepth / 2);
            borderSides.push('borderTop');
            borderSides.push('borderBottom');
          }

          if (
            ($scope.objectOrientation == 1 && $scope.curPlacement.dAlignVal == 'b_b') ||
            ($scope.objectOrientation == 3 && $scope.curPlacement.dAlignVal == 'f_f')
          ) {
            top = dRefBounds.min_z;
            borderSides.push('borderTop');
          }
        }

        $scope.objectStyle.left = (100 * left / $scope.roomLength) + '%';
        $scope.objectStyle.top = (100 * top / $scope.roomBreadth) + '%';

        _.each(borderSides, (borderSide) => {
          $scope.objectStyle[borderSide] = "5px solid blue";
        });
      };
    }
]);