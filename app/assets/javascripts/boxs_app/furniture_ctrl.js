'use strict';
angular.module('boxsApp')
    .controller('FurnitureCtrl', [
        '$scope', '$rootScope', '$http', '$window', '$timeout',
        function ($scope, $rootScope, $http, $window, $timeout) {
          $scope.hasTextures = false

          $scope.init = function (furnitureId, modelUrl) {
            $scope.renderObject = {};
            $scope.furnitureId = furnitureId;
            $scope.modelUrl = modelUrl;
            $scope.cameraPositions = baseData.cameraPositions;
            $scope.yPosition = baseData.yPosition;
            $scope.requiredCaptures = baseData.cameraPositions.length;
            $scope.setRenderer();
            $scope.loadFurniture();
            $scope.setConsoleAccess();
            $scope.lights = [];
            $scope.lightHelpers = {}
            $scope.singleCameraPosition = {}
            $scope.isReadyFoRendering = false
            $scope.isValidated = false
            $scope.shouldRender = false
          };

          $scope.animate = function () {
            requestAnimationFrame( $scope.animate );
            $scope.renderer.render( $scope.scene, $scope.camera );
          };

          $scope.setConsoleAccess = function () {
            window.camera = $scope.camera;
            window.scene = $scope.scene;
            window.renderer = $scope.renderer;
            window.scope = $scope;
          };

          $scope.setRenderer = function () {
            $scope.container = $('#renderer')
            $scope.scene = new THREE.Scene();
            $scope.camera = new THREE.PerspectiveCamera(90, 1, 1, 100000 );
            $scope.camera.position.z = 5;
            $scope.renderer = new THREE.WebGLRenderer({
                antialias: true,
                preserveDrawingBuffer: true,
                precision: 'mediump',
                logarithmicDepthBuffer: true,
                alpha: true
            });
            $scope.renderer.shadowMap.enabled = true;
            $scope.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
            $scope.renderer.setSize($scope.container.width(), $scope.container.width());
            $scope.renderer.setClearColor(0xFFFFFF, 0);
            $scope.renderer.setPixelRatio(1);

            $scope.renderer.setViewport(0, 0, $scope.container.width(), $scope.container.width());
            $scope.container[0].appendChild($scope.renderer.domElement);

            const geometry = new THREE.BoxBufferGeometry(10000, 10000, 10000)
            const material = new THREE.MeshLambertMaterial({ transparent: true, opacity: 0 })
            $scope.room = new THREE.Mesh(geometry, material)
            $scope.scene.add($scope.room)
            $scope.scene.add($scope.camera)

            $scope.orbitControls = new THREE.OrbitControls( $scope.camera, $scope.renderer.domElement )
            $scope.orbitControls.update()

            $scope.axesHelper = new THREE.AxesHelper( 5000 );
            $scope.room.add( $scope.axesHelper );
          };

          $scope.loadFurniture = function() {
            $http.get('/furnitures/' + $scope.furnitureId + '.json')
              .success((furniture) => {
                $scope.furniture = furniture
                $scope.loadModelAndRender();
              })
              .error(function(e) {
                alert("Furniture load failed");
              })
          }

          $scope.loadModelAndRender = function () {
            $http.get($scope.modelUrl)
              .success(function (furnitureModel) {
                // Wait for 500ms to see if there are any textures to be loaded. If not, start the render.
                $timeout(() => {
                    if (!$scope.hasTextures) {
                      $scope.updateDimensionsAndRender(furnitureModel);
                    }
                }, 500)

                THREE.DefaultLoadingManager.onStart = function ( url, itemsLoaded, itemsTotal ) {
                    $scope.hasTextures = true
                };

                THREE.DefaultLoadingManager.onLoad = function ( ) {
                    $scope.updateDimensionsAndRender(furnitureModel);
                };

                const loader = new THREE.ObjectLoader();
                loader.setCrossOrigin('anonymous');
                $scope.renderObject = loader.parse(furnitureModel);
              })
              .error(function () {
                $scope.errorIndicator.show('Could not load the object');
              });
          };

          $scope.saveLights = function() {
            $scope.updateLights()

            $http.put('/furnitures/' + $scope.furnitureId + '/update_lights.json', { lights: $scope.furniture.lights })
              .success(function (furnitureModel) {
                $rootScope.modal.info('Light settings saved')
              })
              .error(function(e) {
                  alert(e.error_message);
              });
          }

          $scope.updateLights = function() {
            _.each($scope.furniture.lights, (lightSetting, index) => {
              let light = $scope.lights[index]

              if (!light) {
                if (lightSetting.type === 'ambient') {
                  light = new THREE.AmbientLight(0xFFFFFF)
                }
                else if (lightSetting.type === 'spot') {
                  light = new THREE.SpotLight(0xFFFFFF)
                  light.decay = 2
                  light.target = $scope.room

                  const colors = [0xFF0000, 0x00FF00, 0x0000FF]
                  const lightHelper = new THREE.SpotLightHelper(light, colors[_.keys($scope.lightHelpers).length]);
                  $scope.room.add(lightHelper);
                  $scope.lightHelpers[index] = lightHelper
                }

                $scope.lights.push(light)
                $scope.room.add(light)
              }

              light.intensity = parseFloat(lightSetting.intensity)

              if (lightSetting.angle) {
                light.angle = parseFloat(lightSetting.angle)
              }

              if (lightSetting.penumbra) {
                light.penumbra = parseFloat(lightSetting.penumbra)
              }

              if (lightSetting.position) {
                light.position.set(
                  parseInt(lightSetting.position.x),
                  parseInt(lightSetting.position.y),
                  parseInt(lightSetting.position.z)
                )
              }

              if ($scope.lightHelpers[index]) {
                $scope.lightHelpers[index].update()
              }
            })
          }

          $scope.updateSingleCamera = function() {
            $scope.updateCameraPosition({ theta: $scope.singleCameraPosition.theta, r: $scope.singleCameraPosition.r })
          }

          $scope.startRendering = function() {
            $scope.shouldRender = true
            $scope.axesHelper.visible = false
            _.each($scope.lightHelpers, (helper) => helper.visible = false)

            $timeout(() => {
              $scope.generateRenders()
            }, 500)
          }

          $scope.stopRendering = function() {
            $scope.shouldRender = false
            $scope.axesHelper.visible = true
            _.each($scope.lightHelpers, (helper) => helper.visible = true)
          }

          $scope.removeAllViews = function() {
            if (confirm("Remove all views?")) {
              $http.put('/furnitures/' + $scope.furnitureId + '/remove_views.json')
                .success((pendingCameraPositions) => {
                  $scope.cameraPositions = pendingCameraPositions
                  $scope.requiredCaptures = $scope.cameraPositions.length
                })
            }
          }

          $scope.updateDimensionsAndRender = function(furnitureModel) {
            const baseObject = $scope.renderObject.children[0];
            baseObject.scale.set(25.4, 25.4, 25.4);
            $scope.dimensions = baseObject.getDimensions();
            $scope.room.add( $scope.renderObject );
            $scope.updateLights()
            $scope.animate();
            $scope.renderObject.position.set(0, 0, 0);
            $scope.renderObject.orientation = 1;
            $scope.camera.fov = 90;
            $scope.camera.updateProjectionMatrix();
            $scope.isReadyFoRendering = true
            $scope.updateCameraPosition({ theta: 30, r: 3000 })

            $http.put('/furnitures/' + $scope.furnitureId + '/update_dimensions.json', {
              furniture: {
                dimensions: {
                    height: $scope.dimensions.y,
                    width: $scope.dimensions.x,
                    depth: $scope.dimensions.z
                }
              }
            })
            .success(function (furnitureModel) {
              $scope.isValidated = true
            })
            .error(function(e) {
                alert(e.error_message);
            });
          };

           $scope.generateRenders = function () {
            if (!$scope.cameraPositions.length) {
              return true;
            }

            $scope.updateCameraPosition($scope.cameraPositions[0]);
            var imageData = $scope.renderer.domElement.toDataURL();

            $http.post('/furnitures/' + $scope.furnitureId + '/furniture_views', {
              image: imageData,
              theta: $scope.cameraPositions[0].theta,
              r: $scope.cameraPositions[0].r
            })
            .success(function () {
              --$scope.requiredCaptures;

              if ($scope.shouldRender) {
                $scope.cameraPositions.splice(0, 1)
                $scope.generateRenders();
              }
            })
            .error(function () {
              alert("Error while adding catalog image");
              console.log("Error while adding catalog image");
              return false;
            });
          };

          $scope.updateCameraPosition = function(cameraPosition) {
            // View 2ft from the floor.
            // TODO Make this customizable based on the category.
            const x = 0
            // fixedY: The height from which we view the object relative to the object's position.
            // For example: If the object is on the floor, it's 5 feet. If the object is at a height
            // of 4 ft (painting ), its 1 foot.
            const fixedY = ( 5 - $scope.yPosition ) * 305
            // Place the object in such a way that the center of the object aligns with the origin.
            const y = fixedY - $scope.dimensions.y / 2
            const z = Math.sqrt(cameraPosition.r * cameraPosition.r - y * y)
            $scope.room.rotation.y = (cameraPosition.theta * Math.PI / 180)

            if (cameraPosition.r <= 2500 && (
                  $scope.dimensions.x >= 2100 ||
                  $scope.dimensions.y >= 2100 ||
                  $scope.dimensions.z >= 2100)) {
              $scope.camera.zoom = 0.8;
            } else {
              $scope.camera.zoom = 1.4;
            }

            $scope.camera.zoom = 0.8;
            $scope.camera.position.set(
                x,
                y,
                z
            );

            $scope.camera.updateProjectionMatrix();
            $scope.animate()
          };
        }
     ]);

THREE.Object3D.prototype.getDimensions = function () {
    return new THREE.Box3().setFromObject(this).getSize(new THREE.Vector3());
};

const RenderAPI = {
  getScope: function() {
      return angular.element('#renderer').scope();
  },

  isRenderCompleted: function() {
    console.log(`requiredCaptures = ${RenderAPI.getScope().requiredCaptures}`)
    return !RenderAPI.getScope().requiredCaptures
  },

  getPendingCaptures: function() {
    return RenderAPI.getScope().requiredCaptures;
  }
};


// s = angular.element('#renderer').scope()
// s.updateCameraPosition({theta: 350, r: 6000})
// s.cameraPositions = [{theta: 350, r: 6000}];
// s.generateRenders()