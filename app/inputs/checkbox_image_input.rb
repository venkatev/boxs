class CheckboxImageInput < Formtastic::Inputs::CheckBoxesInput
  def choice_html(choice)
    template.content_tag(
    :label,
    img_tag(choice) + checkbox_input(choice) + choice_label(choice),
    # img_tag(choice) + checkbox_input(choice) + choice_label(Inspiration.find(choice[1]).reference),
    label_html_options.merge(:for => choice_input_dom_id(choice), :class =>
    "input_with_thumbnail"))
 end

  def img_tag(choice)
    template.image_tag(Inspiration.find(choice[1]).image.url, :height=>150)
  end
end

# class CheckboxImageInput < Formtastic::Inputs::CheckBoxesInput
#   include CloudinaryHelper

#   def choice_html(choice)
#     template.content_tag(
#     :label,
#     img_tag(choice) + checkbox_input(choice) + choice_label(choice),
#     label_html_options.merge(:for => choice_input_dom_id(choice), :class =>
#     "input_with_thumbnail"))
#  end

#   def img_tag(choice)
#     cl_image_tag(Product.find(choice[1]).photos[0].path, :width=>30,
#     :crop=>"scale")
#   end
# end