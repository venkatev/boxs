module ColorEngine
  MATCH_THRESHOLD = 0.05

  ################################################
  # Definitions

  ALL_COLORS = ["#00000D", "#363636", "#6A757B", "#B2B7BB", "#FFFFFF", "#E73D96", "#F8B9D4", "#B2005C", "#D13B5A", "#B32018", "#EF2C25", "#771D7D", "#9A5AA4", "#B76B6D", "#1A3F97", "#4963AE", "#8CA4D4", "#009CC8", "#00A2B1", "#006A5D", "#017286", "#008AAE", "#487E42", "#65B560", "#D1E37D", "#FFE192", "#FED14D", "#F2881E", "#FAA634", "#CF6519", "#F89D88", "#FDD1C4", "#E7C39F", "#9D5F0D", "#BD844A", "#572500"]

  ################################################

  def self.get_nearest(color)
    ALL_COLORS.sort_by {|c| self.color_distance(c, color) }.first
  end

  def self.color_distance(c1, c2)
    c1_rgb = c1.dup.paint.rgb
    c2_rgb = c2.dup.paint.rgb
    # c1_hsv = c1.paint.hsv
    # c2_hsv = c2.paint.hsv

    c1_rgb_n = [c1_rgb.r, c1_rgb.g, c1_rgb.b].map {|v| v / 255.0 }
    c2_rgb_n = [c2_rgb.r, c2_rgb.g, c2_rgb.b].map {|v| v / 255.0 }

    rgb_distance = Math.sqrt(
      (c1_rgb_n[0] - c2_rgb_n[0]) ** 2 +
      (c1_rgb_n[1] - c2_rgb_n[1]) ** 2 +
      (c1_rgb_n[2] - c2_rgb_n[2]) ** 2
    ) / 3.0

    # hsv_distance = Math.sqrt(
    #   ((c1_hsv.h - c2_hsv.h) / 360.0) ** 2 +
    #   (c1_hsv.s - c2_hsv.s) ** 2 +
    #   (c1_hsv.v - c2_hsv.v) ** 2
    # ) / 3.0

    # h_distance = (c1_hsv.h - c2_hsv.h).abs / 360.0
    # s_distance = (c1_hsv.s - c2_hsv.s).abs
    # v_distance = (c1_hsv.v - c2_hsv.v).abs

    # (rgb_distance * (h_distance * s_distance))
    return rgb_distance
  end

  def self.close_colors?(c1, c2)
    self.color_distance(c1, c2) < ColorEngine::MATCH_THRESHOLD
  end
end
