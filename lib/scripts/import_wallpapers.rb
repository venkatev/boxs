require 'fileutils'
require 'byebug'

CATALOG_SRC = "/Users/vikramboxs/Downloads/wallpaper_set"

def import_batch(variant_id)
  textures = Dir["#{CATALOG_SRC}/all/*.jpg"]
  # textures.shuffle
  # textures = textures[0..99]
  imported_models = Dir["#{CATALOG_SRC}/imported_models/#{variant_id}/*"]

  target = "#{CATALOG_SRC}/imported_models/#{variant_id}"
  src = File.dirname(__FILE__) + "/../../data/model_templates/wallpaper_#{variant_id}"
  src = File.expand_path(src)

  textures.each do |texture|
    full_name = File.basename(texture, '.jpg')
    full_name =~ /^([^_]+_[^_]+_[^_]+)/
    short_name = $1
    short_name = short_name.gsub(/\s+/, '-').downcase

    if !File.exists?("#{target}/#{short_name}_#{variant_id}")
      puts(target + "/#{short_name}_#{variant_id}")
      FileUtils.copy_entry src, (target + "/#{short_name}_#{variant_id}")
      FileUtils.copy_entry texture, target + "/#{short_name}_#{variant_id}/wallpaper/wallpaper.jpg"
    end
  end
end

import_batch('2001')