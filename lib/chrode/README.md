# Node backend
The node backend process is used for loading the planner in a chromium process in headless mode and communicating. In order to run the backend process, modify the url in the model service that points to the planner that you would like to connect to.

## Setup
* Install npm or yarn (Yarn is preferred)
* Inside the root folder, run `yarn install`
* Run `yarn start` to start the backend
* By default the backend runs in port 3001.
* Execute `curl --request GET 'localhost:3001/projects/292'` to get the raw byte buffer representing the image

# Frontend
The react frontend that is used to display the screenshot in the frontend.
## Setup
* cd `src/client`
* Run `yarn install`
* Run `PORT=3002 yarn start`
* The above command starts the frontend in the port 3002.
* Navigate to `http://localhost:3002`