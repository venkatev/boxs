const _ = require('underscore');
const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const CatalogChromiumService = require('../services/catalog_chromium_service');
const chromiumSessions = {};
router.use(bodyParser.urlencoded({ extended: true }));

router.get('/:id/renderer', async (request, response) => {
  console.log(`${new Date().toISOString()}`, `/${request.params.id}/renderer started`);
  var furnitureId = request.params.id;

  var catalogChromiumService = CatalogChromiumService.getInstance();
  var renderPage = catalogChromiumService.getRenderPage(furnitureId);

  try {
    if (!renderPage) {
      renderPage = await catalogChromiumService.createRenderPage(furnitureId);
      console.log(`[Furniture #${furnitureId}] Init`)
      await renderPage.loadRenderer(furnitureId);

      renderPage.closeAfterRenderCompleted();
    }

    const data = await getStatusData()
    console.log(`${new Date().toISOString()}`, `/${request.params.id}/renderer done`);
    return response.status(200).send(data)
  }
  catch(e) {
    console.log(`${new Date().toISOString()}`, 'Failed');
    console.log(e);
    console.log(e.stack);
    await renderPage.release();
    return response.status(500).send("Error");
  }
});

router.get('/status', async (request, response) => {
  console.log(`${new Date().toISOString()}`, `/status started`);
  const data = await getStatusData()
  console.log(`${new Date().toISOString()}`, `/status done`);
  return response.status(200).send(data)
});

async function getStatusData() {
  const catalogChromiumService = CatalogChromiumService.getInstance();
  var data = []
  const promises = []

  const pages = catalogChromiumService.getActivePages()

  if (pages.length) {
    _.each(catalogChromiumService.getActivePages(), async (page) => {
      const promise = page.pendingCaptures().then((captureCount) => {
        data.push([page.furnitureId, captureCount])
      })

      promises.push(promise)
    })

    await Promise.all(promises)

    data = _.sortBy(data, (d) => d[0])
    return data
  }
  else {
    return 'No active captures'
  }
}

module.exports = router;