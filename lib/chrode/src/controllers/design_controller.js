const _ = require('underscore');
const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const BoxsChromiumService = require('../services/boxs_chromium_service');
const chromiumSessions = {};
router.use(bodyParser.urlencoded({ extended: true }));

router.post('/:design_id/render/:camera_view_id', async (request, response) => {
  var boxsChromiumService = BoxsChromiumService.getInstance();
  const renderPage = await boxsChromiumService.getRenderPage();

  try {
    await renderPage.loadLocalRenderer(JSON.parse(request.body.payload));
    const renderData = await renderPage.screenshot();

    await renderPage.release();
    console.log(`${new Date().toISOString()}`, 'Completed');
    return response.send(renderData);
  }
  catch(e) {
    console.log(`${new Date().toISOString()}`, 'Failed');
    console.log(e.stack);
    await renderPage.release();
    return response.status(500).send("Error");
  }
});

module.exports = router;