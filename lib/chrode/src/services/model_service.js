BASE_URL = 'https://models.boxsapp.com'

const getRendererUrl = (roomId) => {
  return BASE_URL + '/rooms/' + roomId + '/renderer'
};

module.exports = {
  getRendererUrl
};