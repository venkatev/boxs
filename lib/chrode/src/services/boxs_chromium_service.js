const puppeteer = require('puppeteer');
const config = require('../config');
const _ = require('underscore');
const resolve = require('path').resolve

class RenderPage {
  constructor(page) {
    this.page = page;
  }

  /**
  * Marks as available.
  */
  async release() {
    console.log(`${new Date().toISOString()}`, `Closing browser...`);
    await this.page.close();
  }

  async loadLocalRenderer(payload) {
    const url = "file:///" + resolve(`${__dirname}/../../renderer/index.html`)

    this.page.setViewport({ width: 16 * 75, height: 9 * 75 });

    console.log(`${new Date().toISOString()}`, `Loading Renderer ${url}...`);
    await this.page.goto(url, { waitUntil: 'load', timeout: 0 });

    // Wait for the page to load.
    await this.page.waitFor(500);
    await this.page.evaluate((payload) => {
      RenderAPI.setPayload(payload)
    }, payload)
    await this.page.waitForFunction('RenderAPI.rendered')
  }

  async screenshot() {
    try {
      console.log(`${new Date().toISOString()}`, `Capturing screen`);
      const renderDiv = await this.page.$('.container');
      const base64ImageString = await renderDiv.screenshot({ encoding: "base64", type: 'jpeg' });
      const boundsMap = await this.page.evaluate(() => { return RenderAPI.getBoundsMap() });

      return {
        image: "data:image/jpeg;base64," + base64ImageString,
        bounds_map: boundsMap
      };
    } catch (e) {
      console.error(e);
    }
  }
}

class CatalogRenderPage {
  constructor(page, furnitureId) {
    this.page = page;
    this.furnitureId = furnitureId;
    this.available = true;
  }

  take() {
    this.available = false;
  }

  async release() {
    console.log(`${new Date().toISOString()}`, `Closing page...`);
    this.available = true;
    await this.page.close();
  }

  async loadRenderer(furnitureId) {
    const url = config.services.boxs + `/furnitures/${furnitureId}/renderer`;
    this.page.setViewport({ width: 16 * 75, height: 9 * 75 });

    console.log(`${new Date().toISOString()}`, `Loading Furniture Renderer ${url}...`);
    const response = await this.page.goto(url, { waitUntil: 'load', timeout: 0 });

    if (response.status() !== 200) {
      throw Error(`Page status is ${response.status()}`)
    }

    console.log(`${new Date().toISOString()}`, `Loaded ${url}`);
  }

  async pendingCaptures() {
    return await this.page.evaluate(() => { return RenderAPI.getPendingCaptures() });
  }

  async closeAfterRenderCompleted() {
    await this.page.waitForFunction('RenderAPI.isRenderCompleted()', { polling: 10000, timeout: 120000 });
    console.log(`${new Date().toISOString()}`, `Capture completed`);
    this.release();
  }

}

class BoxsChromiumService {
  constructor() {
  }

  static getInstance() {
    BoxsChromiumService.instance = BoxsChromiumService.instance || new BoxsChromiumService();
    return BoxsChromiumService.instance;
  }

  /**
  * Returns a browser page (equivalent to a tab) that can used for this session.
  */
  async getRenderPage() {
    return await this.addRenderPage();
  }

  /**
  * Adds a new page.
  */
  async addRenderPage() {
    var page

    try {
      page = await this.browser.newPage()
    }
    catch (e) {
      this.init()
      page = await this.browser.newPage()
    }

    return new RenderPage(page)
  }

  async init() {
    this.browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-dev-shm-usage'], headless: true });
    console.log(`${new Date().toISOString()}`, 'Ready');
  }
}

module.exports = BoxsChromiumService;
