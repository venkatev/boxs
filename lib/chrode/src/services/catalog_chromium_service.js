const puppeteer = require('puppeteer');
const config = require('../config');
const _ = require('underscore');

class CatalogRenderPage {
  constructor(page, furnitureId) {
    this.page = page;
    this.furnitureId = furnitureId;
    this.closed = false;
    this.loaded = false;
  }

  async release() {
    console.log(`${new Date().toISOString()}`, `Closing page...Done`);
    this.closed = true;
    await this.page.close();
  }

  async loadRenderer(furnitureId) {
    const url = config.services.boxs + `/furnitures/${furnitureId}/renderer`;
    this.page.setViewport({ width: 16 * 75, height: 9 * 75 });

    console.log(`${new Date().toISOString()}`, `Loading Furniture Renderer ${url}...`);
    const response = await this.page.goto(url, { waitUntil: 'load', timeout: 0 });

    if (response.status() !== 200) {
      throw Error(`Page status is ${response.status()}`)
    }

    this.loaded = true
    console.log(`${new Date().toISOString()}`, `Loaded ${url}`);
  }

  async pendingCaptures() {
    if (!this.loaded) {
      return 0
    }

    return this.page.evaluate(() => { return RenderAPI.getPendingCaptures() });
  }

  async closeAfterRenderCompleted() {
    await this.page.waitForFunction('RenderAPI.isRenderCompleted()', { polling: 10 * 1000, timeout: 5 * 60 * 1000 });
    console.log(`${new Date().toISOString()}`, `Releasing from closeAfterRenderCompleted`);
    this.release();
  }

}

class CatalogChromiumService {
  constructor() {
    this.renderPages = [];
  }

  static getInstance() {
    CatalogChromiumService.instance = CatalogChromiumService.instance || new CatalogChromiumService();
    return CatalogChromiumService.instance;
  }

  async init() {
    this.browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-dev-shm-usage'], headless: true });
    console.log(`${new Date().toISOString()}`, 'Ready');
  }

  getActivePages() {
    return _.reject(this.renderPages, 'closed');
  }

  getRenderPage(furnitureId) {
    return _.find(this.getActivePages(), (r) => r.furnitureId === furnitureId )
  }

  /**
  * Returns a browser page (equivalent to a tab) that can used for this session.
  */
  async createRenderPage(furnitureId) {
    this.gcPages();
    const page = await this.browser.newPage();
    const renderPage = new CatalogRenderPage(page, furnitureId);
    this.renderPages.push(renderPage);
    return renderPage;
  }

  gcPages() {
    const oldPageCount = this.renderPages.length
    this.renderPages = _.filter(this.renderPages, (rp) => !rp.page.isClosed());
    const gcCount = this.renderPages.length - oldPageCount

    if (gcCount) {
      console.log(`Released ${gcCount} pages`);
    }
  }
}

module.exports = CatalogChromiumService;
