const express = require('express');
var compression = require('compression')
const app = express();
const cors = require('cors')
var fs = require('fs')
var path = require('path')
const morgan = require('morgan')
const config = require('./config')
const designController = require('./controllers/design_controller');
const catalogController = require('./controllers/catalog_controller');

app.use(cors())
app.use(compression())
app.use(morgan('[:date[clf]] :method :url :status :res[content-length] - :response-time ms', {
  stream: fs.createWriteStream(path.join(__dirname, `../log/${process.env.NODE_ENV}.log`), { flags: 'a' })
}))

app.get('/chrode', (req, res) => res.send('Hello World!'))
app.use('/chrode/designs', designController)
app.use('/chrode/furnitures', catalogController)
app.use(express.static('public'))

async function onLoad() {
  console.log(`Chrode application starting in ${process.env.NODE_ENV} on port ${config.app.port}`);

  const BoxsChromiumService = require('./services/boxs_chromium_service');
  const bcs = BoxsChromiumService.getInstance();
  await bcs.init();
  bcs.browser.on('disconnected', bcs.init);

  const CatalogChromiumService = require('./services/catalog_chromium_service');
  const ccs = CatalogChromiumService.getInstance();
  await ccs.init();
}

app.listen(config.app.port, onLoad);