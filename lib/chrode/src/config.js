const config = {
 development: {
    app: {
      port: 3002
    },
    services: {
      boxs: 'http://localhost:3001'
    }
 },
 production: {
   app: {
     port: 3002
   },
   services: {
      boxs: 'https://boxs-env.eba-7ur6g8gs.ap-southeast-1.elasticbeanstalk.com'
   }
 }
};

module.exports = config[process.env.NODE_ENV];