'use strict';

var app = angular.module('boxsApp', [])
.run()
.controller('LocalRendererController', [
    '$scope', function ($scope) {
        $scope.loaded = false
        $scope.renderStatus = {}
        $scope.testMode = false

        $scope.init = function() {
          // NOTE Used for test rendering
          if ($scope.testMode) {
            $scope.ts = Date.now()

            setTimeout(() => {
              RenderAPI.rendered = true
            }, 1000)

            $scope.setPayload(
              {"catalog_base_url":"http://localhost:3001","room":{"room_type":"living_room","length":16,"breadth":24},"camera_view":{"id":14,"room_template_id":5,"position":{"x":4955.338478779674,"y":1524.0,"z":2471.232239959707},"target":{"x":-8753.562716843686,"y":1524.0,"z":3069.473484051921},"primary":null,"image":"view_1_1.jpg","created_at":"2021-03-12T03:36:09.610Z","updated_at":"2021-03-12T03:51:34.793Z","image_url":"http://localhost:3001/uploads/camera_view/image/14/view_1_1.jpg","viewport":{"width":1500,"height":1500}},"furniture_items":{"sb1":{"id":158,"position":[4880,0,1007],"dimensions":{"height":2052.790662,"width":1738.4676538555723,"depth":426.4522839999999},"orientation":2,"category":"sideboard","price":15507,"position_bounds":{"min_x":4454,"max_x":4880,"min_y":0,"max_y":2052,"min_z":1007,"max_z":2745}},"s1":{"id":160,"position":[2745,0,4575],"dimensions":{"height":874.2403765581874,"width":2209.1775001333717,"depth":1773.3508239546857},"orientation":3,"category":"sofa_set","price":12387,"position_bounds":{"min_x":536,"max_x":2745,"min_y":0,"max_y":874,"min_z":2802,"max_z":4575}},"d1":{"id":150,"position":[2135,0,5490],"dimensions":{"height":1331.9094532577265,"width":2247.8336977652616,"depth":1646.6344838988614},"orientation":1,"category":"dining_set","price":11198,"position_bounds":{"min_x":2135,"max_x":4382,"min_y":0,"max_y":1331,"min_z":5490,"max_z":7136}},"t1":{"id":162,"position":[610,0,0],"dimensions":{"height":1761.8598239999997,"width":2438.4,"depth":380.4002491972632},"orientation":1,"category":"tv_unit","price":12079,"position_bounds":{"min_x":610,"max_x":3048,"min_y":0,"max_y":1761,"min_z":0,"max_z":380}}}}
              // {"catalog_base_url":"https://boxs.in","room":{"room_type":"living_room","length":16,"breadth":12},"camera_view":{"id":21,"room_template_id":13,"position":{"x":1486,"y":1524,"z":6424},"target":{"x":1478,"y":1524,"z":1744},"primary":null,"image":"layout_wall.png","created_at":"2021-05-18T16:13:30.054Z","updated_at":"2021-05-21T08:28:26.661Z","image_url":"https://boxs-uploads.s3.amazonaws.com/uploads/camera_view/image/21/layout_wall.png","viewport":{"width":1500,"height":1500}},"furniture_items":{"0908_7A-16H_4_l":{"id":2133,"position":[0,0,3660],"dimensions":{"height":1048.1218607617002,"width":2814.8986770149822,"depth":2394.1186476590947},"orientation":4,"category":"sofa_set","position_bounds":{"min_x":0,"max_x":2394,"min_y":0,"max_y":1048,"min_z":846,"max_z":3660}},"1103_9K-13L_2_r":{"id":2134,"position":[3660,0,1871],"dimensions":{"height":1880.2126338278517,"width":1789.4061627843332,"depth":450.60639667075725},"orientation":2,"category":"tv_unit","position_bounds":{"min_x":3210,"max_x":3660,"min_y":0,"max_y":1880,"min_z":1871,"max_z":3660}},"2701_2D-3G_1_l":{"id":2137,"position":[915,1220,305],"dimensions":{"height":617.9049834258421,"width":711.1999999999999,"depth":232.12277734279758},"orientation":1,"category":"wall_shelf","position_bounds":{"min_x":915,"max_x":1626,"min_y":1220,"max_y":1837,"min_z":305,"max_z":537}},"2004_1A-1L_1_c":{"id":2135,"position":[-607,0,0],"dimensions":{"height":3048,"width":4876.799999999999,"depth":152.39999999999998},"orientation":1,"category":"wall_surface","position_bounds":{"min_x":-607,"max_x":4269,"min_y":0,"max_y":3048,"min_z":0,"max_z":152}},"2503_9A-14A_4_l":{"id":1396,"position":[0,1220,3660],"dimensions":{"height":832.9403280980736,"width":1774.868453726933,"depth":22.00275087431128},"orientation":4,"category":"painting","position_bounds":{"min_x":0,"max_x":22,"min_y":1220,"max_y":2052,"min_z":1886,"max_z":3660}}}}
              // {"catalog_base_url":"https://boxs.in","room":{"room_type":"living_room","length":4200,"breadth":3200},"camera_view":{"id":22,"room_template_id":14,"position":{"x":2017,"y":1524,"z":5996},"target":{"x":1930,"y":1524,"z":1647},"primary":null,"image":"layout-2.png","created_at":"2021-05-21T07:47:10.623Z","updated_at":"2021-05-21T07:47:10.623Z","image_url":"https://boxs-uploads.s3.amazonaws.com/uploads/camera_view/image/22/layout-2.png","viewport":{"width":1500,"height":1500}},"furniture_items":{"0908_1A-10H_4_l":{"id":2141,"position":[0,0,3050],"dimensions":{"height":934.2930457967246,"width":2750.513394940946,"depth":2389.6028708006543},"orientation":4,"category":"sofa_set","position_bounds":{"min_x":0,"max_x":2389,"min_y":0,"max_y":934,"min_z":300,"max_z":3050}},"1103_5I-10J_2_r":{"id":2138,"position":[3050,0,1251],"dimensions":{"height":1699.999902,"width":1799.9999559999999,"depth":450.00011599999993},"orientation":2,"category":"tv_unit","position_bounds":{"min_x":2600,"max_x":3050,"min_y":0,"max_y":1699,"min_z":1251,"max_z":3050}},"2701_5A-8B_4_l":{"id":1513,"position":[0,1220,2440],"dimensions":{"height":700.0001239999999,"width":1039.999952,"depth":330.0001091437615},"orientation":4,"category":"wall_shelf","position_bounds":{"min_x":0,"max_x":330,"min_y":1220,"max_y":1920,"min_z":1401,"max_z":2440}},"2004_1A-1J_1_c":{"id":2144,"position":[-918,0,0],"dimensions":{"height":2850.6811220451737,"width":4887.651874656442,"depth":6.329783635117655e-13},"orientation":1,"category":"wall_surface","position_bounds":{"min_x":-918,"max_x":3969,"min_y":0,"max_y":2850,"min_z":0,"max_z":0}},"2503_1C-1H_1_c":{"id":1802,"position":[613,1220,0],"dimensions":{"height":1003.9216432571411,"width":1822.910352230072,"depth":20.474717881792873},"orientation":1,"category":"painting","position_bounds":{"min_x":613,"max_x":2435,"min_y":1220,"max_y":2223,"min_z":0,"max_z":20}}}}
            )
          }
        }

        $scope.setPayload = function(payload) {
            if ($scope.loaded) {
              return
            }

            console.log(payload)
            $scope.loaded = true
            $scope.catalogBaseUrl = payload.catalog_base_url
            $scope.furnitureItems = payload.furniture_items

            $scope.cameraView = payload.camera_view
            $scope.canvasSize = { x: $scope.cameraView.viewport.width, y: $scope.cameraView.viewport.height }
            $scope.camera = new THREE.PerspectiveCamera(90, 1, 1, 100000);
            $scope.furnitureCentroidCamera = new THREE.PerspectiveCamera(90, 1, 1, 100000);
            $scope.furnitureProjectionCamera = new THREE.PerspectiveCamera(90, 1, 1, 100000);

            // We need to see rotations in terms of yaw/theta (w.r.t y-axis), so change the order.
            $scope.camera.rotation.order = 'YXZ'
            $scope.camera.up = new THREE.Vector3(0, 1, 0);

            $scope.furnitureCentroidCamera.rotation.order = 'YXZ'
            $scope.furnitureCentroidCamera.up = new THREE.Vector3(0, 1, 0);
            $scope.furnitureCentroidCamera.position.set($scope.cameraView.position.x, $scope.cameraView.position.y, $scope.cameraView.position.z);

            $scope.furnitureProjectionCamera.rotation.order = 'YXZ'
            $scope.furnitureProjectionCamera.up = new THREE.Vector3(0, 1, 0);
            $scope.furnitureProjectionCamera.position.set($scope.cameraView.position.x, $scope.cameraView.position.y, $scope.cameraView.position.z);

            $scope.setCamera()
            $scope.computeProjections()
            const boundsMap = RenderAPI.getBoundsMap()

            _.each($scope.furnitureItems, (item) => {
                if (item.price) {
                    const bounds = boundsMap[item.itemIdentifier]

                    if (bounds) {
                        const x = (bounds.min_x + bounds.max_x) / 2
                        const y = (bounds.min_y + bounds.max_y) / 2
                        item.hotspotPosition = { left: x, bottom: y }
                    }
                }
            })
        }

        $scope.setCamera = function() {
            $scope.camera.position.set($scope.cameraView.position.x, $scope.cameraView.position.y, $scope.cameraView.position.z);
            $scope.camera.lookAt(new THREE.Vector3($scope.cameraView.target.x, $scope.cameraView.target.y, $scope.cameraView.target.z));
            $scope.camera.updateMatrixWorld();
        }

        $scope.project = function(pointIn3D, camera) {
          const vector = new THREE.Vector3(pointIn3D.x, pointIn3D.y, pointIn3D.z)
          const projection = vector.project(camera)

          // Convert to viewport position. We must use only a 1:1 pixel ratio to avoid squishing. But, the viewport may not be a square.
          // Offset for that by subtracting the viewport (width - height) / 2.
          //
          // Assumes landscape/square.
          // TODO For supporting portrait, use the maximum of width/height and subtract the shorter one.
          return {
            left: Math.round($scope.cameraView.viewport.width / 2 * (projection.x + 1)),
            top: Math.round($scope.cameraView.viewport.width / 2 * (1 - projection.y)) - ($scope.cameraView.viewport.width - $scope.cameraView.viewport.height) / 2,
            z: projection.z
          }
        }

        $scope.getAngleInfo = function(point, orientation) {
            console.log($scope.camera_view);
            var vectorPoint = new THREE.Vector3(point.x, point.y, point.z)
            const relPoint = new THREE.Vector3(point.x - $scope.cameraView.position.x, point.y - $scope.cameraView.position.y, point.z - $scope.cameraView.position.z)

            // Move the camera to origin find the angle with the relative point.
            $scope.furnitureProjectionCamera.position.set(0, 0, 0)
            $scope.furnitureProjectionCamera.lookAt(relPoint)
            $scope.furnitureProjectionCamera.updateMatrixWorld();
            var distance = $scope.furnitureProjectionCamera.position.distanceTo(relPoint)
            var theta = ($scope.furnitureProjectionCamera.rotation.y * 180 / Math.PI)

            switch(orientation) {
                case 1:
                    // No-op
                    break

                case 2:
                    theta = theta + 90
                    break

                case 3:
                    theta = theta + 180
                    break

                case 4:
                    theta = theta + 270
                    break
            }

            theta = (-theta + 360 * 2) % 360

            // Round theta to the nearest 10 degree and distance to 1000.
            theta = Math.round(((360 + theta) % 360) / 10) * 10
            theta = Math.round((360 + theta) % 360)
            distance = Math.round(distance / 1000) * 1000

            if (distance > 6000) {
                distance = 6000
            }

            return { theta: Math.round(theta), r: Math.round(distance) }
        }

        $scope.computeProjections = function() {
            _.each($scope.furnitureItems, (furnitureItem, itemIdentifier) => {
                furnitureItem.itemIdentifier = itemIdentifier
                // console.log(`${furnitureItem.category} - ${furnitureItem.id} - ${furnitureItem.orientation}`)

                const pb = furnitureItem.position_bounds
                const points = [
                    { x: pb.min_x, y: pb.min_y, z: pb.min_z},
                    { x: pb.min_x, y: pb.min_y, z: pb.max_z},
                    { x: pb.min_x, y: pb.max_y, z: pb.min_z},
                    { x: pb.min_x, y: pb.max_y, z: pb.max_z},
                    { x: pb.max_x, y: pb.min_y, z: pb.min_z},
                    { x: pb.max_x, y: pb.min_y, z: pb.max_z},
                    { x: pb.max_x, y: pb.max_y, z: pb.min_z},
                    { x: pb.max_x, y: pb.max_y, z: pb.max_z}
                ]

                furnitureItem.centroid = { x: (pb.min_x + pb.max_x) / 2, y: (pb.min_y + pb.max_y) / 2, z: (pb.min_z + pb.max_z) / 2 }
                furnitureItem.projections = _.map(points, (point) => $scope.project(point, $scope.camera))

                console.log(furnitureItem.itemIdentifier)
                const angleInfo = $scope.getAngleInfo(furnitureItem.centroid, furnitureItem.orientation)

                furnitureItem.angleInfo = angleInfo

                // Get centroid projections.
                $scope.furnitureCentroidCamera.lookAt(new THREE.Vector3(furnitureItem.centroid.x, furnitureItem.centroid.y, furnitureItem.centroid.z))
                $scope.furnitureCentroidCamera.updateMatrixWorld();

                furnitureItem.centroidProjections = _.map(points, (point) => $scope.project(point, $scope.furnitureCentroidCamera))

                const leftPositions = _.map(furnitureItem.projections, 'left')
                const topPositions = _.map(furnitureItem.projections, 'top')
                const zValues = _.map(furnitureItem.projections, (p) => { return 1 - p.z })
                let minLeft = _.min(leftPositions)
                let maxLeft = _.max(leftPositions)
                let minTop = _.min(topPositions)
                let maxTop = _.max(topPositions)

                if (furnitureItem.category === 'wall_surface') {
                    // Hack to fix vertical rendering glitches in wall surfaces. Let them be rendered slightly larger vertically so that
                    // the ceiling and floor shall absorb those excess.
                    minTop -= 100
                    maxTop += 100
                }

                const tp = _.map(furnitureItem.projections, (p) => `${p.left}_${p.top}`).join('|')
                const cp = _.map(furnitureItem.centroidProjections, (p) => `${p.left}_${p.top}`).join('|')

                const projectedWidth = maxLeft - minLeft
                const projectedHeight = maxTop - minTop

                var visibleWidth, visibleHeight

                if (minLeft <= 0) {
                    if (maxLeft <= 1500) {
                        // Extending outside left boundary and ending within the viewport.
                        visibleWidth = maxLeft
                    }
                    else {
                        // Within the viewport
                        visibleWidth = 1500
                    }
                }
                else if (minLeft > 1500) {
                    // Starting after the viewport on the right.
                    visibleWidth = 0
                }
                else {
                    // Left within the viewport.
                    if (maxLeft <= 1500) {
                        // Right within the viewport.
                        visibleWidth = maxLeft - minLeft
                    }
                    else {
                        // Right outside viewport.
                        visibleWidth = 1500 - minLeft
                    }
                }

                if (minTop <= 0) {
                    if (maxTop <= 1500) {
                        // Extending outside top boundary and ending within the viewport.
                        visibleHeight = maxTop
                    }
                    else {
                        // Within the viewport
                        visibleHeight = 1500
                    }
                }
                else if (minTop > 1500) {
                    // Starting after the viewport on the bottom.
                    visibleHeight = 0
                }
                else {
                    // Top within the viewport.
                    if (maxTop <= 1500) {
                        // And, bottom within the viewport.
                        visibleHeight = maxTop - minTop
                    }
                    else {
                        // But, bottom outside viewport.
                        visibleHeight = 1500 - minTop
                    }
                }

                furnitureItem.visibleWidthPercentage = visibleWidth / projectedWidth
                furnitureItem.visibleHeightPercentage = visibleHeight / projectedHeight

                furnitureItem.renderPosition = { left: minLeft, top: minTop }
                furnitureItem.renderSize = { width: projectedWidth, height: projectedHeight }
                furnitureItem.maxZ = _.max(zValues)
                furnitureItem.isVisible = angleInfo.r > 1000 && (furnitureItem.visibleWidthPercentage * furnitureItem.visibleHeightPercentage) > 0.0625
                // furnitureItem.isVisible = true
                furnitureItem.catalogURL = `https://boxsapp.com/furnitures/${furnitureItem.id}/catalog_image?theta=${angleInfo.theta}&r=${angleInfo.r}`
                furnitureItem.renderURL = `http://localhost:3001/furnitures/${furnitureItem.id}/renderer?c=${angleInfo.theta},${angleInfo.r}`

                console.log(furnitureItem)
                console.log(angleInfo)
                if (furnitureItem.isVisible) {
                  setTimeout(() => {
                    $scope.$apply(function() {
                      furnitureItem.imageURL = `${$scope.catalogBaseUrl}/furnitures/${furnitureItem.id}/perspective?t=${angleInfo.theta}&r=${angleInfo.r}&cp=${cp}&tp=${tp}&category=${furnitureItem.category}`
                    })
                  }, 0)
                }

                // console.log(furnitureItem.projections)
            })

            $scope.furnitureItems = _.filter(_.values($scope.furnitureItems), (f) => f.isVisible)
            $scope.furnitureItems = _.sortBy($scope.furnitureItems, (item, index) => {
                // Render wall and floor surfaces before anything else.
                if (item.category === 'wall_surface') {
                    return 0
                }
                else {
                    return item.maxZ
                }
            })

            _.each($scope.furnitureItems, (item, index) => {
                if (item.category === 'wall_surface') {
                    // Let wall surfaces be rendered first, even before the room.
                    item.zIndex = -1
                }
                else {
                    item.zIndex = index
                }

                $scope.renderStatus[index] = false
                console.log(`${index} -> ${item.category}`)
            })
        }

        $scope.onFurnitureRender = function(index) {
            $scope.renderStatus[index] = true
            $scope.updateRenderedStatus()
        }

        $scope.updateRenderedStatus = function() {
            if (_.all(_.values($scope.renderStatus))) {
                RenderAPI.rendered = true
            }
        }
    }
])
.directive('onImageLoad', ['$parse', function ($parse) {
    return {
      restrict: 'A',
      link: function (scope, elem, attrs) {
        var fn = $parse(attrs.onImageLoad);
        elem.on('load', function (event) {
          scope.$apply(function() {
            fn(scope, { $event: event });
          });
        });
      }
    };
  }]);

const RenderAPI = {
    rendered: false,
    getScope: () => {
        return angular.element('#renderer').scope();
    },
    getBoundsMap: () => {
        const furnitureItems = RenderAPI.getScope().furnitureItems
        const boundsMap = {}
        const canvasSize = RenderAPI.getScope().canvasSize;

        _.each(furnitureItems, (furnitureItem) => {
            if (furnitureItem.isVisible) {
                const bounds = {
                    min_x: furnitureItem.renderPosition.left,
                    max_x: furnitureItem.renderPosition.left + furnitureItem.renderSize.width,
                    min_y: canvasSize.y - furnitureItem.renderPosition.top - furnitureItem.renderSize.height,
                    max_y: canvasSize.y - furnitureItem.renderPosition.top,
                }

                if (bounds.min_x < 0) bounds.min_x = 0
                if (bounds.min_y < 0) bounds.min_y = 0
                if (bounds.max_x > canvasSize.x) bounds.max_x = canvasSize.x
                if (bounds.max_y > canvasSize.x) bounds.max_x = canvasSize.x

                boundsMap[furnitureItem.itemIdentifier] = bounds
            }
        })

        return boundsMap
    },
    setPayload: (payload) => {
        RenderAPI.getScope().setPayload(payload)
        RenderAPI.getScope().$apply()
    }
}

$(window).click((event) => {
  console.log(event.clientX, event.clientY)
})

$(() => {
  window.s = RenderAPI.getScope()
})