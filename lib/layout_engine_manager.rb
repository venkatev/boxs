module LayoutEngineManager
  # FIXME_INTEGRATION. This value is duplicated in layout_engine.js (Grid manager).
  CELL_SIZE = 1.0
  ROWS = (1..50).to_a.map(&:to_s).freeze
  COLS = (('A'..'Z').to_a + ('a'..'z').to_a).freeze
  ROOM_HEIGHT = 3050

  # Number of cells defined in each circulation matrix around the object bounds.
  # FIXME_FURNISH - Temp value.
  CIRCULATION_PADDING = 3

  # status: new|draft|published
  FURNISH_CONFIG = {
    'living_room' => {
      wall_sizes: [ '9-10', '11-12', '13-14', '15-17', '18-20', '21-23', '24-26', '27-30' ],
      super_categories: [
        { name: 'entrance_door',    shell: true,  implicit: true },
        { name: 'exit_door',        shell: true,  implicit: true },
        { name: 'window',           shell: true,  implicit: true },
        { name: 'wall_surface',     major: false, implicit: true },
        { name: 'floor_surface',    major: false, implicit: true },
        { name: 'window_curtain',   major: false, implicit: true },
        { name: 'sofa_set',         major: true,  reference: 'entrance_door' },
        { name: 'tv_unit',          major: true, reference: 'sofa_set' },
        { name: 'dining_set',       major: true, reference: 'sofa_set' },
        { name: 'crockery_unit',    major: true, reference: 'dining_set' },
        { name: 'sideboard',        major: false, reference: 'sofa_set' },
        { name: 'console_table',    major: false, reference: 'sofa_set' },
        { name: 'book_shelf',       major: false, reference: 'sofa_set' },
        { name: 'foyer_chest',      major: false, reference: 'entrance_door' },
        { name: 'chest_of_drawers', major: false, reference: 'sofa_set' },
        { name: 'buffet',           major: false, reference: 'dining_set' },
        { name: 'pooja_unit',       major: false, reference: 'dining_set' },
        { name: 'wall_shelf',       major: false, reference: 'sofa_set' },
        { name: 'painting',         major: false, reference: 'entrance_door' },
        { name: 'wall_decor',       major: false, reference: 'sofa_set' },
        { name: 'coner_shelf',      major: false, reference: 'dining_set' },
        { name: 'planter',          major: false, reference: 'sofa_set' },
        { name: 'shoe_rack',        major: false, reference: 'sofa_set' },
        { name: 'bed',              major: false, reference: 'sofa_set' },
        { name: 'wardrobe',         major: false, reference: 'sofa_set' },
        { name: 'dressing_table',   major: false, reference: 'sofa_set' },
        { name: 'bedside_table',    major: false, reference: 'sofa_set' },
        { name: 'study_table',    major: false, reference: 'sofa_set' },
      ],
      categories: {
        'entrance_door' => {
          super_category: 'entrance_door',
          variants: [
            {
              id: '0201',
              name: 'Entrance Door',
              size: { width: '2.5-3', depth: '0.5-0.75', height: '6.5-7' },
              y_position: 0,
              status: 'published'
            }
          ],
          status: 'published'
        },
        'exit_door' => {
          super_category: 'exit_door',
          variants: [
            {
              id: '0301',
              name: 'Internal Door',
              size: { width: '2.5-3', depth: '0.5-0.75', height: '6.5-7' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '0302',
              name: "Open Entrance 3'",
              size: { width: '3-3.5', depth: '0.5-0.75', height: '6.5-7' },
              y_position: 0,
              status: 'published'
            }
          ],
          status: 'published'
        },
        'window' => {
          super_category: 'window',
          variants: [
            {
              id: '0401',
              name: "Window 3' x 4'",
              size: { width: '2.5-3.5', depth: '0.5-0.75', height: '3.5-4.5' },
              y_position: 3,
              status: 'published'
            },
            {
              size: { width: '3.5-4.5', depth: '0.5-0.75', height: '3.5-4.5' },
              y_position: 3,
              name: "Window 4' x 4'",
              id: '0402',
              status: 'published'
            },
            {
              size: { width: '5-7', depth: '0.5-0.75', height: '3.5-4.5' },
              y_position: 3,
              name: "Window 6' x 4'",
              id: '0403',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'balcony_door' => {
          super_category: 'exit_door',
          variants: [
            {
              size: { width: '5-7', depth: '0.5-0.75', height: '6.5-7' },
              y_position: 0,
              name: "Balcony Door 6'",
              id: '0501',
              status: 'published'
            },
            {
              size: { width: '7-9', depth: '0.5-0.75', height: '6.5-7' },
              y_position: 0,
              name: "Balcony Door 8'",
              id: '0502',
              status: 'published'
            }
          ],
          status: 'published'
        },
        # L Set
        # -----
        # 3+Ottoman   : 84" x 62" - 7' x 5.25'
        # 3+2         : 123" x 105" - 11.25' x 8.75'

        # 3+2 Corner  : 88" x 88" - 7.5' x 7.5'
        # 3+3 Corner  : 112" x 88" - 9.5' x 7.5'
        # 3+4 Corner  : 112" x 112" - 9.5' x 9.5'

        # U Set
        # -----
        # 2+1+1       : 138" x 82" - 11.5' x 6.75'
        # 3+1+1       : 162 x 82 - 13.5' x 6.75'

        # 3+2+1       : 162" x 105" - 13.5' x 8.75'
        # 3+2+1+1     :162" x 123" - 13.5' x 10.75'
        'sofa_with_coffee_table' => {
          super_category: 'sofa_set',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '4-6', depth: '4-7', height: '2.5-3.5' },
              y_position: 0,
              name: "Sofa 5'",
              id: '0601',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '4-7', height: '2.5-3.5' },
              y_position: 0,
              name: "Sofa 7'",
              id: '0602',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'sofa_set_u_left_aligned' => {
          super_category: 'sofa_set',
          attached_to_wall: false,
          variants: [
            { # sofa_set_2_1_1_u_la
              size: { width: '10-12', depth: '6-7', height: '2.5-3' },
              y_position: 0,
              name: "U Sofa Set LA 10' x 7'",
              id: '0701',
              status: 'published'
            },
            { # sofa_set_3_1_1_u_la
              size: { width: '12-14', depth: '6-7', height: '2.5-3' },
              y_position: 0,
              name: "U Sofa Set LA 12' x 7'",
              id: '0702',
              status: 'published'
            },
            { # sofa_set_3_2_1_u_la
              size: { width: '12-14', depth: '7-9', height: '2.5-3' },
              y_position: 0,
              name: "U Sofa Set LA 12' x 9'",
              id: '0703',
              status: 'published'
            },
            { # sofa_set_3_2_2_u_la
              size: { width: '12-14', depth: '8.5-9.5', height: '2.5-3' },
              y_position: 0,
              name: "U Sofa Set LA 12' x 9'",
              id: '0704',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'sofa_set_u_right_aligned' => {
          super_category: 'sofa_set',
          attached_to_wall: false,
          variants: [
            { # sofa_set_2_1_1_u_ra
              size: { width: '10-12', depth: '6-7', height: '2.5-3' },
              y_position: 0,
              name: "U Sofa Set RA 10' x 7'",
              id: '0801',
              status: 'published'
            },
            { # sofa_set_3_1_1_u_ra
              size: { width: '12-14', depth: '6-7', height: '2.5-3' },
              y_position: 0,
              name: "U Sofa Set RA 12' x 7'",
              id: '0802',
              status: 'published'
            },
            { # sofa_set_3_2_1_u_la
              size: { width: '12-14', depth: '7-9', height: '2.5-3' },
              y_position: 0,
              name: "U Sofa Set RA 12' x 9'",
              id: '0803',
              status: 'published'
            },
            { # sofa_set_3_2_2_u_ra
              size: { width: '12-14', depth: '7-9', height: '2.5-3' },
              y_position: 0,
              name: "U Sofa Set RA 12' x 9'",
              id: '0804',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'sofa_set_l_left_aligned' => {
          super_category: 'sofa_set',
          attached_to_wall: false,
          # Includes corner and sectional sofas.
          grobr_categories: [
            'sofa_set_2_1_1_l_la', 'sofa_set_2_2_l_la', 'sofa_set_2_3_l_la',
            'sofa_set_3_1_1_l_la', 'sofa_set_3_2_l_la', 'sofa_set_2_2_c_la',
            'sofa_set_2_3_c_la', 'sofa_set_3_2_c_la', 'sofa_set_3_3_c_la'
          ],
          variants: [
            { # 2_1_1_l_la
              size: { width: '7-9', depth: '9-11', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set 2+1+1 LA 9' x 7'",
              id: '0901',
            status: 'published'
            },
            { # 2_2_l_la
              size: { width: '7-9', depth: '7-9', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set 2+2+1 LA 9' x 9'",
              id: '0902',
              status: 'published'
            },
            { # 2_3_l_la
              size: { width: '7-9', depth: '9-11', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set 2+3 LA 9' x 11'",
              id: '0903',
              status: 'published'
            },
            { # 3_1_1_l_la
              size: { width: '9-11', depth: '9-11', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set 3+1+1 LA 11' x 9'",
              id: '0904',
              status: 'published'
            },
            { # 3_2_l_la
              size: { width: '9-11', depth: '7-9', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set 3+2 LA 11' x 9'",
              id: '0905',
              status: 'published'
            },
            { # 2_2_c_la
              size: { width: '7-8', depth: '7-8', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set (Corner) 2+2 LA 7' x 7'",
              id: '0906',
              status: 'published'
            },
            { # 2_3_c_la
              size: { width: '7-8', depth: '9-10', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set (Corner) 2+3 LA 7' x 9'",
              id: '0907',
              status: 'published'
            },
            { # 3_2_c_la
              size: { width: '9-10', depth: '7-8', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set (Corner) 3+3 LA 9' x 7'",
              id: '0908',
              status: 'published'
            },
            { # 3_3_c_la
              size: { width: '9-10', depth: '9-10', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set (Corner) 3+3 LA 9' x 9'",
              id: '0909',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'sofa_set_l_right_aligned' => {
          super_category: 'sofa_set',
          attached_to_wall: false,
          # Includes corner and sectional sofas.
          grobr_categories: [
            'sofa_set_2_1_1_l_ra', 'sofa_set_2_2_l_ra', 'sofa_set_2_3_l_ra',
            'sofa_set_3_1_1_l_ra', 'sofa_set_3_2_l_ra', 'sofa_set_2_2_c_ra',
            'sofa_set_2_3_c_ra', 'sofa_set_3_2_c_ra', 'sofa_set_3_3_c_ra'
          ],
          variants: [
            { # 2_1_1_l_ra
              size: { width: '7-9', depth: '9-11', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set 2+1+1 RA 9' x 7'",
              id: '1001',
              status: 'published'
            },
            { # 2_2_l_ra
              size: { width: '7-9', depth: '7-9', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set 2+2 RA 9' x 9'",
              id: '1002',
              status: 'published'
            },
            { # 2_3_l_ra
              size: { width: '7-9', depth: '9-11', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set 2+3 RA 9' x 11'",
              id: '1003',
              status: 'published'
            },
            { # 3_1_1_l_ra
              size: { width: '9-11', depth: '9-11', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set 3+1+1 RA 11' x 9'",
              id: '1004',
              status: 'published'
            },
            { # 3_2_l_ra
              size: { width: '9-11', depth: '7-9', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set 3+2 RA 11' x 9'",
              id: '1005',
              status: 'published'
            },
            { # 2_2_c_ra
              size: { width: '7-8', depth: '7-8', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set (Corner) 2+2 RA 7' x 7'",
              id: '1006',
              status: 'published'
            },
            { # 2_3_c_ra
              size: { width: '7-8', depth: '9-10', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set (Corner) 2+3 RA 7' x 9'",
              id: '1007',
              status: 'published'
            },
            { # 3_2_c_ra
              size: { width: '9-10', depth: '7-8', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set (Corner) 3+2 RA 9' x 7'",
              id: '1008',
              status: 'published'
            },
            { # 3_3_c_ra
              size: { width: '9-10', depth: '9-10', height: '2.5-3' },
              y_position: 0,
              name: "L Sofa Set (Corner) 3+3 RA 9' x 9'",
              id: '1009',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'tv_unit_floor' => {
          super_category: 'tv_unit',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '3-4', depth: '1-2', height: '3-6' },
              y_position: 0,
              name: "TV Unit Floor 3'",
              id: '1101',
              status: 'published'
            },
            {
              size: { width: '4-5', depth: '1-2', height: '3-6' },
              y_position: 0,
              name: "TV Unit Floor 4'",
              id: '1102',
              status: 'published'
            },
            {
              size: { width: '5-6', depth: '1-2', height: '3-6' },
              y_position: 0,
              name: "TV Unit Floor 5'",
              id: '1103',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '3-6' },
              y_position: 0,
              name: "TV Unit Floor 7'",
              id: '1104',
              status: 'published'
            },
            {
              size: { width: '8-10', depth: '1-2', height: '3-6' },
              y_position: 0,
              name: "TV Unit Floor 9'",
              id: '1105',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'foyer_chest' => { # Placed near main door or in foyer
          super_category: 'foyer_chest',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '2.5-3.5', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Foyer Chest 3'",
              id: '1201',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'chest_of_drawers' => { # Placed in the living area, generally a little tall.
          super_category: 'chest_of_drawers',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '2-3', depth: '1-2', height: '3-7' },
              y_position: 0,
              name: "Chest of Drawers 2.5'",
              id: '1301',
              status: 'published'
            },
            {
              size: { width: '3-5', depth: '1-2', height: '3-7' },
              y_position: 0,
              name: "Chest of Drawers 4'",
              id: '1302',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'buffet' => { # Place next to dining unit
          super_category: 'buffet',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '4-6', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Buffet Table 5'",
              id: '1401',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Buffet Table 7'",
              id: '1402',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'sideboard' => { # Placed in the dining area
          super_category: 'sideboard',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '4-6', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Sideboard 5'",
              id: '1501',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Sideboard 7'",
              id: '1502',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'crockery_unit' => {
          super_category: 'crockery_unit',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '2-4', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Crockery 3'",
              id: '1601',
              status: 'published'
            },
            {
              size: { width: '4-6', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Crockery 5'",
              id: '1602',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Crockery 7'",
              id: '1603',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'dining_set_4_seater' => {
          super_category: 'dining_set',
          attached_to_wall: false,
          variants: [
            { # Rectangle
              size: { width: '4-5.5', depth: '3.5-4', height: '2.5-4.5' }, #Height is increased to accomodate the things placed on table top.

              y_position: 0,
              name: "Dining 4 Seater",
              id: '1701',
              status: 'published'
            },
            { #Square
              size: { width: '4-5', depth: '4-5', height: '2.5-4.75' },
              y_position: 0,
              name: "Dining 4 Seater Square",
              id: '1702',
              status: 'published'
            },
            { #Circle
              size: { width: '4-5', depth: '4-5', height: '2.5-4.75' },
              y_position: 0,
              name: "Dining 4 Seater Circle",
              id: '1703',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'dining_set_6_seater' => {
          super_category: 'dining_set',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '6-7.5', depth: '4-6', height: '2.5-4.75' },
              y_position: 0,
              name: "Dining 6 Seater",
              id: '1801',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'window_curtain' => {
          super_category: 'window_curtain',
          attached_to_wall: true,
          variants: [
            {
              id: '1901',
              name: "Curtain for 3' x 4' Window",
              size: { width: '2-4', depth: '0.5-0.75', height: '5-6' },
              y_position: 3,
              status: 'published'
            },
            {
              id: '1902',
              name: "Curtain for 4' x 6' Window",
              size: { width: '4-5', depth: '0.5-0.75', height: '5-6' },
              y_position: 3,
              status: 'published'
            },
            {
              id: '1903',
              name: "Curtain for 6' x 8' Window",
              size: { width: '5-7', depth: '0.5-0.75', height: '6-8' },
              y_position: 3,
              status: 'published'
            },
            {
              id: '1904',
              name: "Curtain for 8' x 8' Window",
              size: { width: '7-9', depth: '0.5-0.75', height: '6-8' },
              y_position: 3,
              status: 'published'
            }
          ],
          status: 'published'
        },
      'wall_surface' => {
          super_category: 'wall_surface',
          attached_to_wall: true,
          variants: [
            {
              id: '2001',
              name: "Wall Surface 10'",
              size: { width: '9.95-10.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '2002',
              name: "Wall Surface 12'",
              size: { width: '11.95-12.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '2003',
              name: "Wall Surface 14'",
              size: { width: '13.95-14.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '2004',
              name: "Wall Surface 16'",
              size: { width: '15.95-16.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '2005',
              name: "Wall Surface 18'",
              size: { width: '17.95-18.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '2006',
              name: "Wall Surface 20'",
              size: { width: '19.95-20.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            }
          ],
          status: 'published'
        },
        'floor_surface' => {
          super_category: 'floor_surface',
          attached_to_wall: true,
          variants: [
            {
              id: '2101',
              name: "Floor Surface",
              size: { width: '1-30', depth: '0-0.25', height: '0-10' },
              y_position: 0,
              status: 'published' # FIXME - Dont have circulation map yet
            }
          ],
          status: 'published'
        },
        'console_table' => { # Placed in the dining area
          super_category: 'console_table',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '3-4', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Console Table 3'",
              id: '2201',
              status: 'published'
            },
            {
              size: { width: '4-6', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Console Table 5'",
              id: '2202',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Console Table 7'",
              id: '2203',
              status: 'published'
            },
          ],
          status: 'published'
        },
        'book_shelf' => { # Placed in the dining area
          super_category: 'book_shelf',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '3-4', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Book Shelf 3'",
              id: '2301',
              status: 'published'
            },
            {
              size: { width: '4-6', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Book Shelf 5'",
              id: '2302',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Book Shelf 7'",
              id: '2303',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'pooja_unit' => { # Placed in the dining area
          super_category: 'pooja_unit',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '2-3', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Pooja Unit 3'",
              id: '2401',
              status: 'published'
            },
            {
              size: { width: '3-5', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Pooja Unit 4'",
              id: '2402',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'painting' => { # Placed in the dining area
          super_category: 'painting',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '1-2', depth: '0-0.5', height: '1-4' },
              y_position: 4,
              name: "Painting 2'",
              id: '2501',
              status: 'published'
            },
            {
              size: { width: '2-4', depth: '0-0.5', height: '1-4' },
              y_position: 4,
              name: "Painting 4'",
              id: '2502',
              status: 'published'
            },
            {
              size: { width: '4-6', depth: '0-0.5', height: '1-4' },
              y_position: 4,
              name: "Painting 6'",
              id: '2503',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'wall_decor' => { # Placed in the dining area
          super_category: 'wall_decor',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '1-3', depth: '0-0.5', height: '1-4' },
              y_position: 4,
              name: "Wall Decor 3'",
              id: '2601',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'wall_shelf' => { # Placed in the dining area
          super_category: 'wall_shelf',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '2-4', depth: '0.5-1.5', height: '1-4' },
              y_position: 4,
              name: "Wall Shelf 3'",
              id: '2701',
              status: 'published'
            },
            {
              size: { width: '4-6', depth: '0.5-1.5', height: '1-4' },
              y_position: 4,
              name: "Wall Shelf 5'",
              id: '2702',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'corner_shelf' => { # Placed in the dining area
          super_category: 'corner_shelf',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '1-2', depth: '0.5-1.5', height: '1-4' },
              y_position: 4,
              name: "Corner Shelf 2'",
              id: '2801',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'planter' => {
          super_category: 'planter',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '0.5-2.5', depth: '0.5-2.5', height: '1-7' },
              y_position: 0,
              name: "Planter",
              id: '2901',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'shoe_rack' => {
          super_category: 'shoe_rack',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '2-5', depth: '1-2', height: '3-5' },
              y_position: 0,
              name: "Shoe Rack'",
              id: '3001',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'bed' => {
          super_category: 'bed',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '5-6', depth: '6-7', height: '2-3' },
              y_position: 0,
              name: "Queen Bed'",
              id: '3101',
              status: 'published'
            },
            {
              size: { width: '3-5', depth: '4-7', height: '2-7' },
              y_position: 0,
              name: "Kid's Bed'",
              id: '3102',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'wardrobe' => {
          super_category: 'wardrobe',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '4-6', depth: '1-2', height: '7-10' },
              y_position: 0,
              name: "Wardrobe 5'",
              id: '3201',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'bedside_table' => {
          super_category: 'bedside_table',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '1-2', depth: '1-2', height: '1-3' },
              y_position: 0,
              name: "Bedside Table",
              id: '3301',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'dressing_table' => {
          super_category: 'dressing_table',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '1-3', depth: '1-4', height: '2-7' },
              y_position: 0,
              name: "Dressing Table 2'",
              id: '3401',
              status: 'published'
            },
            {
              size: { width: '3-5', depth: '1-4', height: '2-7' },
              y_position: 0,
              name: "Dressing Table 4'",
              id: '3402',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'study_table' => {
          super_category: 'study_table',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '3-4', depth: '1-4', height: '2-7' },
              y_position: 0,
              name: "Study Table 3'",
              id: '3501',
              status: 'published'
            },
            {
              size: { width: '3-6', depth: '1-4', height: '2-7' },
              y_position: 0,
              name: "Kids Study Table 3'",
              id: '3502',
              status: 'published'
            }
          ],
          status: 'published'
        }
      }
    },
    'dining_room' => {
      wall_sizes: [ '9-10', '11-12', '13-14', '15-17', '18-20', '21-23', '24-26', '27-30' ],
      super_categories: [
        { name: 'entrance_door',    shell: true,  implicit: true },
        { name: 'exit_door',        shell: true,  implicit: true },
        { name: 'window',           shell: true,  implicit: true },
        { name: 'wall_surface',     major: false, implicit: true },
        { name: 'floor_surface',    major: false, implicit: true },
        { name: 'window_curtain',   major: false, implicit: true },
        { name: 'dining_set',       major: true, reference: 'sofa_set' },
        { name: 'crockery_unit',    major: true, reference: 'dining_set' },
        { name: 'sideboard',        major: false, reference: 'sofa_set' },
        { name: 'console_table',    major: false, reference: 'sofa_set' },
        { name: 'book_shelf',       major: false, reference: 'sofa_set' },
        { name: 'foyer_chest',      major: false, reference: 'entrance_door' },
        { name: 'chest_of_drawers', major: false, reference: 'sofa_set' },
        { name: 'buffet',           major: false, reference: 'dining_set' },
        { name: 'pooja_unit',       major: false, reference: 'dining_set' },
        { name: 'wall_shelf',       major: false, reference: 'sofa_set' },
        { name: 'painting',         major: false, reference: 'entrance_door' },
        { name: 'wall_decor',       major: false, reference: 'sofa_set' },
        { name: 'coner_shelf',      major: false, reference: 'dining_set' }
      ],
      categories: {
        'entrance_door' => {
          super_category: 'entrance_door',
          variants: [
            {
              id: '0201',
              name: 'Entrance Door',
              size: { width: '2.5-3', depth: '0.5-0.75', height: '6.5-7' },
              y_position: 0,
              status: 'published'
            }
          ],
          status: 'published'
        },
        'exit_door' => {
          super_category: 'exit_door',
          variants: [
            {
              id: '0301',
              name: 'Internal Door',
              size: { width: '2.5-3', depth: '0.5-0.75', height: '6.5-7' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '0302',
              name: "Open Entrance 3'",
              size: { width: '3-3.5', depth: '0.5-0.75', height: '6.5-7' },
              y_position: 0,
              status: 'published'
            }
          ],
          status: 'published'
        },
        'window' => {
          super_category: 'window',
          variants: [
            {
              id: '0401',
              name: "Window 3' x 4'",
              size: { width: '2.5-3.5', depth: '0.5-0.75', height: '3.5-4.5' },
              y_position: 3,
              status: 'published'
            },
            {
              size: { width: '3.5-4.5', depth: '0.5-0.75', height: '3.5-4.5' },
              y_position: 3,
              name: "Window 4' x 4'",
              id: '0402',
              status: 'published'
            },
            {
              size: { width: '5-7', depth: '0.5-0.75', height: '3.5-4.5' },
              y_position: 3,
              name: "Window 6' x 4'",
              id: '0403',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'balcony_door' => {
          super_category: 'exit_door',
          variants: [
            {
              size: { width: '5-7', depth: '0.5-0.75', height: '6.5-7' },
              y_position: 0,
              name: "Balcony Door 6'",
              id: '0501',
              status: 'published'
            },
            {
              size: { width: '7-9', depth: '0.5-0.75', height: '6.5-7' },
              y_position: 0,
              name: "Balcony Door 8'",
              id: '0502',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'foyer_chest' => { # Placed near main door or in foyer
          super_category: 'foyer_chest',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '2.5-3.5', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Foyer Chest 3'",
              id: '1201',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'chest_of_drawers' => { # Placed in the living area, generally a little tall.
          super_category: 'chest_of_drawers',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '2-5', depth: '1-2', height: '3-7' },
              y_position: 0,
              name: "Chest of Drawers'",
              id: '1301',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'buffet' => { # Place next to dining unit
          super_category: 'buffet',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '4-6', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Buffet Table 5'",
              id: '1401',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Buffet Table 7'",
              id: '1402',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'sideboard' => { # Placed in the dining area
          super_category: 'sideboard',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '4-6', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Sideboard 5'",
              id: '1501',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Sideboard 7'",
              id: '1502',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'crockery_unit' => {
          super_category: 'crockery_unit',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '2-4', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Crockery 3'",
              id: '1601',
              status: 'published'
            },
            {
              size: { width: '4-6', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Crockery 5'",
              id: '1602',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Crockery 7'",
              id: '1603',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'dining_set_4_seater' => {
          super_category: 'dining_set',
          attached_to_wall: false,
          variants: [
            { # Rectangle
              size: { width: '4-5.5', depth: '3.5-4', height: '2.5-4.5' }, #Height is increased to accomodate the things placed on table top.

              y_position: 0,
              name: "Dining 4 Seater",
              id: '1701',
              status: 'published'
            },
            { #Square
              size: { width: '4-5', depth: '4-5', height: '2.5-4.75' },
              y_position: 0,
              name: "Dining 4 Seater Square",
              id: '1702',
              status: 'published'
            },
            { #Circle
              size: { width: '4-5', depth: '4-5', height: '2.5-4.75' },
              y_position: 0,
              name: "Dining 4 Seater Circle",
              id: '1703',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'dining_set_6_seater' => {
          super_category: 'dining_set',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '6-7.5', depth: '4-6', height: '2.5-4.75' },
              y_position: 0,
              name: "Dining 6 Seater",
              id: '1801',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'window_curtain' => {
          super_category: 'window_curtain',
          attached_to_wall: true,
          variants: [
            {
              id: '1901',
              name: "Curtain for 3' x 4' Window",
              size: { width: '2-4', depth: '0.5-0.75', height: '4-6' },
              y_position: 3,
              status: 'published'
            },
            {
              id: '1902',
              name: "Curtain for 4' x 6' Window",
              size: { width: '4-5', depth: '0.5-0.75', height: '5-6' },
              y_position: 3,
              status: 'published'
            },
            {
              id: '1903',
              name: "Curtain for 6' x 8' Window",
              size: { width: '5-7', depth: '0.5-0.75', height: '6-8' },
              y_position: 3,
              status: 'published'
            },
            {
              id: '1904',
              name: "Curtain for 8' x 8' Window",
              size: { width: '7-9', depth: '0.5-0.75', height: '6-8' },
              y_position: 3,
              status: 'published'
            }
          ],
          status: 'published'
        },
        'wall_surface' => {
          super_category: 'wall_surface',
          attached_to_wall: true,
          variants: [
            {
              id: '2001',
              name: "Wall Surface 10'",
              size: { width: '9.95-10.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '2002',
              name: "Wall Surface 12'",
              size: { width: '11.95-12.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '2003',
              name: "Wall Surface 14'",
              size: { width: '13.95-14.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '2004',
              name: "Wall Surface 16'",
              size: { width: '15.95-16.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '2005',
              name: "Wall Surface 18'",
              size: { width: '17.95-18.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            },
            {
              id: '2006',
              name: "Wall Surface 20'",
              size: { width: '19.95-20.05', depth: '0-1', height: '10-10' },
              y_position: 0,
              status: 'published'
            }
          ],
          status: 'published'
        },
        'floor_surface' => {
          super_category: 'floor_surface',
          attached_to_wall: true,
          variants: [
            {
              id: '2101',
              name: "Floor Surface",
              size: { width: '1-30', depth: '0-0.25', height: '0-10' },
              y_position: 0,
              status: 'published' # FIXME - Dont have circulation map yet
            }
          ],
          status: 'published'
        },
        'console_table' => { # Placed in the dining area
          super_category: 'console_table',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '3-4', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Console Table 3'",
              id: '2201',
              status: 'published'
            },
            {
              size: { width: '4-6', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Console Table 5'",
              id: '2202',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Console Table 7'",
              id: '2203',
              status: 'published'
            },
          ],
          status: 'published'
        },
        'book_shelf' => { # Placed in the dining area
          super_category: 'book_shelf',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '3-4', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Book Shelf 3'",
              id: '2301',
              status: 'published'
            },
            {
              size: { width: '4-6', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Book Shelf 5'",
              id: '2302',
              status: 'published'
            },
            {
              size: { width: '6-8', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Book Shelf 7'",
              id: '2303',
              status: 'published'
            },
          ],
          status: 'published'
        },
        'pooja_unit' => { # Placed in the dining area
          super_category: 'pooja_unit',
          attached_to_wall: false,
          variants: [
            {
              size: { width: '2-3', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Pooja Unit 3'",
              id: '2401',
              status: 'published'
            },
            {
              size: { width: '3-5', depth: '1-2', height: '2.5-7' },
              y_position: 0,
              name: "Pooja Unit 4'",
              id: '2402',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'painting' => { # Placed in the dining area
          super_category: 'painting',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '1-2', depth: '0-0.5', height: '1-4' },
              y_position: 4,
              name: "Painting 2'",
              id: '2501',
              status: 'published'
            },
            {
              size: { width: '2-4', depth: '0-0.5', height: '1-4' },
              y_position: 4,
              name: "Painting 4'",
              id: '2502',
              status: 'published'
            },
            {
              size: { width: '4-6', depth: '0-0.5', height: '1-4' },
              y_position: 4,
              name: "Painting 6'",
              id: '2503',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'wall_decor' => { # Placed in the dining area
          super_category: 'wall_decor',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '1-3', depth: '0-0.5', height: '1-4' },
              y_position: 4,
              name: "Wall Decor 3'",
              id: '2601',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'wall_shelf' => { # Placed in the dining area
          super_category: 'wall_shelf',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '2-4', depth: '0.5-1.5', height: '1-4' },
              y_position: 4,
              name: "Wall Shelf 3'",
              id: '2701',
              status: 'published'
            },
            {
              size: { width: '4-6', depth: '0.5-1.5', height: '1-4' },
              y_position: 4,
              name: "Wall Shelf 5'",
              id: '2702',
              status: 'published'
            }
          ],
          status: 'published'
        },
        'corner_shelf' => { # Placed in the dining area
          super_category: 'corner_shelf',
          attached_to_wall: true,
          variants: [
            {
              size: { width: '1-2', depth: '0.5-1.5', height: '1-4' },
              y_position: 4,
              name: "Corner Shelf 2'",
              id: '2801',
              status: 'published'
            }
          ],
          status: 'published'
        }
      }
    }
  }

  # Add circulation matrices.
  FURNISH_CONFIG.each do |room_type, room_type_config|
    room_type_config[:categories].each do |category, category_config|
      next if category_config[:status] == 'published'

      category_config[:variants].each do |variant|
        next if variant[:status] == 'published'

        if !File.exists?(File.expand_path(File.dirname(__FILE__)) + "/circulation_maps/#{variant[:id]}.csv")
          puts "Circulation map not found for #{room_type} -> #{category} -> #{variant[:id]}"
          next
        end

        matrix = []
        defined_matrix = CSV.parse(File.read(File.expand_path(File.dirname(__FILE__)) + "/circulation_maps/#{variant[:id]}.csv"), headers: false)
        blank_matrix = defined_matrix.map {|r| r.map {|c| 'i' }}.freeze

        num_layers = (ROOM_HEIGHT / 305.0 / CELL_SIZE).round
        avg_object_height = (variant[:size][:height].split("-").map(&:to_f).sum / 2.0).round
        object_num_layers = (avg_object_height / CELL_SIZE).round

        start_cell = variant[:y_position].round
        end_cell = start_cell + object_num_layers - 1

        0.upto(num_layers) do |layer|
          matrix[layer] = (start_cell..end_cell).include?(layer) ? defined_matrix : blank_matrix
        end

        variant[:circulation] = matrix
      end
    end
  end

  # FIXME
  FURNISH_CATEGORIES_META = {
    'living_room' => [
      { super_category: 'sofa_set', major: true },
      { super_category: 'tv_unit_floor', major: false },
      # { super_category: 'dining_set_4_seater', major: false },
      # { super_category: 'crockery_unit', major: false },
      # { super_category: 'chest_of_drawer', major: false },
      # { super_category: 'wall_decor', major: false }
    ]
  }

  # FIXME. Move Production data to S3?
  module DataStore
    PATH = Rails.env.production? ? '/var/app/data/layout_engine_rules.json' : 'lib/layout_engine_rules.json'

    def self.read
      JSON.parse(File.read(PATH))
    end

    def self.write!(rules)
      File.open(PATH, 'w') {|f| f.write(rules.to_json) }
      LEM.update_rules_cache(rules)
    end
  end

  module TransformHelper
    ROTATION_MAP = {}
    ORIENTATIONS = (1..4).to_a.freeze

    def self.init(room_type, room_absolute_size)
      room_length = LEM.get_room_size_range(room_type, room_absolute_size[:room_length])
      room_breadth = LEM.get_room_size_range(room_type, room_absolute_size[:room_breadth])
      room_size = { room_length: room_length, room_breadth: room_breadth }

      ROTATION_MAP[room_type] ||= {}

      return if ROTATION_MAP[room_type].key?(room_size)

      ROTATION_MAP[room_type][room_size] = {}

      avg_room_length = (room_size[:room_length].split('-').map(&:to_f).sum / 2.0).round
      avg_room_breadth = (room_size[:room_breadth].split('-').map(&:to_f).sum / 2.0).round

      num_cols = (avg_room_length / CELL_SIZE).round
      num_rows = (avg_room_breadth / CELL_SIZE).round
      max_size = [num_rows, num_cols].max

      limited_rows = ROWS[0..(max_size - 1)]
      limited_cols = COLS[0..(max_size - 1)]

      max_size.times do |i|
        max_size.times do |j|
          4.times do |k|
            ROTATION_MAP[room_type][room_size]["#{limited_rows[i]}#{limited_cols[j]}#{ORIENTATIONS[k]}"] =
            "#{limited_rows[j]}#{limited_cols[max_size - 1 - i]}#{ORIENTATIONS[(k + 1) % (ORIENTATIONS.size)]}"
          end
        end
      end
    end

    def self.rotate(room_type, room_size, cell_range, num_rotations, flipped_bounds = false)
      cell_range =~ /^((\d+(?:[A-Z]|[a-z])([1-4]))(-\d+(?:[A-Z]|[a-z])([1-4]))?)/
      orientation = $3.to_i
      context_room_size = flipped_bounds ?
        { room_breadth: room_size[:room_length], room_length: room_size[:room_breadth] } : room_size

      avg_room_length = (context_room_size[:room_length].split('-').map(&:to_f).sum / 2.0).round
      avg_room_breadth = (context_room_size[:room_breadth].split('-').map(&:to_f).sum / 2.0).round

      num_cols = (avg_room_length / CELL_SIZE).round
      num_rows = (avg_room_breadth / CELL_SIZE).round

      cells = cell_range.split('-')

      # Default shape of the room
      # room_shape = num_rows_actual > num_cols_actual ? 'vertical' : 'horizontal'
      room_shape = num_rows > num_cols ? 'vertical' : 'horizontal'

      num_rotations = (4 + num_rotations) if num_rotations < 0
      # num_rotations = num_rotations < 0 ? (4 + num_rotations) : (num_rotations % 4)
      num_rotations.times do
        cells = cells.map { |cell| ROTATION_MAP[room_type][room_size][cell] }
      end

      shift_count = (num_rows - num_cols).abs
      shift_rows = shift_cols = false

      # We need to have the final range of cells starting from the origin. Cannot find a better way
      # to do other than the following: Decide the shift according to the room orientation and number of rotations
      if room_shape == 'vertical'
        shift_cols = true if num_rotations == 2
        shift_rows = true if num_rotations == 3
      elsif room_shape == 'horizontal'
        shift_cols = true if num_rotations == 1
        shift_rows = true if num_rotations == 2
      end

      min_row = nil
      min_col = nil
      max_row = nil
      max_col = nil

      cells.each do |cell|
        cell =~ /(\d+)([A-Z]|[a-z])([1-4])/
        orientation = $3

        row_index = ROWS.index($1)
        row_index = row_index - shift_count if shift_rows

        col_index = COLS.index($2)
        col_index = col_index - shift_count if  shift_cols

        min_row = min_row ? ((row_index < min_row) ? row_index : min_row) : row_index
        max_row = max_row ? ((row_index > max_row) ? row_index : max_row) : row_index
        min_col = min_col ? ((col_index < min_col) ? col_index : min_col) : col_index
        max_col = max_col ? ((col_index > max_col) ? col_index : max_col) : col_index
      end

      return "#{ROWS[min_row]}#{COLS[min_col]}#{orientation}-#{ROWS[max_row]}#{COLS[max_col]}#{orientation}"
    end

    def self.normalize_2(room_size, cell)
      avg_room_length = (room_size[:room_length].split('-').map(&:to_f).sum / 2.0).round
      avg_room_breadth = (room_size[:room_breadth].split('-').map(&:to_f).sum / 2.0).round

      cell =~ /(\d+)([A-Z]|[a-z])([1-4])/
      row_index = ROWS.index($1) + 1
      col_index = COLS.index($2) + 1
      orientation = $3.to_i

      case orientation
      when 1
        new_row_index = row_index
        new_col_index = col_index

      when 2
        new_row_index = (avg_room_length - col_index) + 1
        new_col_index = row_index

      when 3
        new_row_index = (avg_room_breadth - row_index) + 1
        new_col_index = (avg_room_length - col_index) + 1

      when 4
        new_row_index = col_index
        new_col_index = (avg_room_breadth - row_index) + 1
      end

      return "#{ROWS[new_row_index - 1]}#{COLS[new_col_index - 1]}1"
    end

    def self.normalize_2_range(room_size, cell_range)
      cell_1, cell_2 = *cell_range.split('-')
      cell_1_normalized = normalize_2(room_size, cell_1)
      cell_2_normalized = normalize_2(room_size, cell_2)

      min_row = nil
      min_col = nil
      max_row = nil
      max_col = nil
      orientation = nil

      [cell_1_normalized, cell_2_normalized].each do |cell|
        cell =~ /(\d+)([A-Z]|[a-z])([1-4])/
        orientation = $3
        row_index = ROWS.index($1)
        col_index = COLS.index($2)

        min_row = min_row ? ((row_index < min_row) ? row_index : min_row) : row_index
        max_row = max_row ? ((row_index > max_row) ? row_index : max_row) : row_index
        min_col = min_col ? ((col_index < min_col) ? col_index : min_col) : col_index
        max_col = max_col ? ((col_index > max_col) ? col_index : max_col) : col_index
      end

      return "#{ROWS[min_row]}#{COLS[min_col]}#{orientation}-#{ROWS[max_row]}#{COLS[max_col]}#{orientation}"
    end

    def self.normalize(room_type, room_size, cell_range, reverse: false, target_cell_range: nil, flipped_bounds: false)
      cell_range =~ /^((\d+(?:[A-Z]|[a-z])([1-4]))(-\d+(?:[A-Z]|[a-z])([1-4]))?)/
      orientation = $3.to_i
      target_cell_range ||= cell_range

      # If already in orientation 1, do nothing.
      return target_cell_range if orientation == 1

      # Find the number of rotations needed to reach orientation 1.
      num_rotations = 5 - orientation
      return target_cell_range if num_rotations == 0

      num_rotations *= -1 if reverse

      result = rotate(room_type, room_size, target_cell_range, num_rotations, flipped_bounds)
      return result
    end

    def self.get_transformations(room_type, room_size, normalized_cell_range, mode = :all, flipped_bounds: false)
      if mode == :opposites
        opposite_cell_range = rotate(room_type, room_size, normalized_cell_range, 2, flipped_bounds)
        return [normalized_cell_range, opposite_cell_range]

      elsif mode == :adjacents
        adjacent_1_cell_range = rotate(room_type, room_size, normalized_cell_range, 1, flipped_bounds)
        adjacent_2_cell_range = rotate(room_type, room_size, normalized_cell_range, 3, flipped_bounds)
        return [adjacent_1_cell_range, adjacent_2_cell_range]

      elsif mode == :all
        return get_transformations(room_type, room_size, normalized_cell_range, :opposites, flipped_bounds) +
          get_transformations(room_type, room_size, normalized_cell_range, :adjacents, flipped_bounds)
      end
    end
  end
  TH = TransformHelper

  def self.get_config_for_catalog
    config = {}

    FURNISH_CONFIG.each do |room_type, room_type_config|
      config[room_type] = {}
      room_type_config[:super_categories].each do |sc|
        config[room_type][sc[:name]] = {}
      end

      room_type_config[:categories].each do |category, category_config|
        config[room_type][category_config[:super_category]][category] = category_config[:variants].map do |variant|
          variant.slice(:id, :name, :size)
        end
      end
    end

    return config
  end

  def self.get_cell_range(placement)
    placement =~ /^((\d+(?:[A-Z]|[a-z])([1-4]))(-\d+(?:[A-Z]|[a-z])([1-4]))?)/
    return $1
  end

  def self.get_rules(force = false)
    clear_rules_cache! if force
    @@rules ||= DataStore.read
  end

  def self.clear_rules_cache!
    @@rules = nil
  end

  def self.update_rules_cache(rules)
    @@rules = rules
  end

  def self.get_category_for_variant(variant_id)
    FURNISH_CONFIG.each do |room_category, room_config|
      room_config[:categories].each do |category_name, category_config|
        category_config[:variants].each do |variant|
          if variant[:id] == variant_id
            return category_name
          end
        end
      end
    end

    raise "Category not found for variant #{variant_id}"
  end

  def self.get_super_category_for_variant(variant_id)
    FURNISH_CONFIG.each do |room_category, room_config|
      room_config[:categories].each do |category_name, category_config|
        category_config[:variants].each do |variant|
          if variant[:id] == variant_id
            return category_config[:super_category]
          end
        end
      end
    end

    raise "Super category not found for variant #{variant_id}"
  end

  def self.shell?(room_type, category)
    get_super_category_of(room_type, category)[:shell]
  end

  def self.is_major_super_category?(room_type, super_category)
    !!get_super_category_details(room_type, super_category)[:major]
  end

  def self.get_circulation_matrix(room_type, category, variant_id)
    get_category_variant(room_type, category, variant_id)[:circulation] || [[]]
  end

  def self.get_furnishable_super_categories(room_type)
    FURNISH_CONFIG[room_type][:super_categories].select {|mc| mc[:reference] }
  end

  def self.lookup_super_category(room_type, super_category_name)
    FURNISH_CONFIG[room_type][:super_categories].find {|sc| sc[:name] == super_category_name }
  end

  def self.get_y_position(room_type, variant_id)
    lookup_variant(room_type, variant_id)[:y_position]
  end

  def self.get_wall_sizes(room_type)
    FURNISH_CONFIG[room_type][:wall_sizes].map {|size| Range.new(*size.split('-').map(&:to_i)).to_a }.flatten.uniq
  end

  def self.get_room_size_range(room_type, absolute_size)
    FURNISH_CONFIG[room_type][:wall_sizes].find {|size|
      Range.new(*size.split('-').map(&:to_f)).include?(absolute_size)
    }
  end

  def self.attached_to_wall?(room_type, category)
    FURNISH_CONFIG[room_type][:categories][category][:attached_to_wall]
  end

  def self.only_direct_placement?(room_type, category)
    super_category = self.get_super_category_of(room_type, category)
    super_category[:reference].blank?
  end

  def self.get_variants(room_type, category)
    FURNISH_CONFIG[room_type][:categories][category][:variants].select {|variant| variant[:status] == 'published' }
  end

  def self.get_categories(room_type, super_category)
    FURNISH_CONFIG[room_type][:categories].select {|category, details| details[:status] == 'published' && details[:super_category] == super_category }.keys
  end

  def self.get_shell_categories(room_type)
    super_categories = FURNISH_CONFIG[room_type][:super_categories].select {|mc| mc[:shell] }.map {|sc| sc[:name] }
    categories = FURNISH_CONFIG[room_type][:categories].select {|category, details| details[:status] == 'published' && super_categories.include?(details[:super_category]) }
    return categories
  end

  def self.get_category_details(room_type, category)
    FURNISH_CONFIG[room_type][:categories][category]
  end

  def self.get_category_variant(room_type, category, variant_id)
    get_variants(room_type, category).find {|v| v[:id] == variant_id }
  end

  def self.get_category_of(room_type, variant_id)
    record = FURNISH_CONFIG[room_type][:categories].find do |category, details|
      details[:variants].any? {|v| v[:id] == variant_id}
    end

    record[0]
  end

  def self.get_super_category_details(room_type, super_category)
    FURNISH_CONFIG[room_type][:super_categories].find {|sc| sc[:name] == super_category}
  end

  def self.get_super_category_of(room_type, category)
    super_category = FURNISH_CONFIG[room_type][:categories][category][:super_category]
    get_super_category_details(room_type, super_category)
  end

  def self.lookup_variants(room_type, variant_ids)
    category_configs = FURNISH_CONFIG[room_type][:categories].values.flatten
    variants = category_configs.map {|cc| cc[:variants]}.flatten
    variants = variants.select {|variant| variant_ids.include?(variant[:id]) }
    return variants
  end

  def self.lookup_variant(room_type, variant_id)
    lookup_variants(room_type, [ variant_id ])[0]
  end

  def self.get_orientation(placement)
    placement =~ /^\d+(?:[A-Z]|[a-z])([1-4])/
    return $1.to_i
  end

  # FIXME: Get the alignments from the LEM map. For now, hardcoding them.
  def self.get_vertical_alignment(variant_id)
    {"ref"=>"w", "value"=>"b_b"}
  end

  def self.get_depth_alignment(variant_id)
    {"ref"=>"c", "value"=>"b_b"}
  end

  def self.print_lem_details(room_type)
    puts "LEM ID / NAME / WIDTH / DEPTH / HEIGHT"
    # super categories
    sc_arr = FURNISH_CONFIG[room_type][:super_categories].map{|x| x[:name]}
    # categories
    c_arr = sc_arr.map{|sc| LEM.get_categories(room_type, sc)}.flatten
    # lem in each of the categories
    lem_details =  c_arr.map{|c| LEM.get_variants(room_type, c)}.flatten
    lem_details.each{|l| puts "#{l[:id]} / #{l[:name]} / #{l[:size][:width]} / #{l[:size][:depth]} / #{l[:size][:height]}"}
    1
  end

  def self.get_placements(room: {}, object: {}, reference: {})
    room_length = self.get_room_size_range(room[:type], room[:length])
    room_breadth = self.get_room_size_range(room[:type], room[:breadth])
    room_size = { room_breadth: room_breadth, room_length: room_length }
    room_size_transformed = { room_breadth: room_length, room_length: room_breadth }

    rules = get_rules

    if reference.present?
      reference_orientation = get_orientation(reference[:cell_range])
      is_flipped = reference_orientation == 2 || reference_orientation == 4

      # The length and breadth will have to be reversed since we are looking up in the adjacent sides.
      room_context_size = is_flipped ? room_size_transformed : room_size
      normalized_placement = TransformHelper.normalize_2_range(room_size, reference[:cell_range])

      begin
        placements = rules['relative'][object[:category]][object[:variant_id]][reference[:category]][reference[:variant_id]][room_context_size.to_json][normalized_placement] || []
      rescue
        # FIXME
        placements = []
      end

      if object[:variant_id] == '1701' && reference[:variant_id] == '0602' && reference[:cell_range] == '1J2-7N2'
        # puts "HELLOioooooo"
      end

      placements = placements.map {|placement_string|
        placement = process_placement(placement_string)

        # This placement has been arrived rotating the reference to the default orientation.
        # So, apply the same number of rotations in reverse to get the correct cell range.
        placement[:cell_range] = TransformHelper.normalize(room[:type], room_size, reference[:cell_range], reverse: true, target_cell_range: placement[:cell_range], flipped_bounds: is_flipped)
        placement
      }
    else
      # Direct placement.
      room_context_size = room_size

      if attached_to_wall?(room[:type], object[:category])
        room_lookup_size = { room_length: room_length }
      else
        room_lookup_size = room_size
      end

      direct_rules = rules['direct'][object[:category]][object[:variant_id]] rescue []

      if direct_rules.present?
        placements_1 = direct_rules[room_lookup_size.to_json] || []
        placements_1 = placements_1.map { |placement_string|
          placement = process_placement(placement_string)
          TransformHelper.get_transformations(room[:type], room_context_size, placement[:cell_range], :opposites).map {|cell_range|
            placement.merge(cell_range: cell_range)
          }
        }.flatten

        # Lookup with flip wall sizes for rectangular rooms.
        if room_length != room_breadth
          if attached_to_wall?(room[:type], object[:category])
            room_lookup_size = { room_length: room_breadth }
          else
            room_lookup_size = room_size_transformed
          end

          placements_2 = direct_rules[room_lookup_size.to_json] || []
          placements_2 = placements_2.map { |placement_string|
            placement = process_placement(placement_string)
            TransformHelper.get_transformations(room[:type], room_context_size, placement[:cell_range], :adjacents).map {|cell_range|
              placement.merge(cell_range: cell_range)
            }
          }.flatten
        end

        placements = placements_1 + placements_2
      else
        placements = []
      end
    end

    return placements
  end

  def self.process_placement(placement)
    placement =~ /^(\d+(?:[A-Z]|[a-z])[1-4]-\d+(?:[A-Z]|[a-z])[1-4])@(.+)/
    cell_range = $1
    alignment_parts = $2.split('@')
    alignment_parts = alignment_parts.map do |align_part|
      align_part =~ /(.)_(._.)([\+-]\d+)?/
      { ref: $1, value: $2, offset: $3 }
    end

    { cell_range: cell_range, h_align: alignment_parts[0], d_align: alignment_parts[1] }
  end

  def self.generate_direct_rules
    # Generate the rules for main door
    wall_sizes = FURNISH_CONFIG['living_room'][:wall_sizes].map {|s| { room_length: s} }
    door_variants = FURNISH_CONFIG['living_room'][:categories]['entrance_door'][:variants]

    entrance_door_data = {}

    door_variants.each do |variant|
      entrance_door_data[variant[:id]] = {}
      avg_object_width = (variant[:size][:width].split('-').map(&:to_f).sum / 2.0).round

      wall_sizes.each do |wall_size|
        avg_wall_length = (wall_size[:room_length].split('-').map(&:to_f).sum / 2.0).round
        total_cols = (avg_wall_length / CELL_SIZE).round
        object_cols = (avg_object_width / CELL_SIZE).round

        placements = []
        placements << "1#{COLS[0]}1-1#{COLS[object_cols - 1]}1"
        placements << "1#{COLS[total_cols - object_cols]}1-1#{COLS[total_cols - 1]}1"
        entrance_door_data[variant[:id]][wall_size.to_json] = placements
      end
    end

    @data = {
      "direct" => {
        "entrance_door" => entrance_door_data
      },
      "relative" => {
      }
    }

    DataStore.write!(@data)
    puts "Default rules updated."
  end
end
LEM = LayoutEngineManager