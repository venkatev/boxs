Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  mount RailsAdmin::Engine => '/admin_2', as: 'rails_admin'

  resources :designs, :only => [:index, :show] do
    member do
      get :renderer
      get :similar
      get :furniture_recommendations
      post :replace_furniture
      post :save_to_my_designs
    end
  end

  resources :rooms, :only => [:new, :index, :create, :update] do
    collection do
      post :create_with_layout
    end

    member do
      get :layouts
      post :save_design
    end

    resources :custom_designs, :only => [:index, :show] do
      member do
        get :similar
        post :replace_furniture
      end
    end
  end

  resources :moodboards, :only => [:index, :show, :update] do
    member do
      post :publish
      post :unpublish
      post :archive
    end
  end

  resources :furnitures do
    collection do
      get :all
    end

    member do
      get :loader
      get :renderer
      get :perspective
      put :update_dimensions
      put :update_lights
      put :remove_views
    end

    resources :furniture_views, only: [:create, :destroy]
  end

  get '/inspirations', to: "inspirations#index"

  get '/layout_engine/catalog_configs', to: 'layout_engine#catalog_configs'
  get '/layout_engine/manage', to: 'layout_engine#manage', :as => 'manage_layout_engine'
  post '/layout_engine/get_placements', to: 'layout_engine#get_placements'
  post '/layout_engine/save', to: 'layout_engine#save', :as => 'save_layout_engine'

  get '/sessions/user', to: 'sessions#user'
  post '/sessions/request_otp', to: 'sessions#request_otp'
  post '/sessions/login', to: 'sessions#login'
  delete '/sessions/logout', to: 'sessions#logout', :as => 'logout'

  get '/looks', to: 'designs#looks'
  get '/furnitures/:id', to: "designs#furniture"
  get '/rooms/:room_id/layouts/:layout_id/designs/:design_index/views/:view_index', to: 'designs#view'
  get '/rooms/:room_id/layouts/:layout_id/next_design/:view_index', to: 'designs#next'

  get '/rooms/select', to: 'rooms#templates'
  get '/rooms/:id', to: "rooms#show"
  post '/designs/:id/views/:view_index', to: "designs#view"
  post '/designs/next', to: "designs#next"

  get '/chrode/furnitures/:id/renderer', to: "furnitures#chrode_renderer"
  get '/chrode/furnitures/status', to: "furnitures#chrode_status"

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'home#show'
end
