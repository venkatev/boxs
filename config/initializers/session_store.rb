Rails.application.config.session_store :active_record_store,
                                       key: '_pegasus_session',
                                       domain: :all
