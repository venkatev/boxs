RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard do
      only [ 'Moodboard', 'Design', 'CameraView', 'Furniture' ]
      statistics false
    end

    index do
      only [ 'Moodboard', 'Design', 'CameraView', 'Furniture' ]
    end

    new do
      only [ 'Moodboard', 'Furniture' ]
    end

    show do
      only [ 'Moodboard', 'CameraView', 'Furniture' ]
    end

    edit do
      only [ 'Moodboard', 'Design', 'CameraView' ]
    end

    delete do
      only [ 'Moodboard' ]
    end

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'RoomLayout' do
    edit do
      field :room_type, :enum do
        enum do
          RoomLayout::Type.values
        end
      end

      field :name
      field :length
      field :width
      field :image
    end
  end

  config.model 'FurnitureLayout' do
    edit do
      field :room_layout do
        searchable [ { Room => :name } ]
      end

      field :grobr_room_id
    end

    show do
      field :grobr_room_id
      field :designs do
        pretty_value do
          bindings[:view].render :partial => 'design_summary', :locals => { :object => bindings[:object] }
        end
      end
    end
  end

  config.model 'Moodboard' do
    list do
      # field :room_type
      field :public
      field :reference_image
      field :furniture_map
    end

    create do
      field :room_type, :enum do
        enum { RoomLayout::Type.values }
      end

      field :reference_image
      field :public do
        def value
          true
        end

        visible false
      end

      field :furniture_map do
        def value
          bindings[:object].formatted_furniture_map
        end
      end
    end

    edit do
      field :room_type, :enum do
        enum do
          RoomLayout::Type.values
        end
      end

      field :reference_image
      field :public do
        read_only true
      end

      field :furniture_map do
        def value
          bindings[:object].formatted_furniture_map
        end
      end
    end

    show do
      field :room_type
      field :reference_image
      field :public
      field :furniture_map
    end
  end

  config.model 'Design' do
    list do
      field :furniture_layout
      field :moodboard
      field :primary_render do
        formatted_value do
          bindings[:view].tag(:img, { :src => bindings[:object].primary_render.image.url })
        end
      end
    end

    edit do
      field :featured
      field :primary_render do
        read_only true
        pretty_value do
          bindings[:view].tag(:img, { :src => bindings[:object].primary_render.image.url })
        end
      end
    end
  end

  config.model 'Furniture' do
    create do
      field :variant_id
      field :zipped_model_file
      field :identifier
    end

    list do
      field :id
      field :identifier
      field :variant_id
      field :dimensions do
        def value
          h = bindings[:object].dimensions["height"].to_f.round if bindings[:object].dimensions
          w = bindings[:object].dimensions["width"].to_f.round if bindings[:object].dimensions
          d = bindings[:object].dimensions["depth"].to_f.round if bindings[:object].dimensions
          return "h#{h}, w#{w}, d#{d}"
        end
      end
      field :pending_captures do
        def value
          bindings[:object].pending_captures_count
        end
      end
      field :zipped_model_file
      # field :render_url do
      #   def value
      #     bindings[:view].tag(:img, { :href => bindings[:object].id })
      #   end
      # end
      field :data_errors
      # field :image do
      #   pretty_value do
      #     primary_furniture_view = bindings[:object].furniture_views.first
      #     url = primary_furniture_view ? primary_furniture_view.image.url : nil
      #     url ? bindings[:view].tag(:img, { :src => url}) : "Not Available"
      #   end
      # end
    end
  end
end


