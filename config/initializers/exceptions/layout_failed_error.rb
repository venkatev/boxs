class LayoutFailedError < StandardError
  def initialize(message)
    super(message)
  end
end