module Services
  private

  ALL = YAML.load_file("#{Rails.root.to_s}/config/services.yml")[Rails.env]

  public

  GROBR = ALL['grobr']
  CHRODE = ALL['chrode']
end
