# Ref: Fog Issue: https://github.com/fog/fog/issues/3429 and fog.rb

require 'carrierwave/storage/abstract'
require 'carrierwave/storage/file'
require 'carrierwave/storage/fog'

CarrierWave.configure do |config|
  if Rails.env.development? || Rails.env.test?
    config.storage = :file

  elsif Rails.env.production?
    config.storage = :fog
    config.fog_credentials = {
      :provider => 'AWS', # required
      :aws_access_key_id => 'AKIAIGCO65OWLSMYCQVQ',
      :aws_secret_access_key => '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',
      :region => 'ap-south-1',
    }

    config.fog_directory = 'boxs-uploads'
    config.fog_public = true
    config.fog_attributes = {'Cache-Control' => 'max-age=315576000'}
  end
end

module MiniMagick
  class Image
    def pixel_at x, y
      run_command("convert", "#{path}[1x1+#{x.to_i}+#{y.to_i}]", 'txt:').split("\n").each do |line|
        return $1 if /^0,0:.*(#[0-9a-fA-F]+)/.match(line)
      end
      nil
    end
  end
end